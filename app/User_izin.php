<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_izin extends Model
{
    protected $table = 'user_izin';
    public $primaryKey = 'id';
    protected $fillable = ['user_id','izin_id'];

}
