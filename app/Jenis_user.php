<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis_user extends Model
{
    protected $table = 'jenis_user';
    public $primaryKey = 'id';
    protected $fillable = ["nama"];
}
