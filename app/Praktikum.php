<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Praktikum extends Model
{
    protected $table = 'praktikum';
    public $primaryKey = 'id';
    protected $fillable = ["laboratorium_id" , "nama"];

    public function materi() {
        return $this->hasMany(Materi::class, 'praktikum_id');
    }

    public function periode_praktikum()
    {
        return $this->hasMany(Periode_praktikum::class, 'praktikum_id');
    }
    
}