<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Izin extends Model
{
    protected $table = 'izin';
    public $primaryKey = 'id';
    protected $fillable = ['nama'];

    public function peran()
    {
        return $this->belongsToMany(Peran::class,'peran_izin');
    }

}
