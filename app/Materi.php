<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    protected $table = 'materi';
    public $primaryKey = 'id';
    protected $fillable = ["praktikum_id","nama"];

    public function praktikum()
    {
        return $this->belongsTo(Praktikum::class, 'praktikum_id');
    }
    
    // public function detail_modul()
    // {
    //     return $this->hasMany(Detail_modul::class,'materi_id');
    // }
}

