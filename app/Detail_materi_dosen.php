<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_materi_dosen extends Model
{
    protected $table = "detail_materi_dosen";
    public $primaryKey = "id";
    protected $fillable = ["penilaian_dosen_id","modul_materi_dosen_id", "nilai_materi"];
}
