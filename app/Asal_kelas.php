<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asal_kelas extends Model
{
    protected $table = 'asal_kelas';
    public $primaryKey = 'id';
    protected $fillable = ["jenis_kelas_id","nama"];
}
