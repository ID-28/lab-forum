<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul_materi_aslab extends Model
{
    protected $table = 'modul_materi_aslab';
    protected $fillable = ["modul_aslab_id","materi_id"];
}
