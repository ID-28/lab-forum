<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periode_praktikum extends Model
{
    protected $table = 'periode_praktikum';
    public $primaryKey = 'id';
    protected $fillable = ["tahun","status",'praktikum_id'];

    public function praktikum() {
        return $this->belongsTo(Praktikum::class, 'praktikum_id');
    }
    public function modul()
    {
        return $this->hasMany(Modul::class,'periode_praktikum_id');
    }
    public function periode_praktikum_dosen()
    {
        return $this->hasMany(Periode_praktikum_dosen::class, 'periode_praktikum_id');
    }
}
