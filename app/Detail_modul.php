<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_modul extends Model
{
    protected $table = 'detail_modul';
    protected $fillable = ["modul_id","materi_id"];

    public function modul(){
        return $this->belongsTo(Modul::class,'modul_id');
    }

    public function materi(){
        return $this->belongsTo(Materi::class,'materi_id');
    }

}
