<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_materi extends Model
{
    protected $table = 'detail_materi';
    public $primaryKey = 'id';
    protected $fillable = ["id","penilaian_dosen_id", "materi_id" , "nilai_materi"];
}
