<?php

namespace App\Helpers;

class Formulir {
    public static function regId($namaPraktikum,$tahun,$praktikanId) {
        $singkatan = "";
        $data = explode(" ",$namaPraktikum);
        foreach($data as $d){
            $singkatan = $singkatan . $d[0];
        }
        $id = strtoupper($singkatan) . $tahun . str_pad($praktikanId , 4, '0', STR_PAD_LEFT);
        return $id;
    }
}