<?php

namespace App\Helpers;

use \App\Asal_kelas;
use Illuminate\Support\Facades\DB;
use \App\Periode_praktikum_dosen;
use \App\Periode_praktikum_aslab;
use \App\Mahasiswa;

class AslabAsdos {
    public static function getDosenAslab($praktikan){
        $kelas = Asal_kelas::findOrFail($praktikan->asal_kelas_id);
        $idLab = \Auth::user()->id-1;

        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',$idLab)
        ->where('periode_praktikum.status',1)
        ->first();

        $dosen = DB::table('periode_praktikum_dosen')
        ->select('periode_praktikum_dosen.*')
        ->join('periode_praktikum','periode_praktikum.id','periode_praktikum_dosen.periode_praktikum_id')
        ->join('dosen_laboratorium','dosen_laboratorium.id','periode_praktikum_dosen.dosen_laboratorium_id')
        ->where('periode_praktikum_dosen.kuota','!=','0')
        ->where('periode_praktikum_dosen.periode_praktikum_id', $praktikum->id)
        ->where('dosen_laboratorium.laboratorium_id',$idLab)
        ->orderBy('periode_praktikum_dosen.kuota', 'DESC')
        ->get();

        $idDosen = null; 
        foreach($dosen as $dos){
            if($kelas->jenis_kelas_id == $dos->jenis_kelas_id){
                $idDosen = $dos->id;
                break;
            }
        }

        if($idDosen == null){
            return null;
        }

        $aslab = DB::table('periode_praktikum_aslab')
        ->select('periode_praktikum_aslab.*','aslab.username')
        ->join('periode_praktikum','periode_praktikum.id','periode_praktikum_aslab.periode_praktikum_id')
        ->join('aslab','aslab.id','periode_praktikum_aslab.aslab_id')
        ->where('periode_praktikum_aslab.kuota','!=','0')
        ->where('periode_praktikum_aslab.periode_praktikum_id', $praktikum->id)
        ->where('aslab.laboratorium_id',$idLab)
        ->orderBy('periode_praktikum_aslab.kuota', 'DESC')
        ->get();


        $idAslab = null; 
        foreach($aslab as $a){
            if($a->jenis_kelas_id == 3){
                $idAslab = $a->id;
                break;
            }else if($a->jenis_kelas_id == $kelas->jenis_kelas_id){
                $idAslab = $a->id;
                break;
            }
        }


        $hasilAslab = AslabAsdos::aslab($aslab,array('06.2017.1.06883','06.2017.1.06821','06.2017.1.06852'), array('06.2018.1.06989','06.2018.1.07003','06.2018.1.07041','06.2018.1.06975','06.2018.1.07014','06.2018.1.07075','06.2018.1.06789'), $praktikan->mahasiswa_id,false);
        if($hasilAslab == null){
            $hasilAslab2 = AslabAsdos::aslab($aslab,array('06.2017.1.06803'), array('06.2018.1.06993','06.2018.1.07057','06.2018.1.07037','06.2018.1.07032'), $praktikan->mahasiswa_id,true);
            if($hasilAslab2 != null){
                $idAslab = $hasilAslab2->id;    
            }
        }else{
            $idAslab = $hasilAslab->id;
        }


        
        if($idAslab == null){
            return null;
        }

        

        $kurangiKuotaDosen = Periode_praktikum_dosen::findOrFail($idDosen);
        $kurangiKuotaDosen->kuota = $kurangiKuotaDosen->kuota - 1;
        $kurangiKuotaDosen->save();

        $kurangiKuotaAslab = Periode_praktikum_aslab::findOrFail($idAslab);
        $kurangiKuotaAslab->kuota = $kurangiKuotaAslab->kuota - 1;
        $kurangiKuotaAslab->save();

        return array('Dosen'=>$idDosen,'Aslab'=>$idAslab);
    }

    public static function aslab($aslab,$listAslab,$listPraktikan, $id, $cek){
        $mahasiswa = Mahasiswa::find($id);
        if($mahasiswa != null){
            if(in_array($mahasiswa->npm,$listPraktikan)){
                $hasilAslab = null;
                foreach($aslab as $a){
                    foreach($listAslab as $b){
                        if($a->username == $b){
                            $hasilAslab = $a;
                            break;
                        }
                    }
                }
                return $hasilAslab;
            }
        }
    }
}