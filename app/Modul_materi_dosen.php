<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul_materi_dosen extends Model
{
    protected $table = 'modul_materi_dosen';
    protected $fillable = ["modul_dosen_id","materi_id"];

    // public function modul(){
    //     return $this->belongsTo(Modul::class,'modul_dosen_id');
    // }

    // public function materi(){
    //     return $this->belongsTo(Materi::class,'materi_id');
    // }
}
