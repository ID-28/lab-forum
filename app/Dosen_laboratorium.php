<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen_laboratorium extends Model
{
    protected $table = 'dosen_laboratorium';
    protected $fillable = ["laboratorium_id","dosen_id"];
}
