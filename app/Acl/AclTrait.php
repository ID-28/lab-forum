<?php

namespace App\Acl;
use App\Peran;
use App\Izin;


trait AclTrait{
   
    // cek user berdasarkan peran atau izin 
    public function hasIzinTo($izins){
        return $this->hasIzinThroughPeran($izins) || $this->hasIzin($izins);
    }
    protected function hasIzin($izins){
        return (bool) $this->izin->where('nama',$izins->nama)->count();
    }
    protected function hasIzinThroughPeran($izins){
        foreach ($izins->peran as $e) {
            if($this->peran->contains($e)){
                return true;
            }
        }
        return false;
    }

    public function hasPeran(...$perans){
        foreach ($perans as $e ) {
            if($this->peran->contains('nama' , $e)){
                return true;
            }
        }
        return false;
    }

    public function izin(){
        return $this->belongsToMany(Izin::class , 'user_izin');
    }
    
    public function peran(){
        return $this->belongsToMany(Peran::class , 'user_peran');
    }
}