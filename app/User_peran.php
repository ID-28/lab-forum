<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_peran extends Model
{
    protected $table = 'user_peran';
    protected $primaryKey = [
        'user_id',
        'peran_id'
    ];
    public $incrementing = false;
    protected $fillable = ['user_id','peran_id'];

}
