<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laboratorium extends Model
{
    protected $table = 'laboratorium';
    public $primaryKey = 'id';
    protected $fillable = ["nama","ruangan"];

}
