<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Acl\AclTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,AclTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user';
    public $primaryKey = 'id';
    protected $fillable = [
        'jenis_user_id','username','password',
    ];

    public function mahasiswa(){
        return $this->hasOne('App\Mahasiswa', 'npm','username');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

//    public function setPasswordAttribute($password)
//    {
//        if ( !empty($password) ) {
//            $this->attributes['password'] = bcrypt($password);
//        }
//    }
}
