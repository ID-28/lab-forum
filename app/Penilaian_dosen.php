<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian_dosen extends Model
{
    protected $table = 'penilaian_dosen';
    public $primaryKey = 'id';
    protected $fillable = ["periode_praktikum_dosen_id","praktikan_id","nilai_akhir"];

}
