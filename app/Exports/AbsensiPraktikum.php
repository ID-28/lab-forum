<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use \App\Praktikan;
use \App\Sesi;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;

class AbsensiPraktikum implements FromView, WithTitle
{   

    private $idPeriodePraktikum;

    public function __construct(int $idPeriodePraktikum){
        $this->idPeriodePraktikum = $idPeriodePraktikum;
    }
    
    public function view(): View
    {
        $praktikan = DB::table('praktikan')
                    ->select('mahasiswa.nama','mahasiswa.npm','periode_praktikum.tahun','praktikum.nama as namaPraktikum')
                    ->join('periode_praktikum','periode_praktikum.id','praktikan.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','periode_praktikum.praktikum_id')
                    ->join('mahasiswa','mahasiswa.id','praktikan.mahasiswa_id')
                    ->where('periode_praktikum_id', $this->idPeriodePraktikum)
                    ->orderBy('sesi_id', 'ASC')
                    ->get();

        $namaPraktikum  = $praktikan->first()->namaPraktikum;
        $tahunPraktikum  = $praktikan->first()->tahun;

        $materiAslab = DB::table('modul_aslab')
        ->select('modul_aslab.id')
        ->where('modul_aslab.periode_praktikum_id',$this->idPeriodePraktikum)
        ->get();

        $jumlahModul = count($materiAslab);

        return view('admin.excelAbsensiPraktikum', [
            'praktikan' => $praktikan,
            'namaPraktikum' => $namaPraktikum,
            'tahunPraktikum' => $tahunPraktikum,
            'jumlahModul' => $jumlahModul
        ]);
    }
    
    public function title(): string
    {
        return 'Absensi Praktikum';
    }


}
