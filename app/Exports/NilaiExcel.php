<?php
namespace App\Exports;

use App\Penilaian_dosen;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;


class NilaiExcel implements FromView
{
    private $id;
    private $praktikum;
    private $praktikan;
    private $banyakModul;
    private $avg;
    private $nilaiAkhir;

    public function __construct($id,$praktikum,$praktikan,$banyakModul,$avg,$nilaiAkhir)
    {
        $this->id = $id;
        $this->praktikum = $praktikum;
        $this->praktikan = $praktikan;
        $this->banyakModul = $banyakModul;
        $this->avg = $avg;
        $this->nilaiAkhir = $nilaiAkhir;
    }

    public function view(): View
    {
        return view('admin.nilaiExcel', [
            'id' => $this->id,
            'praktikum' => $this->praktikum,
            'praktikan' => $this->praktikan,
            'banyakModul' => $this->banyakModul,
            'avg' => $this->avg,
            'nilaiAkhir' => $this->nilaiAkhir,

        ]);
    }
}
