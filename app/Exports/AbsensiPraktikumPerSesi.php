<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use \App\Praktikan;
use \App\Sesi;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;

class AbsensiPraktikumPerSesi implements FromView, WithTitle
{   

    private $idSesi;
    private $nomor;
    private $idPeriodePraktikum;

    public function __construct(int $idPeriodePraktikum,int $idSesi,int $nomor){
        $this->idSesi = $idSesi;
        $this->nomor = $nomor;
        $this->idPeriodePraktikum = $idPeriodePraktikum;
    }
    
    public function view(): View
    {
        $sesi = DB::table('sesi')
                ->select('sesi.nama as namaSesi','sesi.waktu','periode_praktikum.tahun','praktikum.nama')
                ->join('periode_praktikum','periode_praktikum.id','sesi.periode_praktikum_id')
                ->join('praktikum','praktikum.id','periode_praktikum.praktikum_id')
                ->where('sesi.id',$this->idSesi)
                ->first();

        $praktikan = DB::table('praktikan')
                    ->select('mahasiswa.nama','mahasiswa.npm')
                    ->join('periode_praktikum','periode_praktikum.id','praktikan.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','periode_praktikum.praktikum_id')
                    ->join('mahasiswa','mahasiswa.id','praktikan.mahasiswa_id')
                    ->where('periode_praktikum_id', $this->idPeriodePraktikum)
                    ->where('sesi_id', $this->idSesi)
                    ->get();

        return view('admin.excelAbsensiPraktikum', [
            'praktikan' => $praktikan,
            'sesi' => $sesi,
        ]);
    }
    
    public function title(): string
    {
        return 'Sesi ' . $this->nomor;
    }


}
