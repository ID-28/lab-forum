<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use \App\Praktikan;
use \App\Sesi;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;

class AbsensiPraktikanAslab implements FromView, WithTitle
{   

    private $idAslab;

    public function __construct(int $idAslab){
        $this->idAslab = $idAslab;
    }
    
    public function view(): View
    {
        $praktikan = DB::table('penilaian_aslab')
            ->select('mahasiswa.id as idMahasiswa','mahasiswa.npm as npm','mahasiswa.nama as namaMahasiswa','sesi.nama as namaSesi','sesi.waktu as waktu','praktikan.id as praktikanID')
            ->join('praktikan','praktikan.id','penilaian_aslab.praktikan_id')
            ->join('mahasiswa','mahasiswa.id','praktikan.mahasiswa_id')
            ->join('sesi','sesi.id','praktikan.sesi_id')
            ->join('periode_praktikum','periode_praktikum.id','sesi.periode_praktikum_id')
            ->join('periode_praktikum_aslab','periode_praktikum_aslab.id','penilaian_aslab.periode_praktikum_aslab_id')
            ->where('periode_praktikum.status',1)
            ->where('periode_praktikum_aslab.aslab_id',$this->idAslab)
            ->get();

        // dd($praktikan);

        return view('aslab.excelAbsensiPraktikanAslab', compact('praktikan'));
    }
    
    public function title(): string
    {
        return 'Absensi Praktikum';
    }


}