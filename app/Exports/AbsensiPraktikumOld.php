<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;

class AbsensiPraktikumOld implements WithMultipleSheets
{
    use Exportable;
    
    
    public function sheets(): array
    {

        $idLab = \Auth::user()->id-1;

        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
        ->where('periode_praktikum.status',1)
        ->first();

        $sesi = DB::table('sesi')
        ->select('sesi.*')
        ->where('sesi.periode_praktikum_id',$praktikum->id)
        ->get();

        $sheets = [];
        $nomor = 1;
        foreach ($sesi as $s){
            $sheets[]   = new AbsensiPraktikumPerSesi($praktikum->id,$s->id,$nomor);
            $nomor++;
        }

        return $sheets;
    }
}
