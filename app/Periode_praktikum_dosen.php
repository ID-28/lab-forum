<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periode_praktikum_dosen extends Model
{
    protected $table = 'periode_praktikum_dosen';
    public $primaryKey = 'id';
    protected $fillable = ["dosen_laboratorium_id","periode_praktikum_id","jenis_kelas_id","kuota"];

    public function periode_praktikum() {
        return $this->belongsTo(Periode_praktikum::class, 'periode_praktikum_id');
    }
    public function jenis_kelas() {
        return $this->belongsTo(Jenis_kelas::class, 'jenis_kelas_id');
    }

}


