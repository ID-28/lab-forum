<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';
    public $primaryKey = 'id';
    protected $fillable = ["user_id","judul","deskripsi","gambar"];
}