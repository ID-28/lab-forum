<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran_izin extends Model
{
    protected $table = 'peran_izin';
    public $primaryKey = 'id';
    protected $fillable = ["peran_id","izin_id"];

}
