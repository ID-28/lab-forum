<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    public $primaryKey = 'id';
    protected $fillable = ["npm","password","nama","no_tlpn","foto"];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
