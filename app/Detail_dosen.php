<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_dosen extends Model
{
    protected $table = 'detail_dosen';
    public $primaryKey = 'id';
    protected $fillable = ["penilaian_dosen_id","modul_id","nilai_modul"];
}
