<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Izin;

class AclServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Izin::get()->map(function($izin){
        //     Gate::define($izin->nama, function($user) use ($izin){
        //         return $user->hasIzinTo($izin);
        //     });
        // });


        // @peran('admin')
        // @endperan
        // Blade::if('peran' , function($perans){
        //     return auth()->user()->hasPeran($perans);
        // });
    }
}
