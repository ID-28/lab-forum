<?php

namespace App\Http\Middleware;

use Closure;

class PeranMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $peran, $izin=null)
    {
        if(! $request->user()->hasPeran($peran))
        {
            // return redirect()->route('home');
            abort(404);
        }
        return $next($request);
    }
}
