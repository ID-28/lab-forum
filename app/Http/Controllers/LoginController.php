<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Mahasiswa;
use App\User;
use App\Aslab;
use App\User_peran;
use Session;



class LoginController extends Controller
{
    public function loginAdmin(Request $req)
    {
        $user = User::where('username', $req->username)->first();
        if ($user) {
            if (Auth::attempt(['username'=>$req->username,'password'=>$req->password,'jenis_user_id'=>2])) {
                return \Redirect::route('admin');
            }
    
            return redirect()->back()->withInput($req->only('username'))->withErrors("Username Atau Password Salah");
        } else {
            return redirect()->back()->withErrors("Username Belum Terdaftar");
        }
    }

    public function loginDosen(Request $req)
    {
        $user = User::where('username', $req->username)->first();
        if ($user) {
            if (Auth::attempt(['username'=>$req->username,'password'=>$req->password,'jenis_user_id'=>3])) {
                return \Redirect::route('dosen');
            }
    
            return redirect()->back()->withInput($req->only('username'))->withErrors("NIP Atau Password Salah");
        } else {
            return redirect()->back()->withErrors("NIP belum terdaftar");
        }
    }

    public function logout()
    {
        Auth::logout();
        session_start();
        if(isset($_SESSION['aslab'])){
            session_unset('aslab');
        }
        session_destroy();
        return redirect()->route('home');
    }

    public function registerPraktikan(Request $req)
    {
        $this->validate($req, [
            'npm' => 'required|min:15|max:15|unique:mahasiswa,npm',
            'nama' => 'required|min:3',
            'password' => 'required|min:6',
            'password_confirm' => 'required_with:password|same:password|min:6',
            'telp' => 'required',
            'foto' => 'required|mimes:jpeg,jpg,png|max:20000'
        ]);

        $foto = $req->file('foto');
        $nama_file = str_replace(".", "-", $req->get('npm')) . "." . $foto->getClientOriginalExtension();
        if ($foto->move('upload/foto', $nama_file)) {
            // Ini jenis usernya belum bener
            $mahasiswaBaru = Mahasiswa::create([
                'jenis_kelas_id' => 1,
                'npm' => $req->npm,
                'nama' => ucfirst($req->nama),
                'no_tlpn' => $req->telp,
                'foto' => $nama_file
            ]);

            $userBaru = User::create([
                'jenis_user_id' => 5,
                'username' => $mahasiswaBaru->npm,
                'password'  => Hash::make($req->password),
            ]);


            $userDanMahasiswa = User_peran::create([
                'user_id' => $userBaru->id,
                'peran_id' => 5,
            ]);


            if ($userDanMahasiswa) {
                return redirect()->route('login.praktikan.view')->with(['jenis' => 'success','pesan' => 'Akun Berhasil dibuat, Silahkan Login']);
            }
            return redirect()->back()->with(['jenis' => 'danger','pesan' => 'Gagal Membuat Akun']);
        } else {
            return redirect()->back()->with(['jenis' => 'danger','pesan' => 'Gagal Upload Foto']);
        }
    }

    public function loginPraktikan(Request $req)
    {
        $user = User::where('username', $req->npm)->first();
        if ($user) {
            if (Auth::attempt(['username'=>$req->npm,'password'=>$req->password,'jenis_user_id'=>5])) {
                return \Redirect::route('praktikan');
            }
    
            return redirect()->back()->withInput($req->only('username'))->withErrors("NPM atau Password Salah");
        } else {
            return redirect()->back()->withErrors("NPM belum terdaftar");
        }
    }

    public function loginAslab(Request $req)
    {
        $user = Aslab::where('username', $req->npm)->first();
        if ($user) {
            if (Hash::check($req->password,$user->password)) {
                session_start();
                $_SESSION['aslab'] = $user;
                return \Redirect::route('aslab');
            }else{
                return redirect()->back()->withInput($req->only('username'))->withErrors("NPM atau Password Salah");
            }
        } else {
            return redirect()->back()->withErrors("NPM belum terdaftar");
        }
    }
}
