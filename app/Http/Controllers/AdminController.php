<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Dosen;
use App\Dosen_laboratorium;
use App\Materi;
use App\Modul_dosen;
use App\Modul_aslab;
use App\Modul_materi_dosen;
use App\Modul_materi_aslab;
use App\Periode_praktikum;
use App\Jenis_kelas;
use App\Laboratorium;
use App\Praktikum;
use App\Penilaian_dosen;
use App\Penilaian_aslab;
use App\User;
use App\User_peran;
use App\Praktikan;
use App\Mahasiswa;
use App\Aslab;
use App\Asal_kelas;
use App\Sesi;
use App\Periode_praktikum_aslab;
use App\Periode_praktikum_dosen;
use App\Berita;
use App\Detail_materi_dosen;
use App\Detail_materi_aslab;
use AslabAsdos;
use Session;
use DateTime;
use PDF;
use App\Exports\AbsensiPraktikum;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;
use App\Exports\NilaiExcel;



class AdminController extends Controller
{
    public function index(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
        
        $periodeAslab = DB::table('periode_praktikum_aslab')
                    ->select('periode_praktikum_aslab.id as id','periode_praktikum_aslab.kuota','periode_praktikum.tahun','praktikum.nama as namaPraktikum','aslab.nama','aslab.username', 'jenis_kelas.nama as pagiMalam','jenis_kelas.id as idPagiMalam')
                    ->join('periode_praktikum','periode_praktikum.id','=','periode_praktikum_aslab.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
                    ->join('aslab','aslab.id','=','periode_praktikum_aslab.aslab_id')
                    ->join('jenis_kelas','jenis_kelas.id','=','periode_praktikum_aslab.jenis_kelas_id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status', 1)
                    ->get();
        
        $periodeDosen = DB::table('periode_praktikum_dosen')
                    ->select('periode_praktikum_dosen.id','periode_praktikum_dosen.kuota','periode_praktikum.tahun','praktikum.nama as namaPraktikum','dosen.nama','dosen.nip', 'jenis_kelas.nama as pagiMalam', 'jenis_kelas.id as idPagiMalam')
                    ->join('periode_praktikum','periode_praktikum.id','=','periode_praktikum_dosen.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
                    ->join('dosen_laboratorium','periode_praktikum_dosen.dosen_laboratorium_id','=','dosen_laboratorium.id')
                    ->join('dosen','dosen_laboratorium.dosen_id','=','dosen.id')
                    ->join('jenis_kelas','jenis_kelas.id','=','periode_praktikum_dosen.jenis_kelas_id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status', 1)
                    ->get();

        
        $modulDosen = DB::table('modul_dosen')
                    ->select('modul_dosen.id','modul_dosen.nama as namaModul', 'praktikum.nama as namaPraktikum' ,'periode_praktikum.tahun')
                    ->join('periode_praktikum','modul_dosen.periode_praktikum_id','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                    ->where([['praktikum.laboratorium_id',\Auth::user()->id-1],['periode_praktikum.status',1]])
                    ->get();

        $modulAslab = DB::table('modul_aslab')
                    ->select('modul_aslab.id','modul_aslab.nama as namaModul', 'praktikum.nama as namaPraktikum' ,'periode_praktikum.tahun')
                    ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                    ->where([['praktikum.laboratorium_id',\Auth::user()->id-1],['periode_praktikum.status',1]])
                    ->get();
        
        $materi = DB::table('materi')
                    ->select('materi.id','materi.nama', 'praktikum.nama as namaPraktikum','praktikum.id as idPraktikum')
                    ->join('praktikum','materi.praktikum_id','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->get();
                    
        return view('admin.admin', compact('praktikum','periodeDosen','periodeAslab','modulDosen','modulAslab','materi'));
    }

    // ===================
    // START DOSEN
    // ===================

    public function management_dosens(){
        return view('admin.dosen');
    }

    public function dosen_store(Request $req){
        $this->validate($req, [
            'nip' => 'required|min:5',
            'nama_dosen' => 'required|min:5',
            'password' => 'required|min:6',
            'password_confirm' => 'required_with:password|same:password|min:6',
            'no_tlpn' => 'required',
        ]);

        $dosen = Dosen::create([
            'nama' => $req->nama_dosen,
            'password' => Hash::make($req->password),
            'no_tlpn' => $req->no_tlpn,
            'nip' => $req->nip,
        ]);
        
        for ($i=1; $i <=3 ; $i++) { 
            Dosen_laboratorium::create([
                'laboratorium_id' => $i,
                'dosen_id' => $dosen->id,
            ]);
        }

        $user = User::create([
            'jenis_user_id' => $req->jenis_user,
            'username' => $dosen->nip,
            'password'  => Hash::make($req->password),
        ]);
        
        User_peran::create([
            'user_id' => $user->id,
            'peran_id' => 3,
        ]);

        return redirect()->back()->with(['success' => 'Dosen Baru Berhasil Ditambahkan']);
    }

    public function dosen_delete($id){
        $nip_dosen = Dosen::select('nip')->where('id',$id)->first();
        $id_dosen_peran = User::select('id')->where('username',$nip_dosen->nip)->first();

        User_peran::where('user_id',$id_dosen_peran->id)->delete();
        User::where('username',$nip_dosen->nip)->delete();
        Dosen::destroy($id);
        return redirect()->back()->with(['success' => 'Dosen Berhasil Dihapus']);
    }
    
    public function dosen_update(Request $req, $id){
        $nip_dosen = Dosen::select('nip')->where('id',$id)->first();
        $id_user = User::select('id')->where('username','=',$nip_dosen->nip)->first();
        $password = Dosen::select('password')->where('id',$id)->first();

        $this->validate($req, [
            'unip' => 'required|min:5',
            'unama_dosen' => 'required|min:5',
            'uno_tlpn' => 'required',
        ]);

        Dosen::find($id)->update([
            'nip' => $req->unip,
            'nama' => $req->unama_dosen,
            'password' => $password->password,
            'no_tlpn' => $req->uno_tlpn,
        ]);

        $user = User::findOrFail($id_user->id);
        $user->username = $req->unip;
        $user->save();
        
        if($req->password != null){
            $this->validate($req,[
                'password' => 'required|min:6',
                'password-confirm' => 'required_with:password|same:password|min:6',
            ]);
            $dosen->password = Hash::make($req->password);
            $dosen->save();

            $user->password = Hash::make($req->password);
            $user->save(); 
        }
        return redirect()->back()->with(['success' => 'Dosen Berhasil Diubah']);
    }

    // ===================
    // END DOSEN
    // ===================

    // ===================
    // START MODUL DOSEN
    // ===================

    public function management_modul_dosen(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
                
        $modul_dosen = DB::table('modul_dosen')
                ->select('modul_dosen.id','modul_dosen.nama as namaModul', 'praktikum.nama as namaPraktikum' ,'periode_praktikum.tahun')
                ->join('periode_praktikum','modul_dosen.periode_praktikum_id','periode_praktikum.id')
                ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                ->where([['praktikum.laboratorium_id',\Auth::user()->id-1],['periode_praktikum.status',1]])
                ->get();

        return view('admin.modulDosen', compact('praktikum','modul_dosen'));
    }

    public function modul_store_dosen(Request $req){
        for ($i=1; $i<=$req->jumlah; $i++) { 
            Modul_dosen::create([
                'nama' => 'Modul '.$i,
                'periode_praktikum_id' => $req->praktikum,
            ]);
        }

        return redirect()->back()->with(['success' => 'Modul Dosen Baru Berhasil Ditambahkan']);
    }

    public function modul_delete_dosen($id){
        Modul_dosen::destroy($id);
        return redirect()->back()->with(['success' => 'Modul Dosen Berhasil Dihapus']);
    }

    public function modul_update_dosen(Request $req, $id){
        $this->validate($req, [
            'unama' => 'required|min:5',
        ]);

        Modul_dosen::find($id)->update([
            'praktikum_id' => $req->upraktikum,
            'nama' => $req->unama,
        ]);
        return redirect()->back()->with(['success' => 'Modul Dosen Berhasil Diubah']);
    }

    // ===================
    // END MODUL DOSEN
    // ===================

    // ===================
    // START MODUL ASLAB
    // ===================

    public function management_modul_aslab(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
                
        $modulAslab = DB::table('modul_aslab')
                ->select('modul_aslab.id','modul_aslab.nama as namaModul', 'praktikum.nama as namaPraktikum' ,'periode_praktikum.tahun')
                ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
                ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                ->where([['praktikum.laboratorium_id',\Auth::user()->id-1],['periode_praktikum.status',1]])
                ->get();

        return view('admin.modulAslab', compact('praktikum','modulAslab'));
    }

    public function modul_store_aslab(Request $req){
        for ($i=1; $i<=$req->jumlah; $i++) { 
            Modul_aslab::create([
                'nama' => 'Modul '.$i,
                'periode_praktikum_id' => $req->praktikum,
            ]);
        }

        return redirect()->back()->with(['success' => 'Modul Aslab Baru Berhasil Ditambahkan']);
    }

    public function modul_delete_aslab($id){
        Modul_aslab::destroy($id);
        return redirect()->back()->with(['success' => 'Modul Berhasil Dihapus']);
    }

    public function modul_update_aslab(Request $req, $id){
        $this->validate($req, [
            'unama' => 'required|min:5',
        ]);

        Modul_aslab::find($id)->update([
            'praktikum_id' => $req->upraktikum,
            'nama' => $req->unama,
        ]);
        return redirect()->back()->with(['success' => 'Modul Berhasil Diubah']);
    }

    // ===================
    // END MODUL ASLAb
    // ===================

    // ===================
    // START MATERI
    // ===================

    public function management_materis(){        
        $materi = DB::table('materi')
                ->select('materi.id','materi.nama', 'praktikum.nama as namaPraktikum','praktikum.id as idPraktikum')
                ->join('praktikum','materi.praktikum_id','praktikum.id')
                ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                ->get();

        $daftarPraktikum = Praktikum::where('laboratorium_id', \Auth::user()->id-1)->get();

        $praktikum = DB::table('praktikum')
                    ->select('praktikum.nama')
                    ->where('laboratorium_id', \Auth::user()->id-1)
                    ->get();
                    
        return view('admin.materi', compact('materi','daftarPraktikum','praktikum'));
    }

    public function materi_store(Request $req){
        $this->validate($req, [
            'nama' => 'required|min:5',
            'praktikum' => 'required'
        ]);

        Materi::create([
            'praktikum_id' => $req->praktikum,
            'nama' => $req->nama,
        ]);

        return redirect()->back()->with(['success' => 'Materi Baru Berhasil Ditambahkan']);
    }

    public function materi_delete($id){
        Materi::destroy($id);
        return redirect()->back()->with(['success' => 'Materi baru Berhasil Dihapus']);
    }

    public function materi_update(Request $req, $id){
        $this->validate($req, [
            'unama' => 'required|min:5',
            'upraktikum' => 'required'
        ]);

        materi::find($id)->update([
            'praktikum_id' => $req->upraktikum,
            'nama' => $req->unama,
        ]);
        return redirect()->back()->with(['success' => 'Materi Baru Berhasil Diubah']);
    }

    // ===================
    // END MATERI
    // ===================

    // ===================
    // START PENILAIAN DOSEN
    // ===================

    public function management_penilaian_dosen(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.id as idPraktikum','praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
                
        $modulMateriDosen = Modul_materi_dosen::select('modul_materi_dosen.id','modul_dosen.nama as namaModul','materi.nama as namaMateri','praktikum.nama as namaPraktikum','periode_praktikum.tahun')
                            ->join('modul_dosen','modul_materi_dosen.modul_dosen_id','modul_dosen.id')
                            ->join('materi','modul_materi_dosen.materi_id','materi.id')
                            ->join('periode_praktikum','modul_dosen.periode_praktikum_id','periode_praktikum.id')
                            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                            ->where('periode_praktikum.status',1)
                            ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                            ->get();
        
        $modul = Modul_dosen::select('modul_dosen.id','modul_dosen.nama')
                            ->join('periode_praktikum','modul_dosen.periode_praktikum_id','periode_praktikum.id')
                            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                            ->leftJoin('modul_materi_dosen','modul_dosen.id','modul_materi_dosen.modul_dosen_id')
                            ->where('periode_praktikum.status',1)
                            ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                            ->whereNull('modul_materi_dosen.modul_dosen_id')
                            ->get();
                
        $materi = DB::table('materi')
                    ->select('materi.id','materi.nama')
                    ->whereNotIn('materi.id', DB::table('modul_materi_dosen')->join('modul_dosen','modul_materi_dosen.modul_dosen_id','modul_dosen.id')->join('periode_praktikum','periode_praktikum.id','modul_dosen.periode_praktikum_id')->where('periode_praktikum.status',1)->pluck('modul_materi_dosen.materi_id'))
                    ->where('materi.praktikum_id',$praktikum->idPraktikum)
                    ->get();
        
        $cekModul = Modul_dosen::select('modul_dosen.id')
                    ->join('periode_praktikum','modul_dosen.periode_praktikum_id','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                    ->where('periode_praktikum.status',1)
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->get();

        $cekMateri = Materi::select('materi.id')
                    ->join('praktikum','materi.praktikum_id','praktikum.id')
                    ->where('materi.praktikum_id',$praktikum->idPraktikum)
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->get();

        return view('admin.penilaian-dosen',compact('modulMateriDosen','praktikum','modul','materi','cekModul','cekMateri'));
    }

    public function penilaian_dosen_store(Request $req){
        for($i = 0; $i < count($req->materi); $i++){
            $data = array(
                'modul_dosen_id' => $req->modul,
                'materi_id' => $req->materi[$i],
                // 'nilai_modul' => 0,
            );
            $insertData[] = $data;
        }
        Modul_materi_dosen::insert($insertData);
        return redirect()->back()->with(['success' => 'Berhasil Mengatur Materi Dosen']);
    }

    public function penilaian_dosen_delete($id){
        Modul_materi_dosen::destroy($id);
        return redirect()->back()->with(['success' => 'Materi Penilaian Dosen Berhasil Dihapus']);
    }

    // ===================
    // END PENILAIAN DOSEN
    // ===================

    // ===================
    // START PENILAIAN ASLAB
    // ===================

    public function management_penilaian_aslab(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.id as idPraktikum','praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
                
        $modulMateriAslab = Modul_materi_aslab::select('modul_materi_aslab.id','modul_aslab.nama as namaModul','materi.nama as namaMateri','praktikum.nama as namaPraktikum','periode_praktikum.tahun')
                            ->join('modul_aslab','modul_materi_aslab.modul_aslab_id','modul_aslab.id')
                            ->join('materi','modul_materi_aslab.materi_id','materi.id')
                            ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
                            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                            ->where('periode_praktikum.status',1)
                            ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                            ->get();
        
        $modul = Modul_aslab::select('modul_aslab.id','modul_aslab.nama')
                            ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
                            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                            ->leftJoin('modul_materi_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
                            ->where('periode_praktikum.status',1)
                            ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                            ->whereNull('modul_materi_aslab.modul_aslab_id')
                            ->get();
        
        $materi = DB::table('materi')
                    ->select('materi.id','materi.nama')
                    ->whereNotIn('materi.id', DB::table('modul_materi_aslab')->join('modul_aslab','modul_materi_aslab.modul_aslab_id','modul_aslab.id')->join('periode_praktikum','periode_praktikum.id','modul_aslab.periode_praktikum_id')->where('periode_praktikum.status',1)->pluck('modul_materi_aslab.materi_id'))
                    ->where('materi.praktikum_id',$praktikum->idPraktikum)
                    ->get();
        
        $cekModul = Modul_aslab::select('modul_aslab.id')
                ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
                ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                ->where('periode_praktikum.status',1)
                ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                ->get();

        $cekMateri = Materi::select('materi.id')
                ->join('praktikum','materi.praktikum_id','praktikum.id')
                ->where('materi.praktikum_id',$praktikum->idPraktikum)
                ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                ->get();

        return view('admin.penilaian-aslab',compact('modulMateriAslab','praktikum','modul','materi','cekModul','cekMateri'));
    }

    public function penilaian_aslab_store(Request $req){
        for($i = 0; $i < count($req->materi); $i++){
            $data = array(
                'modul_aslab_id' => $req->modul,
                'materi_id' => $req->materi[$i],
                // 'nilai_modul' => 0,
            );
            $insertData[] = $data;
        }
        Modul_materi_aslab::insert($insertData);
        return redirect()->back()->with(['success' => 'Berhasil Mengatur Materi Asisten Lab']);
    }

    public function penilaian_aslab_delete($id){
        Modul_materi_aslab::destroy($id);
        return redirect()->back()->with(['success' => 'Materi Penilaian Dosen Berhasil Dihapus']);
    }

    // ===================
    // END PENILAIAN Aslab
    // ===================

    // ===================
    // START AKTIF DOSEN
    // ===================
    public function periodeDosenView(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();

                    // dd(\Auth::user()->id-1);
        $dosen = DB::table('dosen')
                ->select('dosen.nama','dosen.nip','dosen_laboratorium.id')
                ->join('dosen_laboratorium','dosen.id','dosen_laboratorium.dosen_id')
                ->where('dosen_laboratorium.laboratorium_id','=',\Auth::user()->id-1)
                ->whereNotIn('dosen_laboratorium.id', DB::table('periode_praktikum_dosen')->where('periode_praktikum_dosen.periode_praktikum_id',$praktikum->id)->pluck('periode_praktikum_dosen.dosen_laboratorium_id'))
                ->get();
        

        $periodeDosen = DB::table('periode_praktikum_dosen')
                    ->select('periode_praktikum_dosen.id','periode_praktikum_dosen.kuota','periode_praktikum.tahun','praktikum.nama as namaPraktikum','dosen.nama','dosen.nip', 'jenis_kelas.nama as pagiMalam', 'jenis_kelas.id as idPagiMalam')
                    ->join('periode_praktikum','periode_praktikum.id','=','periode_praktikum_dosen.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
                    ->join('dosen_laboratorium','periode_praktikum_dosen.dosen_laboratorium_id','=','dosen_laboratorium.id')
                    ->join('dosen','dosen_laboratorium.dosen_id','=','dosen.id')
                    ->join('jenis_kelas','jenis_kelas.id','=','periode_praktikum_dosen.jenis_kelas_id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status', 1)
                    ->get();

        $cekDosen = Dosen::select('dosen.id')
                    ->join('dosen_laboratorium','dosen.id','dosen_laboratorium.dosen_id')
                    // ->join('periode_praktikum_dosen','dosen_laboratorium.id','periode_praktikum_dosen.id')
                    // ->join('periode_praktikum','periode_praktikum_dosen.periode_praktikum_id','periode_praktikum.id')
                    // ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                    // ->join('laboratorium','praktikum.laboratorium_id','laboratorium.id')
                    ->where('dosen_laboratorium.laboratorium_id','=',\Auth::user()->id-1)
                    ->get();

                    // dd(count($cekDosen));

        return view('admin.periodeDosen' ,compact('praktikum','dosen','periodeDosen','cekDosen'));
    }
    public function periodeDosenProses(Request $req){      
        $this->validate($req, [
            'periodePraktikumId' => 'required',
            'dosen' => 'required',
            'kuota' => 'required',
            'pagiMalam' => 'required',
        ]);

        // $cekPeriodePraktikumDosen = periode_praktikum_dosen::where('periode_praktikum_id',$req->periodePraktikumId)
        //                         ->where('dosen_id',$req->dosen)
        //                         ->get();

        // if(count($cekPeriodePraktikumDosen) > 0){
        //     return redirect()->back()->with(['warning' => 'Dosen Sudah Ada']); 
        // }
   
        periode_praktikum_dosen::create([
            'periode_praktikum_id' => $req->periodePraktikumId,
            'dosen_laboratorium_id' => $req->dosen,
            'kuota' => $req->kuota,
            'jenis_kelas_id' => $req->pagiMalam,
        ]);
        
        return redirect()->back()->with(['success' => 'Dosen Berhasil Diaktifkan']); 
    }

    public function periodeDosenUpdate(Request $req,$id){
        $this->validate($req, [
            'uKuota' => 'required',
        ]);

        Periode_praktikum_dosen::find($id)->update([
            'kuota' => $req->uKuota,
            'jenis_kelas_id' => $req->uPagiMalam,
        ]);

        return redirect()->back()->with(['success' => 'Dosen Berhasil Diubah']);
    }

    public function periodeDosenDelete($id){
        Periode_praktikum_dosen::destroy($id);
        return redirect()->back()->with(['success' => 'Dosen Berhasil Dinonaktifkan']);
    }
    // ===================
    // END AKTIF DOSEN
    // ===================

    // Change Password
    public function change_password(Request $req){

        if (Hash::check($req->old, Auth::user()->password)) {
            if($req->new1 == $req->new2){
                User::find(Auth::user()->id)->update([
                    'password' => Hash::make($req->new1),
                ]);
                return redirect()->back()->with(['success' => 'Password Berhasil Dirubah']);
            }else{
                return redirect()->back()->with(['error' => 'Password Baru Tidak Sama']);
            }
        }else{
            return redirect()->back()->with(['error' => 'Password Lama Tidak Sama']);
        }
    }

    // Lihat Nilai
    public function lihatNilai(){
        $asdos = DB::table('penilaian_dosen')
                ->select('periode_praktikum_dosen.dosen_laboratorium_id as idPeriodePraktikumDosen','dosen.nip as nip','dosen.nama as namaDosen','praktikum.nama as namaPraktikum','praktikum.id as idPraktikum','periode_praktikum_dosen.status as status',DB::raw('COUNT(penilaian_dosen.praktikan_id) as total'))
                ->join('periode_praktikum_dosen','penilaian_dosen.periode_praktikum_dosen_id','periode_praktikum_dosen.id')
                ->join('dosen_laboratorium','periode_praktikum_dosen.dosen_laboratorium_id','dosen_laboratorium.id')
                ->join('dosen','dosen_laboratorium.dosen_id','dosen.id')
                ->join('praktikan','penilaian_dosen.praktikan_id','praktikan.id')
                ->join('mahasiswa','praktikan.mahasiswa_id','mahasiswa.id')
                ->join('periode_praktikum','praktikan.periode_praktikum_id','periode_praktikum.id')
                ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                ->where('periode_praktikum.status',1)
                // ->where('periode_praktikum_dosen.status',1)
                ->groupBy('namaDosen')
                ->groupBy('namaPraktikum')
                ->groupBy('idPraktikum')
                ->groupBy('status')
                ->groupBy('nip')
                ->groupBy('idPeriodePraktikumDosen')
                ->get();
        
        $praktikum = DB::table('Periode_praktikum')
                    ->select('praktikum.nama','periode_praktikum.tahun')
                    ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
                    
        return view('admin.lihatNilai',compact('praktikum','asdos'));
    }

    public function excel($nip,$id){

        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.nama as namaPraktikum','periode_praktikum.tahun as tahun')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();

        $praktikan =DB::table('mahasiswa')
                    ->select('dosen.id as idDosen','mahasiswa.npm as npm','mahasiswa.nama as namaMahasiswa','sesi.nama as namaSesi','sesi.waktu as waktu','dosen.nama as namaDosen','penilaian_dosen.id as idPenilaian','praktikan.id as praktikanId','penilaian_dosen.nilai_akhir as nilaiAkhir')
                    ->join('praktikan','praktikan.mahasiswa_id','mahasiswa.id')
                    ->join('penilaian_dosen','praktikan.id','penilaian_dosen.praktikan_id')
                    ->join('periode_praktikum_dosen','penilaian_dosen.periode_praktikum_dosen_id','periode_praktikum_dosen.id')
                    ->join('dosen_laboratorium','periode_praktikum_dosen.dosen_laboratorium_id','dosen_laboratorium.id')
                    ->join('dosen','dosen_laboratorium.dosen_id','dosen.id')
                    ->join('periode_praktikum','praktikan.periode_praktikum_id','periode_praktikum.id')
                    ->join('sesi','praktikan.sesi_id','sesi.id')
                    ->where('periode_praktikum.praktikum_id',$id)
                    ->where('dosen.nip',$nip)
                    ->get();
        
        
        $banyakModul = DB::select('select count(*) as hitung from
                    (SELECT modul_dosen_id from modul_materi_dosen
                    JOIN materi on modul_materi_dosen.materi_id = materi.id
                    JOIN praktikum on materi.praktikum_id = praktikum.id
                    where praktikum_id ='.$id.'
                    GROUP by modul_dosen_id)
                    as hitung;');
                    
        $idModulMateri = DB::table('modul_materi_dosen')
                    ->select('modul_materi_dosen.id as idModul')
                    ->join('materi','modul_materi_dosen.materi_id','materi.id')
                    ->join('praktikum','materi.praktikum_id','praktikum.id')
                    ->where('praktikum_id',$id)
                    ->get();

        $avg = array();
        foreach($praktikan as $p){
            $nilaiPraktikan = array();
            for($i = 1; $i<=$banyakModul[0]->hitung;$i++){
                $nilai = Detail_materi_dosen::select(DB::raw('AVG(detail_materi_dosen.nilai_materi) as avg'))
                ->join('modul_materi_dosen','detail_materi_dosen.modul_materi_dosen_id','modul_materi_dosen.id')
                ->join('penilaian_dosen','detail_materi_dosen.penilaian_dosen_id','penilaian_dosen.id')
                ->join('materi','modul_materi_dosen.materi_id','materi.id')
                ->join('praktikum','materi.praktikum_id','praktikum.id')
                ->where('detail_materi_dosen.penilaian_dosen_id',$p->idPenilaian)
                ->where('penilaian_dosen.praktikan_id',$p->praktikanId)
                ->where('modul_materi_dosen.modul_dosen_id',$idModulMateri[$i-1]->idModul)
                ->where('materi.praktikum_id',$id)
                ->first();

                if($nilai){
                    array_push($nilaiPraktikan,$nilai->avg);
                }else{
                    array_push($nilaiPraktikan,'0');
                }
            }
            array_push($avg, $nilaiPraktikan);
        }

        $nilaiAkhir = DB::table('penilaian_dosen')
                    ->SELECT('penilaian_dosen.nilai_akhir as nilaiAkhir')
                    ->join('praktikan','penilaian_dosen.praktikan_id','praktikan.id')
                    ->join('periode_praktikum','praktikan.periode_praktikum_id','periode_praktikum.id')
                    ->join('mahasiswa','praktikan.mahasiswa_id','praktikan.id')
                    ->where('penilaian_dosen.periode_praktikum_dosen_id',$praktikan[0]->idDosen)
                    ->where('penilaian_dosen.praktikan_id',$praktikan[0]->praktikanId)
                    ->where('periode_praktikum.praktikum_id',$id)
                    ->where('mahasiswa.npm',$praktikan[0]->npm)
                    ->first();

        return Excel::download(new NilaiExcel($id,$praktikum,$praktikan,$banyakModul,$avg,$nilaiAkhir), 'nilai.xlsx');
    }

    public function reject($id){
        DB::table('periode_praktikum_dosen')
        ->where('periode_praktikum_dosen.dosen_laboratorium_id',$id)
        ->update(['periode_praktikum_dosen.status'=>0]);

        return redirect()->back()->with(['success' => 'Berhasil Reject Nilai Dosen']);
    }

    public function praktikanView(){
        $user = DB::table('user')
        ->select('user.id as userId','mahasiswa.npm','mahasiswa.nama')
        ->join('mahasiswa','mahasiswa.npm','=','user.username')
        ->where('jenis_user_id',5)
        ->get();

        return view('admin.praktikan', compact('user'));
    }

    public function praktikanViewDetail($id){
        $user = DB::table('user')
        ->select('user.id as userId','mahasiswa.*')
        ->join('mahasiswa','mahasiswa.npm','=','user.username')
        ->where('jenis_user_id',5)
        ->where('user.id',$id)
        ->first();

        return view('admin.praktikanViewDetail', compact('user'));
    }

    public function praktikanResetPassword(User $user){
        $user->password = Hash::make("12345678");
        $user->save();

        return redirect()->back()->with(['success' => 'Berhasil mereset sandi dengan sandi baru 12345678']);
    }

    public function praktikanDelete($id){
        $user = User::findOrFail($id);
        $mahasiswa = Mahasiswa::where('npm', $user->username)->first();
        $peran = User_peran::where('user_id',$user->id)->where('peran_id',5)->first();
        dd($peran);
        if($peran->delete() && $user->delete() && $mahasiswa->delete()){
            File::delete('/upload/foto/' . $mahasiswa->foto);
            return redirect()->back()->with(['success' => 'Berhasil menghapus praktikan']);
        }else{
            return redirect()->back()->with(['error' => 'Gagal menghapus praktikan']);
        }
    }

    public function verifikasiPraktikan(){
        $idLab = \Auth::user()->id-1;

        $praktikan = DB::table('praktikan')
        ->select('praktikan.id','praktikan.status','mahasiswa.npm','mahasiswa.nama','mahasiswa.foto','praktikum.nama as namaPraktikum','periode_praktikum.tahun','sesi.nama as namaSesi','sesi.waktu','praktikan.status','laboratorium.id as labId','laboratorium.nama as namaLab', 'laboratorium.ruangan as ruangLab')
        ->join('mahasiswa','mahasiswa.id','=','praktikan.mahasiswa_id')
        ->join('sesi','sesi.id','=','praktikan.sesi_id')
        ->join('periode_praktikum','periode_praktikum.id','=','sesi.periode_praktikum_id')
        ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
        ->join('laboratorium','laboratorium.id','=','praktikum.laboratorium_id')
        ->where('laboratorium.id',$idLab)
        ->where('periode_praktikum.status',1)
        ->get();
    
        return view('admin.verifikasiPraktikan',compact('praktikan'));
    }

    public function verifikasiPraktikanDetail($id){
        $praktikan = DB::table('praktikan')
        ->select('praktikan.id','mahasiswa.npm','mahasiswa.nama','mahasiswa.foto','praktikum.nama as namaPraktikum','periode_praktikum.tahun','sesi.nama as namaSesi','sesi.waktu','praktikan.status','laboratorium.id as labId','laboratorium.nama as namaLab', 'laboratorium.ruangan as ruangLab', 'asal_kelas.nama as namaKelas','jenis_kelas.nama as pagiMalam')
        ->join('mahasiswa','mahasiswa.id','=','praktikan.mahasiswa_id')
        ->join('sesi','sesi.id','=','praktikan.sesi_id')
        ->join('periode_praktikum','periode_praktikum.id','=','sesi.periode_praktikum_id')
        ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
        ->join('laboratorium','laboratorium.id','=','praktikum.laboratorium_id')
        ->join('asal_kelas','asal_kelas.id','=','praktikan.asal_kelas_id')
        ->join('jenis_kelas','jenis_kelas.id','=','asal_kelas.jenis_kelas_id')
        ->where('praktikan.id',$id)
        ->first();

        return view('admin.verifikasiPraktikanDetail',compact('praktikan'));
    }

    // send materi
    public function verifikasiPraktikanDetailProses(Praktikan $praktikan){
        
        $idPraktikum = DB::table('periode_praktikum')
                    ->select('praktikum.id as idPraktikum','praktikum.nama as namaPraktikum','periode_praktikum.tahun as tahun')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();

        $modulMateriDosen = Modul_materi_dosen::select('modul_materi_dosen.id')
                            ->join('modul_dosen','modul_materi_dosen.modul_dosen_id','modul_dosen.id')
                            ->join('periode_praktikum','modul_dosen.periode_praktikum_id','periode_praktikum.id')
                            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
                            ->where('periode_praktikum.status',1)
                            ->where('praktikum.id',$idPraktikum->idPraktikum)
                            ->get();


        $dosenAslab = $this->getDosenAslab($praktikan);

        if($dosenAslab == null){
            return redirect()->back()->with(['error' => 'Gagal memilih dosen dan aslab. Kuota dosen atau aslab sudah habis']);
        }

        Penilaian_aslab::create([
            'periode_praktikum_aslab_id' => $dosenAslab['Aslab'],
            'praktikan_id' => $praktikan->id,
            'nilai_akhir' => 0,
        ]);

        $penilaianDosen = Penilaian_dosen::create([
            'periode_praktikum_dosen_id' => $dosenAslab['Dosen'],
            'praktikan_id' => $praktikan->id,
            'nilai_akhir' => 0,
        ]);

        $praktikan->status = 1;
        $praktikan->save();

        // entri materi dosen
        $insertData = array();
        for($i = 0; $i<count($modulMateriDosen); $i++){
            $data = array(
                'penilaian_dosen_id' => $penilaianDosen->id,
                'modul_materi_dosen_id' => $modulMateriDosen[$i]->id,
                'nilai_materi' => 0,
            );
            $insertData[] = $data;
        }
        Detail_materi_dosen::insert($insertData);


        return redirect()->back()->with(['success' => 'Berhasil memverifikasi praktikan']);
    }

    public function getDosenAslab($praktikan){
        $kelas = Asal_kelas::findOrFail($praktikan->asal_kelas_id);
        $idLab = \Auth::user()->id-1;

        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',$idLab)
        ->where('periode_praktikum.status',1)
        ->first();

        $dosen = DB::table('periode_praktikum_dosen')
        ->select('periode_praktikum_dosen.*')
        ->join('periode_praktikum','periode_praktikum.id','periode_praktikum_dosen.periode_praktikum_id')
        ->join('dosen_laboratorium','dosen_laboratorium.id','periode_praktikum_dosen.dosen_laboratorium_id')
        ->where('periode_praktikum_dosen.kuota','!=','0')
        ->where('periode_praktikum_dosen.periode_praktikum_id', $praktikum->id)
        ->where('dosen_laboratorium.laboratorium_id',$idLab)
        ->orderBy('periode_praktikum_dosen.kuota', 'DESC')
        ->get();
        
        

        $idDosen = null; 
        foreach($dosen as $dos){
            if($kelas->jenis_kelas_id == $dos->jenis_kelas_id){
                $idDosen = $dos->id;
                break;
            }
        }

        if($idDosen == null){
            return null;
        }

        $aslab = DB::table('periode_praktikum_aslab')
        ->select('periode_praktikum_aslab.*')
        ->join('periode_praktikum','periode_praktikum.id','periode_praktikum_aslab.periode_praktikum_id')
        ->join('aslab','aslab.id','periode_praktikum_aslab.aslab_id')
        ->where('periode_praktikum_aslab.kuota','!=','0')
        ->where('periode_praktikum_aslab.periode_praktikum_id', $praktikum->id)
        ->where('aslab.laboratorium_id',$idLab)
        ->orderBy('periode_praktikum_aslab.kuota', 'DESC')
        ->get();


        $idAslab = null; 
        foreach($aslab as $a){
            if($a->jenis_kelas_id == 3){
                $idAslab = $a->id;
                break;
            }else if($a->jenis_kelas_id == $kelas->jenis_kelas_id){
                $idAslab = $a->id;
                break;
            }
        }

        if($idAslab == null){
            return null;
        }

        

        $kurangiKuotaDosen = Periode_praktikum_dosen::findOrFail($idDosen);
        $kurangiKuotaDosen->kuota = $kurangiKuotaDosen->kuota - 1;
        $kurangiKuotaDosen->save();

        $kurangiKuotaAslab = Periode_praktikum_aslab::findOrFail($idAslab);
        $kurangiKuotaAslab->kuota = $kurangiKuotaAslab->kuota - 1;
        $kurangiKuotaAslab->save();

        return array('Dosen'=>$idDosen,'Aslab'=>$idAslab);
    }

    public function deleteRegistrasiPraktikan(Praktikan $praktikan){
        $sesi = Sesi::where('id',$praktikan->sesi_id)->first();
        $sesi->kuota = $sesi->kuota+1;
        $sesi->save();

        $praktikan->delete();

        return redirect()->route('verifikasiPraktikan')->with(['success' => 'Berhasil menghapus registrasi praktikan']);
    }

    public function cetakKartuPraktikum(){
        $idLab = \Auth::user()->id-1;

        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',$idLab)
        ->where('periode_praktikum.status',1)
        ->first();

        $lab = Laboratorium::findOrfail($idLab);

        $praktikan = DB::table('praktikan')
                    ->select('mahasiswa.foto','mahasiswa.nama','mahasiswa.npm','sesi.waktu','aslab.nama as namaAslab','dosen.nama as namaDosen')
                    ->join('penilaian_aslab', 'penilaian_aslab.praktikan_id','praktikan.id')
                    ->join('periode_praktikum_aslab', 'penilaian_aslab.periode_praktikum_aslab_id','periode_praktikum_aslab.id')
                    ->join('aslab', 'periode_praktikum_aslab.aslab_id','aslab.id')
                    ->join('penilaian_dosen', 'penilaian_dosen.praktikan_id','praktikan.id')
                    ->join('periode_praktikum_dosen', 'penilaian_dosen.periode_praktikum_dosen_id','periode_praktikum_dosen.id')
                    ->join('dosen_laboratorium', 'dosen_laboratorium.id','periode_praktikum_dosen.dosen_laboratorium_id')
                    ->join('dosen', 'dosen.id','dosen_laboratorium.dosen_id')
                    ->join('sesi', 'sesi.id','praktikan.sesi_id')
                    ->join('mahasiswa', 'mahasiswa.id','praktikan.mahasiswa_id')
                    ->where('praktikan.periode_praktikum_id',$praktikum->id)
                    ->where('praktikan.status',1)
                    ->get();

        $materiAslab = DB::table('modul_aslab')
                    ->select('modul_aslab.id')
                    ->where('modul_aslab.periode_praktikum_id',$praktikum->id)
                    ->get();

        if(count($materiAslab) == 0){
            return redirect()->back()->with(['error' => 'Manajemen materi aslab harus diatur terlebih dahulu']);
        }

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 1000); 

        $pdf = PDF::loadView('admin.newKartuPraktikum', compact('praktikan','praktikum','idLab','lab','materiAslab'));
        $pdf->setPaper('A6', 'landscape');
        return $pdf->stream('KartuPraktikum.pdf');
        
        // return $pdf->download('KartuPraktikum.pdf');

        // return view('admin.newKartuPraktikum', compact('praktikan','praktikum','idLab','lab','modulAslab'));
    }

    public function cetakKartuPraktikumBelakang(){
        $idLab = \Auth::user()->id-1;

        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',$idLab)
        ->where('periode_praktikum.status',1)
        ->first();

        $lab = Laboratorium::findOrfail($idLab);

        $materiAslab = DB::table('modul_aslab')
                    ->select('modul_aslab.id')
                    ->where('modul_aslab.periode_praktikum_id',$praktikum->id)
                    ->get();

        $materiDosen = DB::table('modul_dosen')
        ->select('modul_dosen.id')
        ->where('modul_dosen.periode_praktikum_id',$praktikum->id)
        ->get();

        $jumlahTerbanyak = 0;
        if(count($materiAslab) > count($materiDosen)){
            $jumlahTerbanyak = count($materiAslab);
        }else{
            $jumlahTerbanyak = count($materiDosen);
        }

        $setengah = false;
        if(count($materiAslab) / count($materiDosen) == 2){
            $setengah = true;
        }

        $pdf = PDF::loadView('admin.newKartuPraktikumBelakang', compact('praktikum','idLab','lab','materiAslab','materiDosen','jumlahTerbanyak','setengah'));
        $pdf->setPaper('A6', 'landscape');
        return $pdf->stream('KartuPraktikum.pdf');
    }

    public function manajemenAslabView(){
        $aslab = Aslab::where('laboratorium_id',\Auth::user()->id-1)->get();
        return view('admin.aslab', compact('aslab'));
    }

    public function manajemenAslabProses(Request $req){
        $this->validate($req, [
            'npm' => 'required|min:15|max:15',
            'nama' => 'required|min:3',
            'password' => 'required|min:6',
            'password-confirm' => 'required_with:password|same:password|min:6',
            'no_tlpn' => 'required',
            'jenisKelamin' => 'required',
        ]);

        Aslab::create([
            'username' => $req->npm,
            'laboratorium_id' => \Auth::user()->id-1,
            'nama' => $req->nama,
            'password' => Hash::make($req->password),
            'no_tlpn' => $req->no_tlpn,
            'jenis_kelamin' => $req->jenisKelamin,
        ]);


        return redirect()->back()->with(['success' => 'Berhasil menambah Asisten Lab']);
    }

    public function manajemenAslabDelete(Request $req){
        $aslab = Aslab::findOrFail($req->id)->delete();

        if($aslab){
            return redirect()->back()->with(['success' => 'Berhasil menghapus Asisten Lab']);
        }else{
            return redirect()->back()->with(['error' => 'Asisten Lab gagal dihapus.']);
        }
        
    }

    public function manajemenAslabUpdate(Request $req){
        $this->validate($req, [
            'id' => 'required',
            'npm' => 'required|min:15|max:15',
            'nama' => 'required|min:3',
            'no_tlpn' => 'required',
            'jenisKelamin' => 'required',
        ]);

        $aslab = Aslab::findOrFail($req->id);
        $aslab->username = $req->npm;
        $aslab->nama = $req->nama;
        $aslab->no_tlpn = $req->no_tlpn;
        $aslab->jenis_kelamin = $req->jenisKelamin;
        $aslab->save();

        if($req->password != null){
            $this->validate($req,[
                'password' => 'required|min:6',
                'password-confirm' => 'required_with:password|same:password|min:6',
            ]);
            $aslab->password = Hash::make($req->password);
            $aslab->save();
        }

        return redirect()->back()->with(['success' => 'Berhasil mengubah data Aslab']);
    }

    public function periodePraktikumView(){
        $periodePraktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id','periode_praktikum.status')
        ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
        ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
        ->get();

        // dd($periodePraktikum);
        $praktikum = Praktikum::where('laboratorium_id', \Auth::user()->id-1)->get();
        return view('admin.periodePraktikum',compact('praktikum','periodePraktikum'));
    }

    public function periodePraktikumProses(Request $req){
        $this->validate($req, [
            'praktikum' => 'required',
            'tahun' => 'required',
        ]);

        $cari = periode_praktikum::where('praktikum_id',$req->praktikum)->where('tahun',$req->tahun)->get();

        if(count($cari) > 0){
            return redirect()->back()->with(['error' => 'Periode Praktikum Sudah Ada']); 
        }

        Periode_Praktikum::create([
            'praktikum_id' => $req->praktikum,
            'tahun' => $req->tahun,
            'status' => 0,
        ]);

        return redirect()->back()->with(['success' => 'Berhasil Menambah Periode Praktikum Baru']); 
    }

    public function aktifkanPeriodePraktikum(periode_praktikum $periode){
        $update = DB::table('periode_praktikum')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
        ->update(['periode_praktikum.status' => 0]);

        $periode->status = 1;
        $periode->save();

        return redirect()->back()->with(['success' => 'Berhasil Mengaktifkan Praktikum']); 
    }

    public function sesiPraktikumView(){
        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
        ->where('periode_praktikum.status',1)
        ->first();

        $sesi = DB::table('sesi')
        ->select('sesi.id','sesi.nama','sesi.waktu','sesi.kuota','periode_praktikum.tahun','praktikum.nama as namaPraktikum')
        ->join('periode_praktikum','periode_praktikum.id','=','periode_praktikum_id')
        ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
        ->where('praktikum.laboratorium_id', \Auth::user()->id-1)
        ->where('periode_praktikum.status', 1)
        ->get();

        if($praktikum == null){
            return redirect()->route('periodePraktikum')->with(['error' => 'Anda harus mengaktifkan salah satu praktikum']); 
        }else{
            return view('admin.sesiPraktikum',compact('praktikum','sesi'));
        }
    }

    public function sesiPraktikumProses(Request $req){
        $this->validate($req, [
            'idPraktikum' => 'required',
            'namaSesi' => 'required',
            'waktu' => 'required',
            'kuota' => 'required',
        ]);

        Sesi::create([
            'periode_praktikum_id' => $req->idPraktikum,
            'nama' => $req->namaSesi,
            'waktu' => $req->waktu,
            'kuota' => $req->kuota,
        ]);

        return redirect()->back()->with(['success' => 'Berhasil menambah sesi praktikum']); 

    }


    public function sesiPraktikumProsesDelete(Request $req){
        $praktikan = praktikan::where('sesi_id',$req->id)->get();
        if(count($praktikan) > 0){
            return redirect()->back()->with(['error' => 'Sesi sudah digunakan Praktikan. Hapus Praktikan dulu.']);   
        }else{
            $sesi = sesi::findOrFail($req->id)->delete();
            return redirect()->back()->with(['success' => 'Berhasil menghapus Sesi Praktikum']);
        }
    }

    public function sesiPraktikumProsesUpdate(Request $req){
        $this->validate($req, [
            'id' => 'required',
            'namaSesi' => 'required',
            'waktu' => 'required',
            'kuota' => 'required',
        ]);

        $sesi = Sesi::findOrFail($req->id);
        $sesi->nama = $req->namaSesi;
        $sesi->waktu = $req->waktu;
        $sesi->kuota = $req->kuota;
        $sesi->save();

        return redirect()->back()->with(['success' => 'Berhasil mengubah data Sesi']);
    }

    public function periodeAslabView(){
        $praktikum = DB::table('periode_praktikum')
                    ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
                    ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status',1)
                    ->first();
                    

        //SELECT ASLAB.ID, ASLAB.username, ASLAB.NAMA FROM ASLAB 
        //LEFT JOIN periode_praktikum_aslab ON ASLAB.ID = periode_praktikum_aslab.aslab_id 
        //WHERE periode_praktikum_aslab.aslab_id IS NULL

        //query ne ono sing salah nang kene

        $aslab = DB::table('aslab')
                ->select('aslab.nama','aslab.username','aslab.id')
                ->join('laboratorium','aslab.laboratorium_id','laboratorium.id')
                ->where('aslab.laboratorium_id','=',\Auth::user()->id-1)
                ->whereNotIn('aslab.id', DB::table('periode_praktikum_aslab')->where('periode_praktikum_aslab.periode_praktikum_id',$praktikum->id)->pluck('aslab_id'))
                ->get();

        $periodeAslab = DB::table('periode_praktikum_aslab')
                    ->select('periode_praktikum_aslab.id as id','periode_praktikum_aslab.kuota','periode_praktikum.tahun','praktikum.nama as namaPraktikum','aslab.nama','aslab.username', 'jenis_kelas.nama as pagiMalam','jenis_kelas.id as idPagiMalam')
                    ->join('periode_praktikum','periode_praktikum.id','=','periode_praktikum_aslab.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','=','periode_praktikum.praktikum_id')
                    ->join('aslab','aslab.id','=','periode_praktikum_aslab.aslab_id')
                    ->join('jenis_kelas','jenis_kelas.id','=','periode_praktikum_aslab.jenis_kelas_id')
                    ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
                    ->where('periode_praktikum.status', 1)
                    ->get();
        
        $cekAslab = Aslab::select('aslab.id')
                ->join('laboratorium','aslab.laboratorium_id','laboratorium.id')
                ->where('aslab.laboratorium_id',\Auth::user()->id-1)
                ->get();

        $jenisKelas = jenis_kelas::all();

        $aslabCek = DB::table('aslab')
                ->select('aslab.nama','aslab.username','aslab.id')
                ->join('laboratorium','aslab.laboratorium_id','laboratorium.id')
                ->where('aslab.laboratorium_id','=',\Auth::user()->id-1)
                // ->whereNotIn('aslab.id', DB::table('periode_praktikum_aslab')->where('periode_praktikum_aslab.periode_praktikum_id',$praktikum->id)->pluck('aslab_id'))
                ->get();
        
        if(count($aslabCek) == 0){
            return redirect()->route('manajemenAslab')->with(['error' => 'Anda harus menambahkan minimal 1 Asisten Laboratorium']); 
        }else if($praktikum == null){
            return redirect()->route('periodePraktikum')->with(['error' => 'Anda harus mengaktifkan salah satu praktikum']); 
        }else{
            return view('admin.periodeAslab',compact('praktikum','aslab','periodeAslab','jenisKelas','cekAslab'));
        }

    }

    public function periodeAslabProses(Request $req){
        $this->validate($req, [
            'periodePraktikumId' => 'required',
            'aslab' => 'required',
            'kuota' => 'required',
            'pagiMalam' => 'required',
        ]);

        $cekPeriodePraktikumAslab = periode_praktikum_aslab::where('periode_praktikum_id',$req->periodePraktikumId)
        ->where('aslab_id',$req->aslab)
        ->get();

        if(count($cekPeriodePraktikumAslab) > 0){
            return redirect()->back()->with(['error' => 'Asisten Lab Sudah Ada']); 
        }

        periode_praktikum_aslab::create([
            'periode_praktikum_id' => $req->periodePraktikumId,
            'aslab_id' => $req->aslab,
            'kuota' => $req->kuota,
            'jenis_kelas_id' => $req->pagiMalam,
        ]);
        
        return redirect()->back()->with(['success' => 'Asisten Lab Berhasil Diaktifkan']); 
    }

    public function periodeAslabDelete(Request $req){
        $penilaian = Penilaian_aslab::where('periode_praktikum_aslab_id', $req->id)->get();
        if(count($penilaian) == 0){
            Periode_praktikum_aslab::destroy($req->id);     
            return redirect()->back()->with(['success' => 'Asisten Aslab Berhasil Dinonaktifkan']);
        }else{
            return redirect()->back()->with(['error' => 'Asisten Aslab Gagal Dinonaktifkan']);
        }
    }

    public function periodeAslabUpdate(Request $req){
        $this->validate($req, [
            'kuota' => 'required',
            'pagiMalam' => 'required'
        ]);

        Periode_praktikum_aslab::find($req->id)->update([
            'kuota' => $req->kuota,
            'pagiMalam' => $req->pagiMalam,
        ]);

        return redirect()->back()->with(['success' => 'Asisten Lab Berhasil Diubah']);
    }

    public function cetakAbsensi(){
        $praktikum = DB::table('periode_praktikum')
        ->select('praktikum.nama','periode_praktikum.tahun','periode_praktikum.id')
        ->join('praktikum','periode_praktikum.praktikum_id','=','praktikum.id')
        ->where('praktikum.laboratorium_id',\Auth::user()->id-1)
        ->where('periode_praktikum.status',1)
        ->first();

        $id = $praktikum->id;

        $praktikan = DB::table('praktikan')
                    ->select('mahasiswa.nama','mahasiswa.npm','periode_praktikum.tahun','praktikum.nama as namaPraktikum')
                    ->join('periode_praktikum','periode_praktikum.id','praktikan.periode_praktikum_id')
                    ->join('praktikum','praktikum.id','periode_praktikum.praktikum_id')
                    ->join('mahasiswa','mahasiswa.id','praktikan.mahasiswa_id')
                    ->where('periode_praktikum_id', $id)
                    ->orderBy('sesi_id', 'ASC')
                    ->get();
        if(count($praktikan) == 0){
            return redirect()->back()->with(['error' => 'Belum ada praktikan yang terverifikasi']);
        }

        return (Excel::download(new AbsensiPraktikum($id), 'Absensi Praktikum.xlsx'));
    }

    public function manajemenBerita(){
        if(\Auth::user()->id == 2 || \Auth::user()->id == 3 || \Auth::user()->id == 4){
            $berita = Berita::all()->where('user_id',\Auth::user()->id);
            return view('admin.berita', compact('berita'));
        }else{
            return redirect()->back();
        }
    }

    public function manajemenBeritaView(Berita $berita){
        return view('admin.beritaView',compact('berita'));
    }

    public function manajemenBeritaTambahProses(Request $req){
        $this->validate($req, [
            'judul' => 'required',
            'content' => 'required',
        ]);

        if($req->gambar != null){
            $this->validate($req, [
                'gambar' => 'required|image|mimes:jpeg,jpg,png|max:20000',
            ]);
            $namaFile = \Auth::user()->id . '_' . $req->judul;
            $req->file('gambar')->move('upload/berita', $namaFile);
        }

        $berita = Berita::create([
            'user_id' => \Auth::user()->id,
            'judul' => $req->judul,
            'gambar' => null,
            'deskripsi' => null,
        ]);

        $detail=$req->content;
 
        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');
 
        foreach($images as $k => $img){
            $data = $img->getattribute('src');
 
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
 
            $data = base64_decode($data);
            $image_name= \Auth::user()->id . '_' . time().$k.'.png';
            $path = public_path() .'/upload/berita/'. $image_name;
 
            file_put_contents($path, $data);
 
            $img->removeattribute('src');
            $img->setattribute('src', '/upload/berita/' . $image_name);
        }
 
        $detail = $dom->savehtml();
        $berita->deskripsi = $detail;
        $berita->save();

        if($req->gambar != null){
            $berita->gambar = $namaFile;
            $berita->save();
        }

        return redirect()->back()->with(['success' => 'Berhasil menambahkan Berita']);  
    }

    public function manajemenBeritaViewEdit(Berita $berita){
        return view('admin.beritaViewEdit',compact('berita'));
    }

    public function manajemenBeritaViewEditProses(Request $req){
        $berita = Berita::findOrFail($req->id);
        $this->validate($req, [
            'judul' => 'required',
            'content' => 'required',
        ]);

        if($req->gambar != null){
            $this->validate($req, [
                'gambar' => 'required|image|mimes:jpeg,jpg,png|max:20000',
            ]);

            File::delete('/upload/berita/' . $berita->gambar);

            $namaFile = \Auth::user()->id . '_' . $req->judul;
            $req->file('gambar')->move('upload/berita', $namaFile);

            $berita->gambar = $namaFile;
            $berita->save();
        }

        $detail=$req->content;

        libxml_use_internal_errors(true);

        $dom = new \DOMDocument();
        $dom->loadHTML(
            mb_convert_encoding($detail, 'HTML-ENTITIES', 'UTF-8'),
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );

        $images = $dom->getElementsByTagName('img');

        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');

            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];
                
                $image_name= \Auth::user()->id . '_' . time().$count.'.png';
                $path = public_path() .'/upload/berita/'. $image_name;

                \Image::make($src)
                    ->resize(750, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode($mimeType, 80)
                    ->save($path);

                $image->removeAttribute('src');
                $image->setAttribute('src', '/upload/berita/' . $image_name);
            }
        }

        $detail = $dom->savehtml();
        $berita->judul = $req->judul;
        $berita->deskripsi = $detail;
        $berita->save();

        return redirect()->route('manajemenBeritaView', $req->id)->with(['success'=>'Berhasil mengubah berita']);

    }

    public function manajemenBeritaViewDelete(Request $req){
        $berita = Berita::findOrFail($req->id);

        if($req->gambar != null){
            File::delete('/upload/berita/' . $berita->gambar);
        }

        $detail=$berita->deskripsi;

        libxml_use_internal_errors(true);

        $dom = new \DOMDocument();
        $dom->loadHTML(
            mb_convert_encoding($detail, 'HTML-ENTITIES', 'UTF-8'),
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );

        $images = $dom->getElementsByTagName('img');

        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');
            if (strpos( $src, "/upload/berita/" )) {
                $image_name = explode("/upload/berita/", $src, 2)[1];
                
                $image->removeAttribute('src');
                File::delete('/upload/berita/' . $image_name);
            }
        }

        $berita->delete();

        return redirect()->route('manajemenBerita')->with(['success'=>'Berhasil menghapus berita']); 

        
    }

}
