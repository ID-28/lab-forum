<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Aslab;
use App\Mahasiswa;
use App\Thread;
use App\Comment;
use Session;
use Redirect;
use DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AbsensiPraktikanAslab;

class AslabController extends Controller
{
    public function __construct(){
        session_start();
        if(!isset($_SESSION['aslab'])){
            Redirect::to('/')->send();
        }
    }

    public function index(){
        return view('aslab.aslab');
    }
    public function entryNilai(){
        return view('aslab/entryNilai');
    }

    public function listPraktikan(){
        $idAslab = $_SESSION['aslab']->id;
        $praktikan = DB::table('penilaian_aslab')
            ->select('mahasiswa.id as idMahasiswa','mahasiswa.npm as npm','mahasiswa.nama as namaMahasiswa','sesi.nama as namaSesi','sesi.waktu as waktu','praktikan.id as praktikanID')
            ->join('praktikan','praktikan.id','penilaian_aslab.praktikan_id')
            ->join('mahasiswa','mahasiswa.id','praktikan.mahasiswa_id')
            ->join('sesi','sesi.id','praktikan.sesi_id')
            ->join('periode_praktikum','periode_praktikum.id','sesi.periode_praktikum_id')
            ->join('periode_praktikum_aslab','periode_praktikum_aslab.id','penilaian_aslab.periode_praktikum_aslab_id')
            ->where('periode_praktikum.status',1)
            ->where('periode_praktikum_aslab.aslab_id',$idAslab)
            ->get();

        return view('aslab.listPraktikan',compact('praktikan'));
    }

    public function listPraktikanView($id){
        $user = Mahasiswa::findOrFail($id);
        return view('aslab.praktikanViewDetail',compact('user'));
    }

    public function entryNilaiPraktikan(){
        return view('aslab.entryNilaiPraktikan');
    }

    public function change_password(Request $req){

        $user = Aslab::where('id', $_SESSION['aslab']->id)->first();
        if (Hash::check($req->old,$user->password)) {
            if($req->new1 == $req->new2){
                $user->password =  Hash::make($req->new1);
                $user->save();
                return redirect()->back()->with(['success' => 'Password Berhasil Dirubah']);
            }else{
                return redirect()->back()->with(['warning' => 'Password Baru Tidak Sama']);
            }
        }else{
            return redirect()->back()->with(['warning' => 'Password Lama Tidak Sama']);
        }
    }

    public function cetakListPraktikanAslab(){
        $idAslab = $_SESSION['aslab']->id;
        return (Excel::download(new AbsensiPraktikanAslab($idAslab), 'Absensi Praktikan.xlsx'));
    }

    public function listPraktikum()
    {
        $idAslab = $_SESSION['aslab']->id;

        // dd($idAslab);
//        dd($id_mahasiswa);

        $praktikum = DB::table('periode_praktikum_aslab')
            ->select('praktikum.nama as namaPraktikum','periode_praktikum.id as idPraktikum')
            ->join('periode_praktikum','periode_praktikum.id','periode_praktikum_aslab.periode_praktikum_id')
            ->join('praktikum','praktikum.id','periode_praktikum.praktikum_id')
            ->where('periode_praktikum_aslab.aslab_id',$idAslab)
            ->where('periode_praktikum.status',1)
            ->get();

        // dd($praktikum);

        return view('aslab.praktikum',compact('praktikum','id_user'));
    }

    public function listModul($praktikum)
    {
        $jenis_user = DB::table('jenis_user')
            ->select('jenis_user.id as jenis_user')
            ->where('jenis_user.nama','=','aslab')
            ->first();

        $modul = DB::table('modul_aslab')
            ->select('modul_aslab.id as idModul','modul_aslab.nama as namaModul')
            ->join('periode_praktikum','periode_praktikum.id','modul_aslab.periode_praktikum_id')
            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
//            ->where([['praktikum.laboratorium_id',\Auth::user()->id-1],['periode_praktikum.status',1]])
            ->where('modul_aslab.periode_praktikum_id','=',$praktikum)
            ->get();

        // dd($jenis_user);

        return view('aslab.modul',compact('modul','jenis_user'));
    }

    public function listMateri($modul)
    {
        $materi = DB::table('modul_materi_aslab')
            ->select('modul_materi_aslab.modul_aslab_id as idma','modul_materi_aslab.materi_id as idm','modul_aslab.nama as namaModul','materi.nama as namaMateri', 'praktikum.nama as namaPraktikum' ,'periode_praktikum.tahun','materi.id as idmat','modul_aslab.id as id')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('materi','materi.id','modul_materi_aslab.materi_id')
            ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
//            ->where([['praktikum.laboratorium_id',\Auth::user()->id-1],['periode_praktikum.status',1]])
//            ->where('modul_aslab.periode_praktikum_id','=',$request->id)
            ->where('modul_materi_aslab.modul_aslab_id','=',$modul)
            ->get();

//        dd($materi);

        return view('aslab.materi',compact('materi'));
    }

    public function listThread($materi)
    {
        $jenisUser =  DB::table('jenis_user')
            ->select('jenis_user.id as jenis_user')
            ->where('jenis_user.nama','=','aslab')
            ->first();

        $datamateri = DB::table('materi')
            ->select('materi.id as idMateri','modul_aslab.id as idModul','modul_dosen.id as idModulDosen','praktikum.id as idPraktikum','materi.nama as namaMateri','praktikum.nama as namaPraktikum','modul_aslab.nama as namaModul', 'modul_dosen.nama as modulDosen')
            ->join('praktikum','praktikum.id','materi.praktikum_id')
            ->join('periode_praktikum','periode_praktikum.praktikum_id','praktikum.id')
            ->join('modul_materi_aslab','modul_materi_aslab.materi_id','materi.id')
            ->join('modul_materi_dosen','modul_materi_dosen.materi_id','materi.id')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('modul_dosen','modul_dosen.id','modul_materi_dosen.modul_dosen_id')
            ->where('materi.id','=',$materi)
            ->first();

        $thread = DB::table('thread')
            ->select('thread.id as idThread','thread.judul','thread.keterangan','mahasiswa.nama as namaMahasiswa')
            ->join('materi','materi.id','thread.materi_id')
            ->join('praktikum','praktikum.id','materi.praktikum_id')
            ->join('periode_praktikum','periode_praktikum.id','praktikum.id')
            ->join('user','user.id','thread.user_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->where('thread.materi_id','=',$materi)
            ->get();

//        dd($thread);

        return view('thread.index',compact('thread','jenisUser','datamateri'));
    }

    public function view($thread)
    {
        $jenisUser = DB::table('jenis_user')
            ->select('jenis_user.id as jenis_user')
            ->where('jenis_user.nama','=','aslab')
            ->first();

        // dd($jenisUser);

        $materi = DB::table('thread')
            ->select('thread.id as idThread','thread.judul as judulThread', 'thread.status','thread.keterangan as ket','thread.created_at as posted',
                'materi.nama as namaMateri','materi.id as idMateri',
                'modul_dosen.id as idModulDosen','modul_aslab.id as idModul','modul_dosen.nama as namaModulDosen','modul_aslab.nama as namaModul',
                'praktikum.id as idPraktikum','praktikum.nama as namaPraktikum',
                'user.username as username','mahasiswa.nama as namaMahasiswa')
            ->join('user','user.id','thread.user_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->join('materi','materi.id','thread.materi_id')
            ->join('modul_materi_aslab','modul_materi_aslab.materi_id','materi.id')
            ->join('modul_materi_dosen','modul_materi_dosen.materi_id','materi.id')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('modul_dosen','modul_dosen.id','modul_materi_dosen.modul_dosen_id')
            ->join('praktikum','praktikum.id','materi.praktikum_id')
            ->join('periode_praktikum','periode_praktikum.praktikum_id','praktikum.id')
            ->where('thread.id','=', $thread)
            ->where('periode_praktikum.status','=',1)
            ->first();

        $MComment = DB::table('comment')
            ->select('comment.id as idComment','comment.comment','comment.created_at',
                'thread.id as idThread','thread.status as state','thread.user_id as user_id',
                'user.username as username','mahasiswa.nama as nama')
            ->join('thread','thread.id','comment.thread_id')
            ->join('user','user.id','comment.comment_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->where('comment.thread_id','=',$thread)
            ->where('comment.comment_by','=',5)
            ->latest('comment.created_at')
//            ->paginate(2);
            ->get();

        $DComment = DB::table('comment')
            ->select('comment.id as idComment','comment.comment','comment.created_at',
                'thread.id as idThread','thread.status as state','thread.user_id as user_id',
                'user.username as username','dosen.nama as nama')
            ->join('thread','thread.id','comment.thread_id')
            ->join('user','user.id','comment.comment_id')
            ->join('dosen','dosen.nip','user.username')
            ->where('comment.thread_id','=',$thread)
            ->where('comment.comment_by','=',3)
            ->latest('comment.created_at')
            ->get();

        $AComment = DB::table('comment')
            ->select('comment.id as idComment','comment.comment','comment.created_at',
                'thread.id as idThread','thread.status as state','thread.user_id as user_id',
                'aslab.username as username','aslab.nama as nama')
            ->join('thread','thread.id','comment.thread_id')
            ->join('aslab','aslab.id','comment.comment_id')
            ->where('comment.thread_id','=',$thread)
            ->latest('comment.created_at')
            ->where('comment.comment_by','=',4)
//            ->paginate(2);
            ->get();

        // dd($AComment);

        return view('thread.view', compact('materi','MComment','DComment','AComment','jenisUser'));
    }

    public function addThreadComment(Request $request, Thread $thread){
        $idAslab = $_SESSION['aslab']->id;

        $this->validate($request,[
            'body'=>'required',
        ]);

        $comment = Comment::create([
            'comment' => $request->body,
            'thread_id' => $thread->id,
            'comment_id' => $idAslab,
            'comment_by' => 4,
        ]);

        return redirect()->back();
    }
}
