<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Thread;
use App\Praktikum;
use App\Periode_praktikum;
use App\Materi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ThreadController extends Controller
{
    public function index($materi)
    {
        $jenisUser = DB::table('user')
            ->select('jenis_user_id as jenis_user')
            ->where('user.id', '=', \Auth::user()->id)
            ->first();

        $praktikum = DB::table('praktikan')
            ->select('mahasiswa.npm as username')
            ->join('mahasiswa','mahasiswa.id','praktikan.mahasiswa_id')
            ->join('user','user.username','mahasiswa.npm')
            ->join('periode_praktikum','periode_praktikum.id','praktikan.periode_praktikum_id')
            ->join('praktikum','praktikum.id','periode_praktikum.id')
            ->join('materi','materi.praktikum_id','praktikum.id')
            ->where('materi.id','=',$materi)
            ->where('user.id','=',\Auth::user()->id)
            ->first();

//        dd($jenisUser);

        $praktikum_dosen = DB::table('periode_praktikum_dosen')
            ->select('dosen.nip as username_dosen')
            ->join('dosen_laboratorium','dosen_laboratorium.id','periode_praktikum_dosen.dosen_laboratorium_id')
            ->join('dosen','dosen.id','dosen_laboratorium.dosen_id')
            ->join('user','user.username','dosen.nip')
            ->join('periode_praktikum','periode_praktikum.id','periode_praktikum_dosen.periode_praktikum_id')
            ->join('praktikum','praktikum.id','periode_praktikum.id')
            ->join('materi','materi.praktikum_id','praktikum.id')
            ->where('materi.id','=',$materi)
            ->where('user.id','=',\Auth::user()->id)
            ->first();

//        dd($praktikum_dosen);

        $datamateri = DB::table('materi')
            ->select('materi.id as idMateri', 'modul_aslab.id as idModul', 'modul_dosen.id as idModulDosen', 'praktikum.id as idPraktikum', 'materi.nama as namaMateri', 'praktikum.nama as namaPraktikum', 'modul_aslab.nama as namaModul', 'modul_dosen.nama as modulDosen')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->join('modul_materi_aslab', 'modul_materi_aslab.materi_id', 'materi.id')
            ->join('modul_materi_dosen', 'modul_materi_dosen.materi_id', 'materi.id')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('modul_dosen', 'modul_dosen.id', 'modul_materi_dosen.modul_dosen_id')
            ->where('materi.id', '=', $materi)
            ->first();

        $thread = DB::table('thread')
            ->select('thread.id as idThread', 'thread.judul', 'thread.keterangan', 'mahasiswa.nama as namaMahasiswa')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'praktikum.id')
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('thread.materi_id', '=', $materi)
            ->where('periode_praktikum.status', '=', 1)
            ->get();

//        dd($jenisUser);

        return view('thread.index', compact('thread', 'jenisUser', 'datamateri','praktikum','praktikum_dosen'));
    }

    public function create($materi)
    {
        $mate = DB::table('modul_materi_aslab')
            ->select('materi.id as idmat', 'materi.nama as namaMate')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('materi', 'materi.id', 'modul_materi_aslab.materi_id')
            ->join('periode_praktikum', 'modul_aslab.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('materi.id', '=', $materi)
            ->first();

//        dd($mate);

        return view('thread.create', compact('mate'));
    }

    public function store(Request $request)
    {

        //validate
        $this->validate($request, [
            'judul' => 'required|min:5',
            'keterangan' => 'required|min:10',
            'materi' => 'required',
            'status' => 'null'
        ]);

//        dd(\Auth::user()->id);

        $thread = Thread::create([
            'materi_id' => $request->materi,
            'user_id' => \Auth::user()->id,
            'judul' => $request->judul,
            'keterangan' => $request->keterangan,
            'status' => $request->status,
        ]);

        //redirect
        return redirect()->route('view_thread', $thread->id);
    }

    public function view($thread)
    {
        $jenisUser = DB::table('user')
            ->select('jenis_user_id as jenis_user')
            ->where('user.id', '=', \Auth::user()->id)
            ->first();

        $idUserLogin = DB::table('user')
            ->select('id as idUser')
            ->where('user.id', '=', \Auth::user()->id)
            ->first();

        $materi = DB::table('thread')
            ->select('thread.id as idThread', 'thread.judul as judulThread', 'thread.status', 'thread.keterangan as ket', 'thread.created_at as posted',
                'materi.nama as namaMateri', 'materi.id as idMateri',
                'modul_dosen.id as idModulDosen', 'modul_aslab.id as idModul', 'modul_dosen.nama as namaModulDosen', 'modul_aslab.nama as namaModul',
                'praktikum.id as idPraktikum', 'praktikum.nama as namaPraktikum',
                'user.username as username', 'mahasiswa.nama as namaMahasiswa')
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('modul_materi_aslab', 'modul_materi_aslab.materi_id', 'materi.id')
            ->join('modul_materi_dosen', 'modul_materi_dosen.materi_id', 'materi.id')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('modul_dosen', 'modul_dosen.id', 'modul_materi_dosen.modul_dosen_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('thread.id', '=', $thread)
            ->where('periode_praktikum.status', '=', 1)
            ->first();

        $MComment = DB::table('comment')
            ->select('comment.id as idComment', 'comment.comment', 'comment.created_at', 'comment.comment_id', 'comment.comment_by',
                'thread.id as idThread', 'thread.status as state', 'thread.user_id as user_id',
                'user.username as username', 'mahasiswa.nama as nama')
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by', '=', 5)
            ->latest('comment.created_at')
//            ->paginate(2);
            ->get();

        $DComment = DB::table('comment')
            ->select('comment.id as idComment', 'comment.comment', 'comment.created_at', 'comment.comment_id', 'comment.comment_by',
                'thread.id as idThread', 'thread.status as state', 'thread.user_id as user_id',
                'user.username as username', 'dosen.nama as nama')
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('dosen', 'dosen.nip', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by', '=', 3)
            ->latest('comment.created_at')
            ->get();

        $AComment = DB::table('comment')
            ->select('comment.id as idComment', 'comment.comment', 'comment.created_at', 'comment.comment_id', 'comment.comment_by',
                'thread.id as idThread', 'thread.status as state', 'thread.user_id as user_id',
                'aslab.username as username', 'aslab.nama as nama')
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('aslab', 'aslab.id', 'comment.comment_id')
            ->where('comment.thread_id', '=', $thread)
            ->latest('comment.created_at')
            ->where('comment.comment_by', '=', 4)
//            ->paginate(2);
            ->get();

        // dd($AComment);

        return view('thread.view', compact('materi', 'MComment', 'DComment', 'AComment', 'jenisUser', 'idUserLogin'));
    }

    public function addThreadComment(Request $request, Thread $thread)
    {
        $jenisUser = DB::table('user')
            ->select('jenis_user_id as jenis_user')
            ->where('user.id', '=', \Auth::user()->id)
            ->first();

        // dd($jenisUser);

        $this->validate($request, [
            'body' => 'required',
        ]);

        $comment = Comment::create([
            'comment' => $request->body,
            'thread_id' => $thread->id,
            'comment_id' => \Auth::user()->id,
            'comment_by' => $jenisUser->jenis_user,
        ]);

        return redirect()->back();
    }

    public function updateComment(Request $request, Comment $comment)
    {
        $this->validate($request, [
            'body' => 'required',
        ]);

        $comment = Comment::where('id', $comment->id)
            ->update(['comment' => $request->body]);

        return redirect()->back();
    }

    public function listThread()
    {
        $thread = DB::table('thread')
            ->select('thread.id as idThread', 'thread.judul', 'thread.keterangan', 'materi.nama as namaMateri', 'praktikum.nama as namaPraktikum', 'periode_praktikum.tahun', 'modul_aslab.nama as namaModul', 'materi.id as idMateri', 'mahasiswa.nama as namaMahasiswa')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'praktikum.id')
            ->join('modul_materi_aslab', 'modul_materi_aslab.materi_id', 'materi.id')
//            ->join('modul_materi_aslab','modul_materi_aslab.modul_aslab_id','modul_aslab.id')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->get();


        return view('thread.list', compact('thread'));
    }

    public function threadSolution()
    {
//        dd(input::all());
        $statusId = input::get('statusId');
        $threadId = input::get('threadId');
        $thread = Thread::find($threadId);
        $thread->status = $statusId;
        if ($thread->save()) {
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function deleteComment(Comment $comment)
    {
        $comment->delete();

        return back();
    }
}
