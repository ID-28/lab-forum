<?php

namespace App\Http\Controllers\API;

use App\Comment;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
// use App\Detail_modul;
use App\Dosen;
use App\Praktikum;
use App\Materi;
use App\Periode_praktikum;
use App\Penilaian_dosen;
// use App\Detail_dosen;
use App\Detail_materi_dosen;
use App\User;
use App\Praktikan;
use App\Mahasiswa;
use PDF;
use DateTime;

// use App\Exports\NilaiExcel;
// use Excel;
use Illuminate\Support\Facades\Mail;

class ApiDosenController extends Controller
{
    public function index(Request $request)
    {
        $praktikum = DB::table('penilaian_dosen')
            ->select('dosen.nama as namaDosen', 'praktikum.nama as namaPraktikum', DB::raw('COUNT(penilaian_dosen.praktikan_id) as total'))
            ->join('praktikan', 'penilaian_dosen.praktikan_id', 'praktikan.id')
            ->join('periode_praktikum', 'praktikan.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->join('periode_praktikum_dosen', 'penilaian_dosen.periode_praktikum_dosen_id', 'periode_praktikum_dosen.id')
            ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
            ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
            // ->where('dosen_laboratorium.laboratorium_id',\Auth::user()->id-1)
            ->where('dosen.nip', '=', $request->username)
            ->where('periode_praktikum.status', 1)
            ->groupBy('namaDosen')
            ->groupBy('namaPraktikum')
            ->get();

        //        dd($praktikum);

        return response()->json(compact('praktikum'));
    }

    public function entry_nilai()
    {
        return view('dosen.entry-nilai');
    }

    public function entry_nilai_create(Request $req)
    {

        $banyakModul = DB::select('select count(*) as hitung from
                        (SELECT modul_dosen_id from modul_materi_dosen
                        JOIN materi on modul_materi_dosen.materi_id = materi.id
                        JOIN praktikum on materi.praktikum_id = praktikum.id
                        where praktikum_id =' . $req->nama_praktikum . '
                        GROUP by modul_dosen_id)
                        as hitung;');

        $idModulMateri = DB::table('modul_materi_dosen')
            ->select('modul_materi_dosen.id as idModul')
            ->join('materi', 'modul_materi_dosen.materi_id', 'materi.id')
            ->join('praktikum', 'materi.praktikum_id', 'praktikum.id')
            ->where('praktikum_id', $req->nama_praktikum)
            ->get();

        $modulMateri = DB::table('modul_materi_dosen')
            ->select('modul_dosen.id as idDosen', 'materi.id as idMateri', 'materi.nama as nama')
            ->join('materi', 'modul_materi_dosen.materi_id', '=', 'materi.id')
            ->join('modul_dosen', 'modul_materi_dosen.modul_dosen_id', 'modul_dosen.id')
            ->join('periode_praktikum', 'modul_dosen.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('periode_praktikum.status', 1)
            ->where('praktikum.id', $req->nama_praktikum)
            ->get();

        $idModul = DB::table('modul_materi_dosen')
            ->select('modul_materi_dosen.modul_dosen_id as idModul')
            ->join('modul_dosen', 'modul_materi_dosen.modul_dosen_id', 'modul_dosen.id')
            ->join('periode_praktikum', 'modul_dosen.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('periode_praktikum.status', 1)
            ->where('praktikum.id', $req->nama_praktikum)
            ->groupBy('modul_materi_dosen.modul_dosen_id')
            ->get();

        $praktikan = DB::table('mahasiswa')
            ->select('mahasiswa.npm as npm', 'mahasiswa.nama as namaMahasiswa', 'sesi.nama as namaSesi', 'sesi.waktu as waktu', 'penilaian_dosen.id as idPenilaian', 'praktikan.id as praktikanID', 'penilaian_dosen.id as idPenilaian')
            ->join('praktikan', 'praktikan.mahasiswa_id', 'mahasiswa.id')
            ->join('penilaian_dosen', 'praktikan.id', 'penilaian_dosen.praktikan_id')
            ->join('periode_praktikum_dosen', 'penilaian_dosen.periode_praktikum_dosen_id', 'periode_praktikum_dosen.id')
            ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
            ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
            ->join('periode_praktikum', 'praktikan.periode_praktikum_id', 'periode_praktikum.id')
            ->join('sesi', 'praktikan.sesi_id', 'sesi.id')
            ->where('periode_praktikum.praktikum_id', $req->nama_praktikum)
            ->where('dosen.nip', Auth::user()->username)
            ->get();

        if (count($praktikan) == 0) {
            return redirect()->back()->with(['error' => 'Anda Tidak Mempunyai Praktikan']);
        }

        $nilaiSemuaPraktikan = array();
        foreach ($praktikan as $p) {
            $nilaiPraktikan = array();
            for ($i = 1; $i <= $banyakModul[0]->hitung; $i++) {
                $nilai = Detail_materi_dosen::select(DB::raw('AVG(detail_materi_dosen.nilai_materi) as avg'))
                    ->join('modul_materi_dosen', 'detail_materi_dosen.modul_materi_dosen_id', 'modul_materi_dosen.id')
                    ->join('penilaian_dosen', 'detail_materi_dosen.penilaian_dosen_id', 'penilaian_dosen.id')
                    ->join('materi', 'modul_materi_dosen.materi_id', 'materi.id')
                    ->join('praktikum', 'materi.praktikum_id', 'praktikum.id')
                    ->where('detail_materi_dosen.penilaian_dosen_id', $p->idPenilaian)
                    ->where('penilaian_dosen.praktikan_id', $p->praktikanID)
                    ->where('modul_materi_dosen.modul_dosen_id', $idModulMateri[$i - 1]->idModul)
                    ->where('materi.praktikum_id', $req->nama_praktikum)
                    ->first();
                if ($nilai) {
                    array_push($nilaiPraktikan, $nilai->avg);
                } else {
                    array_push($nilaiPraktikan, '0');
                }
            }
            array_push($nilaiSemuaPraktikan, $nilaiPraktikan);
        }
        // OTW
        $idPeriodePraktikumDosen = DB::table('periode_praktikum_dosen')
            ->select('periode_praktikum_dosen.id as idDosen', 'periode_praktikum_dosen.status as status')
            ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
            ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'periode_praktikum_dosen.periode_praktikum_id')
            ->where('dosen.nip', Auth::user()->username)
            ->where('periode_praktikum.praktikum_id', $req->nama_praktikum)
            ->first();

        return view(
            'dosen.entry-nilai-store',
            [
                'banyakModul' => $banyakModul[0]->hitung,
                'modulMateri' => $modulMateri,
                'idModul' => $idModul,
                'praktikan' => $praktikan,
                'idPraktikum' => $req->nama_praktikum,
                'nilai' => $nilaiSemuaPraktikan,
                'idPeriodePraktikumDosen' => $idPeriodePraktikumDosen,
            ]
        );
    }

    public function entry_nilai_store(Request $req)
    {
        for ($i = 0; $i < count($req->materi); $i++) {
            DB::table('detail_materi_dosen')
                ->join('penilaian_dosen', 'detail_materi_dosen.penilaian_dosen_id', 'penilaian_dosen.id')
                ->join('praktikan', 'penilaian_dosen.praktikan_id', 'praktikan.id')
                ->join('mahasiswa', 'praktikan.mahasiswa_id', 'mahasiswa.id')
                ->join('periode_praktikum_dosen', 'penilaian_dosen.periode_praktikum_dosen_id', 'periode_praktikum_dosen.id')
                ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
                ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
                ->join('periode_praktikum', 'praktikan.periode_praktikum_id', 'periode_praktikum.id')
                ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
                ->join('modul_materi_dosen', 'detail_materi_dosen.modul_materi_dosen_id', 'modul_materi_dosen.id')
                ->join('materi', 'modul_materi_dosen.materi_id', 'materi.id')
                ->where('praktikum.id', $req->idPraktikum)
                ->where('periode_praktikum.status', 1)
                ->where('dosen.nip', Auth::user()->username)
                ->where('mahasiswa.npm', $req->npm)
                ->where('modul_materi_dosen.modul_dosen_id', $req->idModul)
                ->where('modul_materi_dosen.materi_id', $req->materiId[$i])
                ->update(['detail_materi_dosen.nilai_materi' => $req->materi[$i]]);
        }

        $banyakModul = DB::select('select count(*) as hitung from
                    (SELECT modul_dosen_id from modul_materi_dosen
                    JOIN materi on modul_materi_dosen.materi_id = materi.id
                    JOIN praktikum on materi.praktikum_id = praktikum.id
                    where praktikum_id =' . $req->idPraktikum . '
                    GROUP by modul_dosen_id)
                    as hitung;');

        $na = array();
        for ($i = 1; $i <= $banyakModul[0]->hitung; $i++) {
            $avg = Penilaian_dosen::select(DB::raw('avg(detail_materi_dosen.nilai_materi) as rata_rata'))
                ->join('praktikan', 'penilaian_dosen.praktikan_id', 'praktikan.id')
                ->join('mahasiswa', 'praktikan.mahasiswa_id', 'mahasiswa.id')
                ->join('periode_praktikum_dosen', 'penilaian_dosen.periode_praktikum_dosen_id', 'periode_praktikum_dosen.id')
                ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
                ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
                ->join('detail_materi_dosen', 'penilaian_dosen.id', 'detail_materi_dosen.penilaian_dosen_id')
                ->join('modul_materi_dosen', 'detail_materi_dosen.modul_materi_dosen_id', 'modul_materi_dosen.id')
                ->join('periode_praktikum', 'praktikan.periode_praktikum_id', 'periode_praktikum.id')
                ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
                ->where('dosen.nip', Auth::user()->username)
                ->where('mahasiswa.npm', $req->npm)
                ->where('praktikum.id', $req->idPraktikum)
                ->where('periode_praktikum.status', 1)
                ->where('modul_materi_dosen.modul_dosen_id', $i)
                ->first();

            if ($avg) {
                array_push($na, $avg->rata_rata);
            } else {
                array_push($na, '0');
            }
        }
        $total = 0;
        for ($i = 0; $i <= count($na) - 1; $i++) {
            $tmp[$i] = $na[$i];
            $total = $total + $tmp[$i];
        }
        $hasil = $total / count($na);

        DB::table('penilaian_dosen')
            ->where('penilaian_dosen.id', $req->idpenilaian)
            ->update(['penilaian_dosen.nilai_akhir' => $hasil]);

        return redirect()->back()->with(['success' => 'Nilai Berhasil Ditambahkan']);
    }

    // LIST PRAKTIKAN
    public function list_praktikan()
    {
        return view('dosen.list-praktikan');
    }

    public function list_praktikan_view(Request $req)
    {
        // OTW
        $praktikan = DB::table('mahasiswa')
            ->select('mahasiswa.npm as npm', 'mahasiswa.nama as namaMahasiswa', 'sesi.nama as namaSesi', 'sesi.waktu as waktu', 'penilaian_dosen.id as idPenilaian', 'praktikan.id as praktikanID', 'penilaian_dosen.periode_praktikum_dosen_id as idDosen', 'penilaian_dosen.nilai_akhir as nilaiAkhir')
            ->join('praktikan', 'praktikan.mahasiswa_id', 'mahasiswa.id')
            ->join('penilaian_dosen', 'praktikan.id', 'penilaian_dosen.praktikan_id')
            ->join('periode_praktikum_dosen', 'penilaian_dosen.periode_praktikum_dosen_id', 'periode_praktikum_dosen.id')
            ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
            ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
            ->join('periode_praktikum', 'praktikan.periode_praktikum_id', 'periode_praktikum.id')
            ->join('sesi', 'praktikan.sesi_id', 'sesi.id')
            ->where('periode_praktikum.praktikum_id', $req->nama_praktikum)
            ->where('dosen.nip', Auth::user()->username)
            ->get();

        if (count($praktikan) == 0) {
            return redirect()->back()->with(['error' => 'Anda Tidak Mempunyai Praktikan']);
        }


        $banyakModul = DB::select('select count(*) as hitung from
                    (SELECT modul_dosen_id from modul_materi_dosen
                    JOIN materi on modul_materi_dosen.materi_id = materi.id
                    JOIN praktikum on materi.praktikum_id = praktikum.id
                    where praktikum_id =' . $req->nama_praktikum . '
                    GROUP by modul_dosen_id)
                    as hitung;');

        $idModulMateri = DB::table('modul_materi_dosen')
            ->select('modul_materi_dosen.id as idModul')
            ->join('materi', 'modul_materi_dosen.materi_id', 'materi.id')
            ->join('praktikum', 'materi.praktikum_id', 'praktikum.id')
            ->where('praktikum_id', $req->nama_praktikum)
            ->get();

        $nilaiSemuaPraktikan = array();
        foreach ($praktikan as $p) {
            $nilaiPraktikan = array();
            for ($i = 1; $i <= $banyakModul[0]->hitung; $i++) {
                $nilai = Detail_materi_dosen::select(DB::raw('AVG(detail_materi_dosen.nilai_materi) as avg'))
                    ->join('modul_materi_dosen', 'detail_materi_dosen.modul_materi_dosen_id', 'modul_materi_dosen.id')
                    ->join('penilaian_dosen', 'detail_materi_dosen.penilaian_dosen_id', 'penilaian_dosen.id')
                    ->join('materi', 'modul_materi_dosen.materi_id', 'materi.id')
                    ->join('praktikum', 'materi.praktikum_id', 'praktikum.id')
                    ->where('detail_materi_dosen.penilaian_dosen_id', $p->idPenilaian)
                    ->where('penilaian_dosen.praktikan_id', $p->praktikanID)
                    ->where('modul_materi_dosen.modul_dosen_id', $idModulMateri[$i - 1]->idModul)
                    ->where('materi.praktikum_id', $req->nama_praktikum)
                    ->first();

                if ($nilai) {
                    array_push($nilaiPraktikan, $nilai->avg);
                } else {
                    array_push($nilaiPraktikan, '0');
                }
            }
            array_push($nilaiSemuaPraktikan, $nilaiPraktikan);
        }

        $idLaboratorium = DB::table('praktikum')
            ->select('praktikum.nama as namaPraktikum', 'laboratorium.nama as namaLab', 'laboratorium_id as idLab')
            ->join('laboratorium', 'praktikum.laboratorium_id', 'laboratorium.id')
            ->where('praktikum.id', $req->nama_praktikum)
            ->first();

        $idDosenLab = DB::table('dosen_laboratorium')
            ->select('dosen_laboratorium.id as idDosenLab', 'dosen.nama as namaDosen', 'laboratorium.nama as namaLab', 'periode_praktikum_dosen.status as status', 'dosen_laboratorium.id as idLab')
            ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
            ->join('laboratorium', 'dosen_laboratorium.laboratorium_id', 'laboratorium.id')
            ->join('periode_praktikum_dosen', 'dosen_laboratorium.id', 'periode_praktikum_dosen.dosen_laboratorium_id')
            ->where('dosen_laboratorium.laboratorium_id', $idLaboratorium->idLab)
            ->where('dosen.nip', Auth::user()->username)
            ->first();

        return view('dosen.list-praktikan-view', [
            'banyakModul' => $banyakModul[0]->hitung,
            'idPraktikum' => $req->nama_praktikum,
            'praktikan' => $praktikan,
            'avg' => $nilaiSemuaPraktikan,
            'idDosenLab' => $idDosenLab,
        ]);
    }


    public function kirimNilai(Request $req)
    {
        DB::table('periode_praktikum_dosen')
            ->join('dosen_laboratorium', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
            ->join('dosen', 'dosen_laboratorium.dosen_id', 'dosen.id')
            ->where('periode_praktikum_dosen.dosen_laboratorium_id', $req->idLab)
            ->where('dosen.nip', Auth::user()->username)
            ->update(['periode_praktikum_dosen.status' => 1]);

        return redirect()->back()->with(['success' => 'Nilai Berhasil Dikirim']);
    }

    public function get_pdf($idPraktikum)
    {

        // $dataDetailModul = DB::select('select count(*) as hitung from
        //                     (SELECT modul_id from detail_modul
        //                     JOIN materi on detail_modul.materi_id = materi.id
        //                     JOIN praktikum on materi.praktikum_id = praktikum.id
        //                     where praktikum_id = '.$idPraktikum.'
        //                     GROUP by modul_id)
        //                     as hitung');

        // $avg = Detail_materi::select(DB::raw('avg(detail_materi.nilai_materi) as rata_rata'))
        //                         ->join('materi','materi.id','=','detail_materi.materi_id')
        //                         ->join('detail_modul','detail_modul.materi_id','=','materi.id')
        //                         ->join('praktikum','praktikum.id','=','materi.praktikum_id')
        //                         ->where('praktikum.id','=',''.$idPraktikum)
        //                         ->groupBy('detail_modul.modul_id')
        //                         ->get();

        // $nilai_akhir = Penilaian_dosen::select('nilai_akhir')
        //                 ->join('dosen','penilaian_dosen.dosen_id','=','dosen.id')
        //                 ->join('praktikum','dosen.laboratorium_id','=','praktikum.id')
        //                 ->where('praktikum.id','=',''.$idPraktikum)
        //                 ->get();
        $det = "xxx";
        // $det=array('nilai_satu','nilai_dua','nilai_tiga','nilai_empat','nilai_lima','nilai_enam');
        // $praktikan = '';
        // switch ($dataDetailModul[0]->hitung) {
        //     case 1:
        //         $praktikan = DB::select('select nilai_1.npm,nilai_1.nama_mahasiswa,nilai_1.nama_sesi,nilai_1.nilai_satu,nilai_1.nilai_akhir from nilai_1 where nilai_1.id_praktikum='.$idPraktikum.' and nilai_1.nip='.Auth::user()->username);
        //     break;
        //     case 2:
        //         $praktikan = DB::select('select nilai_1.npm,nilai_1.nama_mahasiswa,nilai_1.nama_sesi,nilai_1.nilai_satu,nilai_2.nilai_dua,nilai_1.nilai_akhir from nilai_1,nilai_2 where nilai_1.npm = nilai_2.npm and nilai_1.id_praktikum='.$idPraktikum.' and nilai_1.nip='.Auth::user()->username);
        //     break;
        //     case 3:
        //         $praktikan = DB::select('select nilai_1.npm,nilai_1.nama_mahasiswa,nilai_1.nama_sesi,nilai_1.nilai_satu,nilai_2.nilai_dua,nilai_3.nilai_tiga,nilai_1.nilai_akhir from nilai_1,nilai_2,nilai_3 where nilai_1.npm = nilai_2.npm and nilai_2.npm = nilai_3.npm and nilai_1.id_praktikum='.$idPraktikum.' and nilai_1.nip='.Auth::user()->username);
        //     break;
        // }

        $pdf = PDF::loadView('dosen.laporan-penilaian', [
            // 'dataDetailModul' => $dataDetailModul[0]->hitung,
            // 'idPraktikum' => $idPraktikum,
            // 'praktikan' => $praktikan,
            'det' => $det,
        ]);
        return $pdf->stream('nilai.pdf');
    }

    public function sendEmailExcel($nama_praktikum)
    {

        // $dataDetailModul = DB::select('select count(*) as hitung from
        //                     (SELECT modul_id from detail_modul
        //                     JOIN materi on detail_modul.materi_id = materi.id
        //                     JOIN praktikum on materi.praktikum_id = praktikum.id
        //                     where praktikum_id = '.$nama_praktikum.'
        //                     GROUP by modul_id)
        //                     as hitung');

        // $avg = Detail_materi::select(DB::raw('avg(detail_materi.nilai_materi) as rata_rata'))
        //                         ->join('materi','materi.id','=','detail_materi.materi_id')
        //                         ->join('detail_modul','detail_modul.materi_id','=','materi.id')
        //                         ->join('praktikum','praktikum.id','=','materi.praktikum_id')
        //                         ->where('praktikum.id','=',''.$nama_praktikum)
        //                         ->groupBy('detail_modul.modul_id')
        //                         ->get();

        // $nilai_akhir = Penilaian_dosen::select('nilai_akhir')
        //                 ->join('dosen','penilaian_dosen.dosen_id','=','dosen.id')
        //                 ->join('praktikum','dosen.laboratorium_id','=','praktikum.id')
        //                 ->where('praktikum.id','=',''.$nama_praktikum)
        //                 ->get();

        // $det=array('nilai_satu','nilai_dua','nilai_tiga','nilai_empat','nilai_lima','nilai_enam');
        // $praktikan = '';
        // switch ($dataDetailModul[0]->hitung) {
        //     case 1:
        //         $praktikan = DB::select('select nilai_1.npm,nilai_1.nama_mahasiswa,nilai_1.nama_sesi,nilai_1.nilai_satu,nilai_1.nilai_akhir from nilai_1 where nilai_1.id_praktikum='.$nama_praktikum.' and nilai_1.nip='.Auth::user()->username);
        //     break;
        //     case 2:
        //         $praktikan = DB::select('select nilai_1.npm,nilai_1.nama_mahasiswa,nilai_1.nama_sesi,nilai_1.nilai_satu,nilai_2.nilai_dua,nilai_1.nilai_akhir from nilai_1,nilai_2 where nilai_1.npm = nilai_2.npm and nilai_1.id_praktikum='.$nama_praktikum.' and nilai_1.nip='.Auth::user()->username);
        //     break;
        //     case 3:
        //         $praktikan = DB::select('select nilai_1.npm,nilai_1.nama_mahasiswa,nilai_1.nama_sesi,nilai_1.nilai_satu,nilai_2.nilai_dua,nilai_3.nilai_tiga,nilai_1.nilai_akhir from nilai_1,nilai_2,nilai_3 where nilai_1.npm = nilai_2.npm and nilai_2.npm = nilai_3.npm and nilai_1.id_praktikum='.$nama_praktikum.' and nilai_1.nip='.Auth::user()->username);
        //     break;
        // }

        // $attachment =  Excel::download(new DosenViewExcel($dataDetailModul[0]->hitung, $nama_praktikum,$praktikan,$det), 'nilai.xlsx')->getFile();

        // Mail::raw('Nilai Praktikum',function ($message) use ($attachment) {
        Mail::raw('Nilai Praktikum', function ($message) {
            $message->from('futaikhi.fut@gmail.com', 'Futaikhi');
            $message->to('farid.usnadi@gmail.com', 'Bangke');
            $message->subject('Nilai cuk');
            // $message->attach($attachment,
            //         ['as' => 'nilai.xlsx']
            // );
        });

        return view('dosen.dosen', [
            // 'dataDetailModul' => $dataDetailModul[0]->hitung,
            // 'nama_praktikum' => $nama_praktikum,
            // 'praktikan' => $praktikan,
            // 'det' => $det,
        ]);
    }


    public function change_password(Request $req)
    {

        if (Hash::check($req->old, Auth::user()->password)) {
            if ($req->new1 == $req->new2) {
                User::find(Auth::user()->id)->update([
                    'password' => Hash::make($req->new1),
                ]);
                return redirect()->back()->with(['success' => 'Password Berhasil Dirubah']);
            } else {
                return redirect()->back()->with(['warning' => 'Password Baru Tidak Sama']);
            }
        } else {
            return redirect()->back()->with(['warning' => 'Password Lama Tidak Sama']);
        }
    }

    public function daftarPraktikum(Request $request)
    {
        $id_dosen = DB::table('user')
            ->select('dosen.id')
            ->join('dosen', 'dosen.nip', '=', 'user.username')
            ->where('user.username', $request->username)
            ->first();

        //        dd($id_dosen);

        $praktikum = DB::table('dosen')
            ->select('praktikum.nama as namaPraktikum', 'periode_praktikum.id as idPraktikum')
            ->join('dosen_laboratorium', 'dosen_laboratorium.dosen_id', 'dosen.id')
            ->join('periode_praktikum_dosen', 'periode_praktikum_dosen.dosen_laboratorium_id', 'dosen_laboratorium.id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'periode_praktikum_dosen.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', 'periode_praktikum.praktikum_id')
            ->where('dosen.id', $id_dosen->id)
            ->where('periode_praktikum.status', 1)
            ->get();

//        return response()->json(compact($praktikum));
        return $praktikum->toArray();
    }

    public function listModul($praktikum)
    {
        $modul = DB::table('modul_dosen')
            ->select('modul_dosen.id as idModul',
                'modul_dosen.nama as namaModul')
            ->join('periode_praktikum', 'periode_praktikum.id', 'modul_dosen.periode_praktikum_id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('modul_dosen.periode_praktikum_id', '=', $praktikum)
            ->where('periode_praktikum.status', '=', 1)
            ->get();

        return $modul->toArray();
    }

    public function listMateri($modul)
    {
        $materi = DB::table('modul_materi_dosen')
            ->select('materi.nama as namaMateri',
                'materi.id as idMateri')
            ->join('modul_dosen', 'modul_dosen.id', 'modul_materi_dosen.modul_dosen_id')
            ->join('materi', 'materi.id', 'modul_materi_dosen.materi_id')
            ->join('periode_praktikum', 'modul_dosen.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('modul_materi_dosen.modul_dosen_id', '=', $modul)
            ->where('periode_praktikum.status', '=', 1)
            ->get();

        //        dd($materi);

        return $materi->toArray();
    }

    public function threadList($materi)
    {
        $thread = DB::table('thread')
            ->select('thread.id as idThread',
                'thread.judul',
                'thread.keterangan',
                'mahasiswa.nama as namaMahasiswa')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'praktikum.id')
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('thread.materi_id', '=', $materi)
            ->get();

        //        dd($thread);
        return $thread->toArray();
    }

    public function view($thread)
    {
        $materi = DB::table('thread')
            ->select(
                'thread.id as idThread',
                'thread.judul as judulThread',
                'thread.keterangan as ket',
                'thread.created_at as posted',
                'materi.id as idMateri',
                'materi.nama as namaMateri',
                'modul_dosen.id as idModulDosen',
                'modul_dosen.nama as namaModulDosen',
                'praktikum.id as idPraktikum',
                'praktikum.nama as namaPraktikum',
                'user.username as username',
                'mahasiswa.nama as namaMahasiswa'
            )
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('modul_materi_dosen', 'modul_materi_dosen.materi_id', 'materi.id')
            ->join('modul_dosen', 'modul_dosen.id', 'modul_materi_dosen.modul_dosen_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('thread.id', '=', $thread)
            ->where('periode_praktikum.status', '=', 1)
            ->first();

        return response()->json($materi, 200);
    }

    public function commentPrakikan($thread)
    {
        $MComment = DB::table('comment')
            ->select(
                'comment.id as idComment',
                'comment.comment',
                'comment.created_at',
                'thread.status as status',
                'thread.id as idThread',
                'user.username as username',
                'mahasiswa.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by','=',5)
            ->latest('comment.created_at')
            //            ->paginate(2);
            ->get();

        return $MComment->toArray();
    }

    public function commentDosen($thread)
    {
        $DComment = DB::table('comment')
            ->select(
                'comment.id as idComment',
                'comment.comment',
                'thread.status as status',
                'comment.created_at',
                'thread.id as idThread',
                'user.username as username',
                'dosen.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('dosen', 'dosen.nip', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by','=',3)
            ->latest('comment.created_at')
            ->get();

        return $DComment->toArray();
    }

    public function commentAslab($thread)
    {
        $AComment = DB::table('comment')
            ->select(
                 'comment.id as idComment',
                'comment.comment',
                'comment.created_at',
                'thread.status as status',
                'thread.id as idThread',
                'aslab.username as username',
                'aslab.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('aslab', 'aslab.id', 'comment.comment_id')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by','=',4)
            ->latest('comment.created_at')
            //            ->paginate(2);
            ->get();

        return $AComment->toArray();
    }

    public function addThreadComment(Request $request, Thread $thread)
    {
        $this->validate($request, [
            'comment' => 'required',
        ]);

        $comment = Comment::create([
            'comment' => $request->comment,
            'thread_id' => $thread->id,
            'comment_id' => $request->user_id,
            'comment_by' => 3
        ]);

        return response()->json($comment, 201);
    }
}
