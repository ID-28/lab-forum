<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Aslab;
use App\Mahasiswa;
use App\Comment;
use App\Thread;
use Session;
use Redirect;
use DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AbsensiPraktikanAslab;

class ApiAslabController extends Controller
{
//    public function __construct()
//    {
//        session_start();
//        if (!isset($_SESSION['aslab'])) {
//            Redirect::to('/')->send();
//        }
//    }

    public function index()
    {
        return view('aslab.aslab');
    }
    public function entryNilai()
    {
        return view('aslab/entryNilai');
    }

    public function listPraktikan()
    {
        $idAslab = $_SESSION['aslab']->id;
        $praktikan = DB::table('penilaian_aslab')
            ->select('mahasiswa.id as idMahasiswa', 'mahasiswa.npm as npm', 'mahasiswa.nama as namaMahasiswa', 'sesi.nama as namaSesi', 'sesi.waktu as waktu', 'praktikan.id as praktikanID')
            ->join('praktikan', 'praktikan.id', 'penilaian_aslab.praktikan_id')
            ->join('mahasiswa', 'mahasiswa.id', 'praktikan.mahasiswa_id')
            ->join('sesi', 'sesi.id', 'praktikan.sesi_id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'sesi.periode_praktikum_id')
            ->join('periode_praktikum_aslab', 'periode_praktikum_aslab.id', 'penilaian_aslab.periode_praktikum_aslab_id')
            ->where('periode_praktikum.status', 1)
            ->where('periode_praktikum_aslab.aslab_id', $idAslab)
            ->get();

        return view('aslab.listPraktikan', compact('praktikan'));
    }

    public function listPraktikanView($id)
    {
        $user = Mahasiswa::findOrFail($id);
        return view('aslab.praktikanViewDetail', compact('user'));
    }

    public function entryNilaiPraktikan()
    {
        return view('aslab.entryNilaiPraktikan');
    }

    public function change_password(Request $req)
    {

        $user = Aslab::where('id', $_SESSION['aslab']->id)->first();
        if (Hash::check($req->old, $user->password)) {
            if ($req->new1 == $req->new2) {
                $user->password =  Hash::make($req->new1);
                $user->save();
                return redirect()->back()->with(['success' => 'Password Berhasil Dirubah']);
            } else {
                return redirect()->back()->with(['warning' => 'Password Baru Tidak Sama']);
            }
        } else {
            return redirect()->back()->with(['warning' => 'Password Lama Tidak Sama']);
        }
    }

    public function cetakListPraktikanAslab()
    {
        $idAslab = $_SESSION['aslab']->id;
        return (Excel::download(new AbsensiPraktikanAslab($idAslab), 'Absensi Praktikan.xlsx'));
    }

    public function listPraktikum(Request $request)
    {
        $idAslab = DB::table('aslab')
            ->select('aslab.id', 'aslab.username')
            ->where('aslab.username', $request->username)
            ->first();

//         dd($idAslab);
        //        dd($id_mahasiswa);

        $praktikum = DB::table('periode_praktikum_aslab')
            ->select('praktikum.nama as namaPraktikum', 'periode_praktikum.id as idPraktikum')
            ->join('periode_praktikum', 'periode_praktikum.id', 'periode_praktikum_aslab.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', 'periode_praktikum.praktikum_id')
            ->where('periode_praktikum_aslab.aslab_id', $idAslab->id)
            ->where('periode_praktikum.status', 1)
            ->get();

        // dd($praktikum);

        return $praktikum->toArray();
    }

    public function listModul($praktikum)
    {
        $modul = DB::table('modul_aslab')
            ->select('modul_aslab.id as idModul', 'modul_aslab.nama as namaModul')
            ->join('periode_praktikum', 'periode_praktikum.id', 'modul_aslab.periode_praktikum_id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('modul_aslab.periode_praktikum_id', '=', $praktikum)
            ->get();

        // dd($jenis_user);

        return $modul->toArray();
    }

    public function listMateri($modul)
    {
        $materi = DB::table('modul_materi_aslab')
            ->select('materi.nama as namaMateri',
                'materi.id as idmat')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('materi', 'materi.id', 'modul_materi_aslab.materi_id')
            ->join('periode_praktikum', 'modul_aslab.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('modul_materi_aslab.modul_aslab_id', '=', $modul)
            ->get();

        //        dd($materi);

        return $materi->toArray();
    }

    public function threadList($materi)
    {
        $thread = DB::table('thread')
            ->select('thread.id as idThread',
                'thread.judul',
                'thread.keterangan',
                'mahasiswa.nama as namaMahasiswa')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'praktikum.id')
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('thread.materi_id', '=', $materi)
            ->get();

        //        dd($thread);

        return $thread->toArray();
    }

    public function view($thread)
    {
        $materi = DB::table('thread')
            ->select(
                'thread.id as idThread',
                'thread.judul as judulThread',
                'thread.status',
                'thread.keterangan as ket',
                'thread.created_at as posted',
                'materi.id as idMateri',
                'materi.nama as namaMateri',
                'modul_aslab.id as idModulAslab',
                'modul_aslab.nama as namaModulAslab',
                'praktikum.id as idPraktikum',
                'praktikum.nama as namaPraktikum',
                'user.username as username',
                'mahasiswa.nama as namaMahasiswa'
            )
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('modul_materi_aslab', 'modul_materi_aslab.materi_id', 'materi.id')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('thread.id', '=', $thread)
            ->where('periode_praktikum.status', '=', 1)
            ->first();

        return response()->json($materi, 200);
    }

    public function commentPrakikan($thread)
    {
        $MComment = DB::table('comment')
            ->select(
               'comment.id as idComment',
               'comment.comment',
               'thread.status as status',
               'comment.created_at',
               'thread.id as idThread',
               'user.username as username',
               'mahasiswa.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by','=', 5)
            ->latest('comment.created_at')
            //            ->paginate(2);
            ->get();

        return $MComment->toArray();
    }

    public function commentDosen($thread)
    {
        $DComment = DB::table('comment')
            ->select(
                 'comment.id as idComment',
                 'comment.comment',
                 'thread.status as status',
                 'comment.created_at',
                 'thread.id as idThread',
                 'user.username as username',
                 'dosen.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('dosen', 'dosen.nip', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by', '=', 3)
            ->latest('comment.created_at')
            ->get();

        return $DComment->toArray();
    }

    public function commentAslab($thread)
    {
        $AComment = DB::table('comment')
            ->select(
                'comment.id as idComment',
                'comment.comment',
                'comment.created_at',
                'thread.status as status',
                'thread.id as idThread',
                'aslab.username as username',
                'aslab.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('aslab', 'aslab.id', 'comment.comment_id')
            ->where('comment.thread_id', '=', $thread)
            ->latest('comment.created_at')
            //            ->paginate(2);
            ->where('comment.comment_by','=',4)
            ->get();

        return $AComment->toArray();
    }

    public function addThreadComment(Request $request, Thread $thread)
    {
        $this->validate($request, [
            'comment' => 'required',
        ]);

        $comment = Comment::create([
            'comment' => $request->comment,
            'thread_id' => $thread->id,
            'comment_id' => $request->user_id,
            'comment_by' => 4
        ]);

        return response()->json($comment, 201);
    }
}
