<?php

namespace App\Http\Controllers\API;

use App\Comment;
use App\Http\Requests\StoreThreadRequest;
use App\Thread;
use App\Praktikum;
use App\Periode_praktikum;
use App\Materi;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ApiThreadController extends Controller
{
    public function index(Request $request)
    {
        $jenisUser = DB::table('user')
            ->select('jenis_user_id as jenis_user')
            ->where('user.id','=',$request->username)
            ->first();

        $datamateri = DB::table('materi')
            ->select('materi.id as idMateri','modul_aslab.id as idModul','modul_dosen.id as idModulDosen','praktikum.id as idPraktikum','materi.nama as namaMateri','praktikum.nama as namaPraktikum','modul_aslab.nama as namaModul', 'modul_dosen.nama as modulDosen')
            ->join('praktikum','praktikum.id','materi.praktikum_id')
            ->join('periode_praktikum','periode_praktikum.praktikum_id','praktikum.id')
            ->join('modul_materi_aslab','modul_materi_aslab.materi_id','materi.id')
            ->join('modul_materi_dosen','modul_materi_dosen.materi_id','materi.id')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('modul_dosen','modul_dosen.id','modul_materi_dosen.modul_dosen_id')
            ->where('materi.id','=',$request->materi)
            ->first();

        $thread = DB::table('thread')
            ->select('thread.id as idThread','thread.judul','thread.keterangan','materi.nama as namaMateri','praktikum.nama as namaPraktikum','periode_praktikum.tahun','modul_aslab.nama as namaModul','materi.id as idMateri','mahasiswa.nama as namaMahasiswa')
            ->join('materi','materi.id','thread.materi_id')
            ->join('praktikum','praktikum.id','materi.praktikum_id')
            ->join('periode_praktikum','periode_praktikum.id','praktikum.id')
            ->join('modul_materi_aslab','modul_materi_aslab.materi_id','materi.id')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('user','user.id','thread.user_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->where('thread.materi_id','=',$request->materi)
            ->get();

//        dd($thread);
        return response()->json(compact('thread','datamateri','jenisUser'), 200);
    }

    public function create($materi)
    {
        $mate = DB::table('modul_materi_aslab')
            ->select('materi.id as idmat','materi.nama as namaMate')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('materi','materi.id','modul_materi_aslab.materi_id')
            ->join('periode_praktikum','modul_aslab.periode_praktikum_id','periode_praktikum.id')
            ->join('praktikum','periode_praktikum.praktikum_id','praktikum.id')
            ->where('materi.id','=',$materi)
//            ->take(5)
            ->first();

//        dd($mate);

        return view('thread.create',compact('mate'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|min:5',
            'keterangan'  => 'required|min:10',
            'materi_id'    => 'required'
        ]);

//        $thread = new Thread();
//        $thread->user_id = $request->user_id;
//        $thread->materi_id = $request->materi_id;
//        $thread->judul = $request->judul;
//        $thread->keterangan = $request->keterangan;
//
//        $thread->save();

//        dd($request->all());

        $thread = Thread::create([
            'user_id' => $request->get('user_id'),
            'materi_id' => $request->get('materi_id'),
            'judul' => $request->get('judul'),
            'keterangan' =>$request->get('keterangan'),
        ]);

        return response()->json($thread,201);
    }

    public function view($thread)
    {
        $materi = DB::table('thread')
            ->select('thread.id as idThread','thread.judul as judulThread', 'thread.status','thread.keterangan as ket', 'materi.nama as namaMateri','user.username as username', 'materi.id as idMateri','mahasiswa.nama as namaMahasiswa')
            ->join('materi','materi.id','thread.materi_id')
            ->join('user','user.id','thread.user_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->where('thread.id','=', $thread)
            ->first();

        $MComment = DB::table('comment')
            ->select('comment.id as idComment','comment.comment','thread.id as idThread','thread.status as state','thread.user_id as user_id','user.username as username','mahasiswa.nama as nama')
            ->join('thread','thread.id','comment.thread_id')
            ->join('user','user.id','comment.user_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->where('comment.thread_id','=',$thread)
            ->latest('comment.created_at')
            ->get();

//        dd($MComment);

        $DComment = DB::table('comment')
            ->select('comment.comment','thread.status as state','user.id','user.username as username','dosen.nama as nama')
            ->join('thread','thread.id','comment.thread_id')
            ->join('user','user.id','comment.user_id')
            ->join('dosen','dosen.nip','user.username')
            ->where('comment.thread_id','=',$thread)
            ->latest('comment.created_at')
            ->get();

        return response()->json(compact('materi','DComment','MComment'), 201);
    }

    public function addThreadComment(Request $request, Thread $thread){
        $this->validate($request,[
            'body'=>'required',
        ]);

        $comment = Comment::create([
            'comment' => $request->body,
            'thread_id' => $thread->id,
            'user_id' => \Auth::user()->id,
            'commentable_id' => $thread->id,
            'commentable_type' => 'App\Thread',
        ]);

        return response()->json($comment, 201);
    }

    public function listThread()
    {
        $thread = DB::table('thread')
            ->select('thread.id as idThread','thread.judul','thread.keterangan','materi.nama as namaMateri','praktikum.nama as namaPraktikum','periode_praktikum.tahun','modul_aslab.nama as namaModul','materi.id as idMateri','mahasiswa.nama as namaMahasiswa')
            ->join('materi','materi.id','thread.materi_id')
            ->join('praktikum','praktikum.id','materi.praktikum_id')
            ->join('periode_praktikum','periode_praktikum.id','praktikum.id')
            ->join('modul_materi_aslab','modul_materi_aslab.materi_id','materi.id')
//            ->join('modul_materi_aslab','modul_materi_aslab.modul_aslab_id','modul_aslab.id')
            ->join('modul_aslab','modul_aslab.id','modul_materi_aslab.modul_aslab_id')
            ->join('user','user.id','thread.user_id')
            ->join('mahasiswa','mahasiswa.npm','user.username')
            ->get();


        return response()->json($thread, 201);
    }

    public function markAsSolution()
    {
//        dd(input::all());
        $statusId=input::get('statusId');
        $threadId=input::get('threadId');
        $thread=Thread::find($threadId);
        $thread->status=$statusId;

        return redirect()->back();
    }

    public function ThreadAuth() {
        $data = "Welcome " . Auth::user()->username;
        return response()->json($data, 200);
    }
}
