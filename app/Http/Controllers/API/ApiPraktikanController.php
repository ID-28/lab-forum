<?php

namespace App\Http\Controllers\API;

use App\Comment;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Sesi;
use App\Asal_kelas;
use App\Praktikan;
use App\Mahasiswa;
use App\User;
use App\Berita;
use App\Penilaian_aslab;
use App\Penilaian_dosen;
use Illuminate\Support\Facades\Input;
use PDF;

class ApiPraktikanController extends Controller
{
    public function beranda()
    {
        if (\Auth::check()) {
            return back();
        }
        return view('pages.index');
    }
    public function index(Request $request)
    {
        $id_mahasiswa = DB::table('user')
            ->select('mahasiswa.id')
            ->join('mahasiswa', 'mahasiswa.npm', '=', 'user.username')
            ->where('user.username', $request->username)
            ->first();

        $praktikan = DB::table('praktikan')
            ->select('praktikan.id', 'praktikum.nama as namaPraktikum', 'periode_praktikum.tahun', 'sesi.nama as namaSesi', 'sesi.waktu', 'praktikan.status')
            ->join('sesi', 'sesi.id', '=', 'praktikan.sesi_id')
            ->join('periode_praktikum', 'periode_praktikum.id', '=', 'sesi.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', '=', 'periode_praktikum.praktikum_id')
            ->where('praktikan.mahasiswa_id', $id_mahasiswa->id)
            ->get();

        $aslabAsdos = array();
        if (count($praktikan) > 0) {
            foreach ($praktikan as $p) {
                if ($p->status == 1) {
                    $aslab = DB::table('penilaian_aslab')
                        ->select('aslab.nama as namaAslab', 'aslab.no_tlpn as tlpnAslab', 'aslab.username as NPM')
                        ->join('periode_praktikum_aslab', 'periode_praktikum_aslab.id', 'penilaian_aslab.periode_praktikum_aslab_id')
                        ->join('aslab', 'periode_praktikum_aslab.aslab_id', 'aslab.id')
                        ->where('praktikan_id', $p->id)
                        ->first();

                    $dosen = DB::table('penilaian_dosen')
                        ->select('dosen.nama as namaDosen', 'dosen.no_tlpn as tlpnDosen', 'dosen.nip as NIP')
                        ->join('periode_praktikum_dosen', 'periode_praktikum_dosen.id', 'penilaian_dosen.periode_praktikum_dosen_id')
                        ->join('dosen_laboratorium', 'dosen_laboratorium.id', 'periode_praktikum_dosen.dosen_laboratorium_id')
                        ->join('dosen', 'dosen.id', 'dosen_laboratorium.dosen_id')
                        ->where('praktikan_id', $p->id)
                        ->first();

                    array_push($aslabAsdos, $aslab);
                    array_push($aslabAsdos, $dosen);
                }
            }
        }

        return $praktikan->toArray();
    }

    public function edit()
    {
        return view('praktikan.editProfile');
    }

    public function update(Request $req)
    {
        if ($req->has('nama')) {
            $this->validate($req, [
                'nama' => 'required'
            ]);

            $mahasiswa = Mahasiswa::where('npm', \Auth::user()->username)->firstOrFail();
            $mahasiswa->nama = ucfirst($req->get('nama'));
            $mahasiswa->save();
        }

        if ($req->has('password') && $req->get('password') != '') {
            $this->validate($req, [
                'password' => 'required'
            ]);

            $mahasiswa = User::where('username', \Auth::user()->username)->firstOrFail();
            $mahasiswa->password = Hash::make($req->get('password'));
            $mahasiswa->save();
        }

        if ($req->has('telp')) {
            $this->validate($req, [
                'telp' => 'required'
            ]);

            $mahasiswa = Mahasiswa::where('npm', \Auth::user()->username)->firstOrFail();
            $mahasiswa->no_tlpn = $req->get('telp');
            $mahasiswa->save();
        }

        if ($req->has('foto')) {
            $this->validate($req, [
                'foto' => 'required|mimes:jpeg,jpg,png|max:20000'
            ]);

            $foto = $req->file('foto');
            $nama_file = str_replace(".", "-", \Auth::user()->username) . "." . $foto->getClientOriginalExtension();
            $mahasiswa = Mahasiswa::where('npm', \Auth::user()->username)->firstOrFail();
            unlink('upload/foto/' . $mahasiswa->foto);
            if ($foto->move('upload/foto', $nama_file)) {
                $mahasiswa->foto = $nama_file;
                $mahasiswa->save();
            }
        }

        return redirect()->back()->with(['jenis' => 'success', 'pesan' => 'Data Berhasil diubah']);
    }

    public function sesiPraktikum()
    {
        $sesi = DB::table('sesi')
            ->select('sesi.*', 'praktikum.nama as nama_praktikum', 'praktikum.id as id_praktikum', 'periode_praktikum.tahun')
            ->join('periode_praktikum', 'periode_praktikum.id', '=', 'sesi.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', '=', 'periode_praktikum.praktikum_id')
            ->where('periode_praktikum.status', 1)
            ->get();
        return view('praktikan.sesiPraktikum', compact('sesi'));
    }

    public function registrasiPraktikum()
    {
        $sesi = DB::table('sesi')
            ->select('sesi.*', 'praktikum.nama as nama_praktikum', 'praktikum.id as id_praktikum', 'periode_praktikum.tahun')
            ->join('periode_praktikum', 'periode_praktikum.id', '=', 'sesi.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', '=', 'periode_praktikum.praktikum_id')
            ->where('periode_praktikum.status', 1)
            ->where('sesi.kuota', '>', 0)
            ->get();
        $kelas = Asal_kelas::all();
        return view('praktikan.registrasiPraktikum', compact('sesi', 'kelas'));
    }

    public function registrasiPraktikumProses(Request $req)
    {
        $this->validate($req, [
            'kelas' => 'required|numeric',
            'sesi' => 'required|numeric',
        ]);

        $mahasiswa = Mahasiswa::where('npm', '=', \Auth::User()->username)->first();

        $periodePraktikum = DB::table('sesi')
            ->select('periode_praktikum.id', 'periode_praktikum.tahun')
            ->join('periode_praktikum', 'periode_praktikum.id', '=', 'sesi.periode_praktikum_id')
            ->where('sesi.id', $req->sesi)
            ->first();

        $cekPraktikan = DB::table('praktikan')
            ->select('praktikan.*', 'sesi.*', 'periode_praktikum.*')
            ->join('sesi', 'sesi.id', '=', 'praktikan.sesi_id')
            ->join('periode_praktikum', 'periode_praktikum.id', '=', 'sesi.periode_praktikum_id')
            ->where('mahasiswa_id', $mahasiswa->id)
            ->where('periode_praktikum.id', $periodePraktikum->id)
            ->get();

        $sekarang = Carbon::now();

        if (count($cekPraktikan) > 0) {
            return redirect()->route('praktikan.registrasiPraktikum.view')->with(['jenis' => 'danger', 'pesan' => 'Anda sudah mendaftar praktikum tersebut']);
        }

        $praktikan = Praktikan::create([
            'periode_praktikum_id' => $periodePraktikum->id,
            'sesi_id' => $req->sesi,
            'asal_kelas_id' => $req->kelas,
            'mahasiswa_id' => $mahasiswa->id,
            'tgl_registrasi' => $sekarang,
            'status' => 0,
        ]);

        $sesi = Sesi::where('id', $req->sesi)->first();
        $sesi->kuota = $sesi->kuota - 1;
        $sesi->save();

        return redirect()->route('praktikan')->with(['jenis' => 'success', 'pesan' => 'Berhasil mendaftar praktikum, Silahkan cetak formulir dan verifikasi ke Aslab dengan membawa persyaratan']);
    }

    public function cetakFormulir($id)
    {
        $praktikan = DB::table('praktikan')
            ->select('praktikan.id', 'mahasiswa.npm', 'mahasiswa.nama', 'mahasiswa.foto', 'praktikum.nama as namaPraktikum', 'periode_praktikum.tahun', 'sesi.nama as namaSesi', 'sesi.waktu', 'praktikan.status', 'laboratorium.id as labId', 'laboratorium.nama as namaLab', 'laboratorium.ruangan as ruangLab')
            ->join('mahasiswa', 'mahasiswa.id', '=', 'praktikan.mahasiswa_id')
            ->join('sesi', 'sesi.id', '=', 'praktikan.sesi_id')
            ->join('periode_praktikum', 'periode_praktikum.id', '=', 'sesi.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', '=', 'periode_praktikum.praktikum_id')
            ->join('laboratorium', 'laboratorium.id', '=', 'praktikum.laboratorium_id')
            ->where('praktikan.id', $id)
            ->first();

        $waktu = Carbon::now()->format('d M Y');

        $pdf = PDF::loadView('praktikan.formulir', compact('praktikan', 'waktu'));
        return $pdf->download('formulir.pdf');
    }

    public function berita()
    {

        if (isset($_GET['lab'])) {
            $lab = 0;
            if ($_GET['lab'] == 'basprog') {
                $lab = '4';
            } else if ($_GET['lab'] == 'jarkom') {
                $lab = '3';
            } else if ($_GET['lab'] == 'rpl') {
                $lab = '2';
            }

            if ($lab == 0) {
                return redirect()->back();
            } else {
                $berita = Berita::where('user_id', $lab)->orderBy('updated_at', 'DESC')->paginate(10);
            }
        } else {
            $berita = Berita::orderBy('updated_at', 'DESC')->paginate(10);
        }

        return view('pages.berita', compact('berita'));
    }

    public function beritaDetail(Berita $berita)
    {

        $lainnya = Berita::inRandomOrder()->take(3)->offset(1)->get();

        return view('pages.beritaDetail', compact('berita', 'lainnya'));
    }

    public function listPraktikum(Request $request)
    {
        $id_mahasiswa = DB::table('user')
            ->select('mahasiswa.id')
            ->join('mahasiswa', 'mahasiswa.npm', '=', 'user.username')
            ->where('user.username', $request->username)
            ->first();

//                dd($id_mahasiswa);

        $praktikum = DB::table('praktikan')
            ->select('praktikum.nama as namaPraktikum', 'periode_praktikum.id as idPraktikum')
            ->join('periode_praktikum', 'periode_praktikum.id', 'praktikan.periode_praktikum_id')
            ->join('praktikum', 'praktikum.id', 'periode_praktikum.praktikum_id')
            ->where('praktikan.mahasiswa_id', $id_mahasiswa->id)
            ->where('periode_praktikum.status', 1)
            ->get();

//        dd($praktikum);

        return $praktikum->toArray();
    }

    public function listModul($praktikum)
    {
        $modul = DB::table('modul_aslab')
            ->select('modul_aslab.id as idModul', 'modul_aslab.nama as namaModul')
            ->join('periode_praktikum', 'periode_praktikum.id', 'modul_aslab.periode_praktikum_id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('modul_aslab.periode_praktikum_id', '=', $praktikum)
            ->get();

        return $modul->toArray();
    }

    public function listMateri($modul)
    {
        $materi = DB::table('modul_materi_aslab')
            ->select('materi.nama as namaMateri',
                'materi.id as id_materi')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('materi', 'materi.id', 'modul_materi_aslab.materi_id')
            ->join('periode_praktikum', 'modul_aslab.periode_praktikum_id', 'periode_praktikum.id')
            ->join('praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('modul_materi_aslab.modul_aslab_id', '=', $modul)
            ->get();

        //        dd($materi);

        return $materi->toArray();
    }

    public function threadList($materi)
    {
        $thread = DB::table('thread')
            ->select('materi.id as idMateri',
                'materi.nama as materi',
                'thread.id as idThread',
                'thread.judul',
                'thread.keterangan',
                'mahasiswa.nama as namaMahasiswa')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.id', 'praktikum.id')
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('thread.materi_id', '=', $materi)
            ->get();

        //        dd($thread);

        return $thread->toArray();
    }

    public function view($thread)
    {
        $materi = DB::table('thread')
            ->select(
                'thread.id as idThread',
                'thread.judul as judulThread',
                'thread.keterangan as ket',
                'thread.created_at as posted',
                'thread.user_id as poster',
                'materi.id as idMateri',
                'materi.nama as namaMateri',
                'modul_aslab.id as idModulAslab',
                'modul_aslab.nama as namaModulAslab',
                'praktikum.id as idPraktikum',
                'praktikum.nama as namaPraktikum',
                'user.username as username',
                'mahasiswa.nama as namaMahasiswa'
            )
            ->join('user', 'user.id', 'thread.user_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->join('materi', 'materi.id', 'thread.materi_id')
            ->join('modul_materi_aslab', 'modul_materi_aslab.materi_id', 'materi.id')
            ->join('modul_aslab', 'modul_aslab.id', 'modul_materi_aslab.modul_aslab_id')
            ->join('praktikum', 'praktikum.id', 'materi.praktikum_id')
            ->join('periode_praktikum', 'periode_praktikum.praktikum_id', 'praktikum.id')
            ->where('thread.id', '=', $thread)
            ->where('periode_praktikum.status', '=', 1)
            ->first();

        return response()->json($materi, 200);
    }

    public function commentPrakikan($thread)
    {
        $MComment = DB::table('comment')
            ->select(
                'comment.id as idComment',
                'comment.comment',
                'thread.status as status',
                'comment.created_at',
                'thread.id as idThread',
                'user.username as username',
                'mahasiswa.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('mahasiswa', 'mahasiswa.npm', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->where('comment.comment_by','=',5)
            ->latest('comment.created_at')
            //            ->paginate(2);
            ->get();

        return $MComment->toArray();
    }

    public function commentDosen($thread)
    {
        $DComment = DB::table('comment')
            ->select(
                'comment.id as idComment',
                'comment.comment',
                'thread.status as status',
                'comment.created_at',
                'thread.id as idThread',
                'user.username as username',
                'dosen.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('user', 'user.id', 'comment.comment_id')
            ->join('dosen', 'dosen.nip', 'user.username')
            ->where('comment.thread_id', '=', $thread)
            ->latest('comment.created_at')
            ->where('comment.comment_by','=',3)
            ->get();

        return $DComment->toArray();
    }

    public function commentAslab($thread)
    {
        $AComment = DB::table('comment')
            ->select(
                'comment.id as idComment',
                'comment.comment',
                'comment.created_at',
                'thread.status as status',
                'thread.id as idThread',
                'aslab.username as username',
                'aslab.nama as nama'
            )
            ->join('thread', 'thread.id', 'comment.thread_id')
            ->join('aslab', 'aslab.id', 'comment.comment_id')
            ->where('comment.thread_id', '=', $thread)
            ->latest('comment.created_at')
            ->where('comment.comment_by','=',4)
            //            ->paginate(2);
            ->get();

        return $AComment->toArray();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|min:5',
            'keterangan'  => 'required|min:10',
            'materi_id'    => 'required'
        ]);

        $thread = Thread::create([
            'user_id' => $request->get('user_id'),
            'materi_id' => $request->get('materi_id'),
            'judul' => $request->get('judul'),
            'keterangan' => $request->get('keterangan'),
            'status' => 0
        ]);

        return response()->json($thread, 201);
    }

    public function addThreadComment(Request $request, Thread $thread)
    {
        $this->validate($request, [
            'comment' => 'required',
        ]);

        $comment = Comment::create([
            'comment' => $request->comment,
            'thread_id' => $thread->id,
            'comment_id' => $request->user_id,
            'comment_by' => 5
        ]);

        return response()->json($comment, 201);
    }

    public function threadSolution(Request $request, Thread $thread)
    {
        $thread = Thread::where('id', $thread->id)
            ->update(['status' => $request->comment_id]);

        return response()->json($thread, 201);
    }
}
