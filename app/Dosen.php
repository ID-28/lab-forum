<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';
    public $primaryKey = 'id';
    protected $fillable = ["id","nip","nama" , "password" , "no_tlpn" ];

}
