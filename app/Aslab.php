<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Aslab extends Model
{
    protected $table = 'aslab';
    public $primaryKey = 'id';
    protected $fillable = ["laboratorium_id","nama","no_tlpn","kuota",'username','password','jenis_kelamin'];
}
