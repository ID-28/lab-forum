<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = 'peran';
    public $primaryKey = 'id';
    protected $fillable = ["nama"];

    public function izin()
    {
        return $this->belongsToMany(Izin::class,'peran_izin');
    }
}
