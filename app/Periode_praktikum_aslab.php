<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periode_praktikum_aslab extends Model
{
    protected $table = 'periode_praktikum_aslab';
    public $primaryKey = 'id';
    protected $fillable = ["periode_praktikum_id",'aslab_id','kuota','jenis_kelas_id'];

}
