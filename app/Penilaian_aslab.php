<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian_aslab extends Model
{
    protected $table = 'penilaian_aslab';
    public $primaryKey = 'id';
    protected $fillable = ["periode_praktikum_aslab_id","praktikan_id","nilai_akhir"];

}
