<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis_kelas extends Model
{
    protected $table = 'jenis_kelas';
    public $primaryKey = 'id';

    public function dosen() {
        return $this->hasMany(Dosen::class, 'jenis_kelas_id');
    }
    public function periode_praktikum_dosen()
    {
        return $this->hasMany(Jenis_kelas::class, 'jenis_kelas_id');
    }
}
