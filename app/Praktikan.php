<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Praktikan extends Model
{
    protected $table = 'praktikan';
    public $primaryKey = 'id';
    protected $fillable = ["periode_praktikum_id","sesi_id","mahasiswa_id","asal_kelas_id","tgl_registrasi","status"];

}
