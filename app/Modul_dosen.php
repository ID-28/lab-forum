<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul_dosen extends Model
{
    protected $table = 'modul_dosen';
    public $primaryKey = 'id';
    protected $fillable = ["periode_praktikum_id","nama"];

    public function periode_praktikum()
    {
        return $this->belongsTo(periode_praktikum::class, 'periode_praktikum_id');
    }

}
