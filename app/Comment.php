<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
//    use CommentableTrait;

    protected $table='comment';

    protected $fillable=['comment', 'thread_id', 'comment_id', 'comment_by'];

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
