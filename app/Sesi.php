<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesi extends Model
{
    protected $table = 'sesi';
    public $primaryKey = 'id';
    protected $fillable = ["nama" , "waktu" , "kuota",'periode_praktikum_id'];

}
