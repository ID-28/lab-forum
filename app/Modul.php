<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    protected $table = 'modul';
    public $primaryKey = 'id';
    protected $fillable = ["periode_praktikum_id","nama","status"];

    public function periode_praktikum()
    {
        return $this->belongsTo(periode_praktikum::class, 'periode_praktikum_id');
    }

    // public function detail_modul()
    // {
    //     return $this->hasMany(Detail_modul::class,'modul_id');
    // }
    


}
