<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenLaboratoriumTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'dosen_laboratorium';

    /**
     * Run the migrations.
     * @table dosen_laboratorium
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('laboratorium_id');
            $table->unsignedInteger('dosen_id');

            $table->index(["dosen_id"], 'fk_dosen_laboratorium_dosen1_idx');

            $table->index(["laboratorium_id"], 'fk_dosen_laboratorium_laboratorium1_idx');
            $table->nullableTimestamps();


            $table->foreign('laboratorium_id', 'fk_dosen_laboratorium_laboratorium1_idx')
                ->references('id')->on('laboratorium')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('dosen_id', 'fk_dosen_laboratorium_dosen1_idx')
                ->references('id')->on('dosen')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
