<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSesiTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'sesi';

    /**
     * Run the migrations.
     * @table sesi
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('periode_praktikum_id');
            $table->string('nama', 45)->nullable();
            $table->string('waktu', 45)->nullable();
            $table->integer('kuota')->nullable();

            $table->index(["periode_praktikum_id"], 'fk_sesi_periode_praktikum1_idx');
            $table->nullableTimestamps();


            $table->foreign('periode_praktikum_id', 'fk_sesi_periode_praktikum1_idx')
                ->references('id')->on('periode_praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
