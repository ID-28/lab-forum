<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulMateriAslabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'modul_materi_aslab';

    /**
     * Run the migrations.
     * @table modul_materi_aslab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('modul_aslab_id');
            $table->unsignedInteger('materi_id');

            $table->index(["modul_aslab_id"], 'fk_modul_materi_aslab_modul_aslab1_idx');

            $table->index(["materi_id"], 'fk_modul_materi_aslab_materi1_idx');
            $table->nullableTimestamps();


            $table->foreign('materi_id', 'fk_modul_materi_aslab_materi1_idx')
                ->references('id')->on('materi')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('modul_aslab_id', 'fk_modul_materi_aslab_modul_aslab1_idx')
                ->references('id')->on('modul_aslab')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
