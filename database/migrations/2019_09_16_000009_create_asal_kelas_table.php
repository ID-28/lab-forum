<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsalKelasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'asal_kelas';

    /**
     * Run the migrations.
     * @table asal_kelas
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('jenis_kelas_id');
            $table->string('nama', 45)->nullable();

            $table->index(["jenis_kelas_id"], 'fk_asal_kelas_jenis_kelas1_idx');
            $table->nullableTimestamps();


            $table->foreign('jenis_kelas_id', 'fk_asal_kelas_jenis_kelas1_idx')
                ->references('id')->on('jenis_kelas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
