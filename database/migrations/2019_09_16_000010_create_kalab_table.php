<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKalabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'kalab';

    /**
     * Run the migrations.
     * @table kalab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('laboratorium_id');
            $table->string('nama', 45)->nullable();

            $table->index(["laboratorium_id"], 'fk_kalab_laboratorium1_idx');
            $table->nullableTimestamps();


            $table->foreign('laboratorium_id', 'fk_kalab_laboratorium1_idx')
                ->references('id')->on('laboratorium')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
