<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianAslabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'penilaian_aslab';

    /**
     * Run the migrations.
     * @table penilaian_aslab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('periode_praktikum_aslab_id');
            $table->unsignedInteger('praktikan_id');
            $table->float('nilai_akhir')->nullable();

            $table->index(["periode_praktikum_aslab_id"], 'fk_penilaian_aslab_periode_praktikum_aslab1_idx');

            $table->index(["praktikan_id"], 'fk_penilaian_aslab_praktikan1_idx');
            $table->nullableTimestamps();


            $table->foreign('periode_praktikum_aslab_id', 'fk_penilaian_aslab_periode_praktikum_aslab1_idx')
                ->references('id')->on('periode_praktikum_aslab')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('praktikan_id', 'fk_penilaian_aslab_praktikan1_idx')
                ->references('id')->on('praktikan')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
