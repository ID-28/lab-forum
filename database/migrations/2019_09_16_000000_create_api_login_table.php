<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiLoginTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'api_login';

    /**
     * Run the migrations.
     * @table api_login
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('token')->nullable();
            $table->integer('login_by')->nullable();
            $table->integer('login_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
