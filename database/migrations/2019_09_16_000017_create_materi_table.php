<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'materi';

    /**
     * Run the migrations.
     * @table materi
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('praktikum_id');
            $table->string('nama', 200)->nullable();

            $table->index(["praktikum_id"], 'fk_materi_praktikum1_idx');
            $table->nullableTimestamps();


            $table->foreign('praktikum_id', 'fk_materi_praktikum1_idx')
                ->references('id')->on('praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
