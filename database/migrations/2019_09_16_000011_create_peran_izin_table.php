<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeranIzinTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'peran_izin';

    /**
     * Run the migrations.
     * @table peran_izin
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('peran_id');
            $table->unsignedInteger('izin_id');

            $table->index(["peran_id"], 'fk_peran_izin_peran1_idx');

            $table->index(["izin_id"], 'fk_peran_izin_izin1_idx');
            $table->nullableTimestamps();


            $table->foreign('peran_id', 'fk_peran_izin_peran1_idx')
                ->references('id')->on('peran')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('izin_id', 'fk_peran_izin_izin1_idx')
                ->references('id')->on('izin')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
