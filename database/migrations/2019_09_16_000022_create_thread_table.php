<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'thread';

    /**
     * Run the migrations.
     * @table thread
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('materi_id');
            $table->unsignedInteger('user_id');
            $table->string('judul')->nullable();
            $table->longText('keterangan')->nullable();
            $table->tinyInteger('status')->nullable();

            $table->index(["materi_id"], 'fk_thread_materi1_idx');
            $table->nullableTimestamps();

            $table->foreign('materi_id', 'fk_thread_materi1_idx')
                ->references('id')->on('materi')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->index(["user_id"], 'fk_thread_user1_idx');

            $table->foreign('user_id', 'fk_thread_user1_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
