<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodePraktikumTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'periode_praktikum';

    /**
     * Run the migrations.
     * @table periode_praktikum
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('praktikum_id');
            $table->string('tahun', 45)->nullable();
            $table->integer('status')->nullable()->default('0');

            $table->index(["praktikum_id"], 'fk_praktikum_has_periode_praktikum_praktikum1_idx');
            $table->nullableTimestamps();


            $table->foreign('praktikum_id', 'fk_praktikum_has_periode_praktikum_praktikum1_idx')
                ->references('id')->on('praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
