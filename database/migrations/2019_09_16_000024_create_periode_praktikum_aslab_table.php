<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodePraktikumAslabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'periode_praktikum_aslab';

    /**
     * Run the migrations.
     * @table periode_praktikum_aslab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('periode_praktikum_id');
            $table->unsignedInteger('aslab_id');
            $table->unsignedInteger('jenis_kelas_id');
            $table->integer('kuota')->nullable();

            $table->index(["aslab_id"], 'fk_periode_praktikum_has_aslab_aslab1_idx');

            $table->index(["periode_praktikum_id"], 'fk_periode_praktikum_has_aslab_periode_praktikum1_idx');

            $table->index(["jenis_kelas_id"], 'fk_periode_praktikum_aslab_jenis_kelas1_idx');
            $table->nullableTimestamps();


            $table->foreign('periode_praktikum_id', 'fk_periode_praktikum_has_aslab_periode_praktikum1_idx')
                ->references('id')->on('periode_praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('aslab_id', 'fk_periode_praktikum_has_aslab_aslab1_idx')
                ->references('id')->on('aslab')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('jenis_kelas_id', 'fk_periode_praktikum_aslab_jenis_kelas1_idx')
                ->references('id')->on('jenis_kelas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
