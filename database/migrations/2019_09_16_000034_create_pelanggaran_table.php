<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelanggaranTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'pelanggaran';

    /**
     * Run the migrations.
     * @table pelanggaran
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('penilaian_aslab_id');
            $table->unsignedInteger('modul_aslab_id');
            $table->unsignedInteger('jenis_pelanggaran_id');
            $table->string('deskripsi_pelanggaran', 200)->nullable();

            $table->index(["modul_aslab_id"], 'fk_pelanggaran_modul_aslab1_idx');

            $table->index(["jenis_pelanggaran_id"], 'fk_pelanggaran_jenis_pelanggaran1_idx');

            $table->index(["penilaian_aslab_id"], 'fk_pelanggaran_penilaian_aslab1_idx');
            $table->nullableTimestamps();


            $table->foreign('jenis_pelanggaran_id', 'fk_pelanggaran_jenis_pelanggaran1_idx')
                ->references('id')->on('jenis_pelanggaran')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('penilaian_aslab_id', 'fk_pelanggaran_penilaian_aslab1_idx')
                ->references('id')->on('penilaian_aslab')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('modul_aslab_id', 'fk_pelanggaran_modul_aslab1_idx')
                ->references('id')->on('modul_aslab')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
