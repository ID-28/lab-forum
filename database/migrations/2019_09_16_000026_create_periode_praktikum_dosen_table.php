<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodePraktikumDosenTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'periode_praktikum_dosen';

    /**
     * Run the migrations.
     * @table periode_praktikum_dosen
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('dosen_laboratorium_id');
            $table->unsignedInteger('periode_praktikum_id');
            $table->unsignedInteger('jenis_kelas_id');
            $table->integer('kuota')->nullable();
            $table->tinyInteger('status')->nullable()->default('0');

            $table->index(["dosen_laboratorium_id"], 'fk_periode_praktikum_dosen_dosen_laboratorium1_idx');

            $table->index(["jenis_kelas_id"], 'fk_periode_praktikum_dosen_jenis_kelas1_idx');

            $table->index(["periode_praktikum_id"], 'fk_periode_praktikum_has_dosen_periode_praktikum1_idx');
            $table->nullableTimestamps();


            $table->foreign('periode_praktikum_id', 'fk_periode_praktikum_has_dosen_periode_praktikum1_idx')
                ->references('id')->on('periode_praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('jenis_kelas_id', 'fk_periode_praktikum_dosen_jenis_kelas1_idx')
                ->references('id')->on('jenis_kelas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('dosen_laboratorium_id', 'fk_periode_praktikum_dosen_dosen_laboratorium1_idx')
                ->references('id')->on('dosen_laboratorium')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
