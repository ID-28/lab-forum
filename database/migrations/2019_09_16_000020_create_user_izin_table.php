<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIzinTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_izin';

    /**
     * Run the migrations.
     * @table user_izin
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('izin_id');

            $table->index(["user_id"], 'fk_user_izin_user1_idx');

            $table->index(["izin_id"], 'fk_user_izin_izin1_idx');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_user_izin_user1_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('izin_id', 'fk_user_izin_izin1_idx')
                ->references('id')->on('izin')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
