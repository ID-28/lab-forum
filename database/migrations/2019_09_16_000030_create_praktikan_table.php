<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraktikanTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'praktikan';

    /**
     * Run the migrations.
     * @table praktikan
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('periode_praktikum_id');
            $table->unsignedInteger('sesi_id');
            $table->unsignedInteger('mahasiswa_id');
            $table->unsignedInteger('asal_kelas_id');
            $table->date('tgl_registrasi')->nullable();
            $table->float('nilai_total_praktikum')->nullable();
            $table->tinyInteger('status')->nullable();

            $table->index(["sesi_id"], 'fk_praktikan_sesi1_idx');

            $table->index(["asal_kelas_id"], 'fk_praktikan_asal_kelas1_idx');

            $table->index(["mahasiswa_id"], 'fk_praktikan_mahasiswa1_idx');

            $table->index(["periode_praktikum_id"], 'fk_praktikan_periode_praktikum1_idx');
            $table->nullableTimestamps();


            $table->foreign('mahasiswa_id', 'fk_praktikan_mahasiswa1_idx')
                ->references('id')->on('mahasiswa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('sesi_id', 'fk_praktikan_sesi1_idx')
                ->references('id')->on('sesi')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('periode_praktikum_id', 'fk_praktikan_periode_praktikum1_idx')
                ->references('id')->on('periode_praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('asal_kelas_id', 'fk_praktikan_asal_kelas1_idx')
                ->references('id')->on('asal_kelas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
