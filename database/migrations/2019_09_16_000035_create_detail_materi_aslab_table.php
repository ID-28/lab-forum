<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMateriAslabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'detail_materi_aslab';

    /**
     * Run the migrations.
     * @table detail_materi_aslab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('penilaian_aslab_id');
            $table->unsignedInteger('modul_materi_aslab_id');
            $table->float('nilai_materi')->nullable();

            $table->index(["penilaian_aslab_id"], 'fk_detail_materi_aslab_penilaian_aslab1_idx');

            $table->index(["modul_materi_aslab_id"], 'fk_detail_materi_aslab_modul_materi_aslab1_idx');
            $table->nullableTimestamps();


            $table->foreign('penilaian_aslab_id', 'fk_detail_materi_aslab_penilaian_aslab1_idx')
                ->references('id')->on('penilaian_aslab')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('modul_materi_aslab_id', 'fk_detail_materi_aslab_modul_materi_aslab1_idx')
                ->references('id')->on('modul_materi_aslab')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
