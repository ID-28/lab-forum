<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'comment';

    /**
     * Run the migrations.
     * @table comment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('thread_id');
            $table->longText('comment')->nullable();
            $table->integer('comment_id')->nullable();
            $table->integer('comment_by')->nullable();


            $table->index(["thread_id"], 'fk_comment_thread1_idx');
            $table->nullableTimestamps();


            $table->foreign('thread_id', 'fk_comment_thread1_idx')
                ->references('id')->on('thread')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
