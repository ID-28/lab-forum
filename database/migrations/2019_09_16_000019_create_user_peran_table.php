<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPeranTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_peran';

    /**
     * Run the migrations.
     * @table user_peran
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('peran_id');

            $table->index(["peran_id"], 'fk_user_peran_peran1_idx');

            $table->index(["user_id"], 'fk_user_peran_user1_idx');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_user_peran_user1_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('peran_id', 'fk_user_peran_peran1_idx')
                ->references('id')->on('peran')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
