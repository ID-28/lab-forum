<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAslabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'aslab';

    /**
     * Run the migrations.
     * @table aslab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('laboratorium_id');
            $table->string('nama', 70)->nullable();
            $table->string('username', 45)->nullable();
            $table->string('password', 200)->nullable();
            $table->string('jenis_kelamin', 12)->nullable();
            $table->string('no_tlpn', 13)->nullable();

            $table->index(["laboratorium_id"], 'fk_aslab_laboratorium1_idx');

            $table->unique(["username"], 'username_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('laboratorium_id', 'fk_aslab_laboratorium1_idx')
                ->references('id')->on('laboratorium')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
