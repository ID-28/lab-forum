<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulAslabTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'modul_aslab';

    /**
     * Run the migrations.
     * @table modul_aslab
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('periode_praktikum_id');
            $table->string('nama', 45)->nullable();

            $table->index(["periode_praktikum_id"], 'fk_modul_aslab_periode_praktikum1_idx');
            $table->nullableTimestamps();


            $table->foreign('periode_praktikum_id', 'fk_modul_aslab_periode_praktikum1_idx')
                ->references('id')->on('periode_praktikum')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
