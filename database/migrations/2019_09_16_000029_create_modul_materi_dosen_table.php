<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulMateriDosenTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'modul_materi_dosen';

    /**
     * Run the migrations.
     * @table modul_materi_dosen
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('modul_dosen_id');
            $table->unsignedInteger('materi_id');

            $table->index(["modul_dosen_id"], 'fk_modul_materi_dosen_modul_dosen1_idx');

            $table->index(["materi_id"], 'fk_materi_aktif_materi1_idx');
            $table->nullableTimestamps();


            $table->foreign('materi_id', 'fk_materi_aktif_materi1_idx')
                ->references('id')->on('materi')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('modul_dosen_id', 'fk_modul_materi_dosen_modul_dosen1_idx')
                ->references('id')->on('modul_dosen')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
