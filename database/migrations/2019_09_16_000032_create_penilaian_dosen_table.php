<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianDosenTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'penilaian_dosen';

    /**
     * Run the migrations.
     * @table penilaian_dosen
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('periode_praktikum_dosen_id');
            $table->unsignedInteger('praktikan_id');
            $table->float('nilai_akhir')->nullable();

            $table->index(["periode_praktikum_dosen_id"], 'fk_penilaian_dosen_periode_praktikum_dosen1_idx');

            $table->index(["praktikan_id"], 'fk_penilaian_dosen_praktikan1_idx');
            $table->nullableTimestamps();


            $table->foreign('praktikan_id', 'fk_penilaian_dosen_praktikan1_idx')
                ->references('id')->on('praktikan')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('periode_praktikum_dosen_id', 'fk_penilaian_dosen_periode_praktikum_dosen1_idx')
                ->references('id')->on('periode_praktikum_dosen')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
