<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailSesiTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'detail_sesi';

    /**
     * Run the migrations.
     * @table detail_sesi
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('sesi_id');
            $table->date('tanggal')->nullable();
            $table->string('pertemuan', 45)->nullable();

            $table->index(["sesi_id"], 'fk_detail_sesi_sesi1_idx');
            $table->nullableTimestamps();


            $table->foreign('sesi_id', 'fk_detail_sesi_sesi1_idx')
                ->references('id')->on('sesi')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
