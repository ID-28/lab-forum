<?php

use Illuminate\Database\Seeder;
use App\Asal_kelas;


class asalKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Asal_kelas::create([
            'jenis_kelas_id' => 1,
            'nama' => 'Kelas A',
        ]);
        Asal_kelas::create([
            'jenis_kelas_id' => 1,
            'nama' => 'Kelas B',
        ]);
        Asal_kelas::create([
            'jenis_kelas_id' => 1,
            'nama' => 'Kelas H',
        ]);
        Asal_kelas::create([
            'jenis_kelas_id' => 2,
            'nama' => 'Kelas A1',
        ]);
        Asal_kelas::create([
            'jenis_kelas_id' => 2,
            'nama' => 'Kelas B1',
        ]);
        Asal_kelas::create([
            'jenis_kelas_id' => 2,
            'nama' => 'Kelas H1',
        ]);

    }
}
