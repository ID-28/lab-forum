<?php

use Illuminate\Database\Seeder;
use App\Izin;


class izinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Izin::create([
            'nama' => 'create'
        ]);
        Izin::create([
            'nama' => 'read'
        ]);
        Izin::create([
            'nama' => 'update'
        ]);
        Izin::create([
            'nama' => 'delete'
        ]);
        Izin::create([
            'nama' => 'managerpl'
        ]);
        Izin::create([
            'nama' => 'managejarkom'
        ]);
        Izin::create([
            'nama' => 'managebasprog'
        ]);
    }
}
