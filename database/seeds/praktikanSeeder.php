<?php

use Illuminate\Database\Seeder;
use App\Praktikan;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
class praktikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $curTime = new \DateTime();
        // $created_at = $curTime->format("Y-m-d H:i:s");

        // Praktikan::create([
            // 'periode_praktikum_id' => 1,
            // 'sesi_id' => 1,
            // 'mahasiswa_id' => 1,
            // 'password' => Hash::make('farid'),
            // 'tgl_registrasi' => $created_at,
            // 'status' => 1,
        // ]);
        // Praktikan::create([
        //     'periode_praktikum_id' => 1,
        //     'sesi_id' => 2,
        //     'mahasiswa_id' => 2,
        //     'password' => Hash::make('anandi'),
        //     'tgl_registrasi' => $created_at,
        //     'status' => 1,
        // ]);
        // Praktikan::create([
        //     'periode_praktikum_id' => 1,
        //     'sesi_id' => 1,
        //     'mahasiswa_id' => 3,
        //     'password' => Hash::make('futaikhi'),
        //     'tgl_registrasi' => $created_at,
        //     'status' => 1,
        // ]);
        // Praktikan::create([
        //     'periode_praktikum_id' => 1,
        //     'sesi_id' => 1,
        //     'mahasiswa_id' => 4,
        //     'password' => Hash::make('ari'),
        //     'tgl_registrasi' => $created_at,
        //     'status' => 1,
        // ]);
        // Praktikan::create([
        //     'periode_praktikum_id' => 1,
        //     'sesi_id' => 1,
        //     'mahasiswa_id' => 5,
        //     'password' => Hash::make('fajri'),
        //     'tgl_registrasi' => $created_at,
        //     'status' => 1,
        // ]);

        // $faker = Faker::create();

        // for($i = 1; $i <= 120; $i++){
        //     $periode = 0;
        //     $sesi = 0;
        //     $asal_kelas = 0;
        //     if($i>100){
        //         $periode = 6;
        //         $sesi = 5;
        //         $asal_kelas = 6;
        //     }else if($i<100 && $i>=80){
        //         $periode = 5;
        //         $sesi = 5;
        //         $asal_kelas = 5;
        //     }else if($i<80 && $i>=60){
        //         $periode = 4;
        //         $sesi = 4;
        //         $asal_kelas = 4;
        //     }else if($i<60 && $i>=40){
        //         $periode = 3;
        //         $sesi = 3;
        //         $asal_kelas = 3;
        //     }else if($i<40 && $i>=20){
        //         $periode = 2;
        //         $sesi = 2;
        //         $asal_kelas = 2;
        //     }else{
        //         $periode = 1;
        //         $sesi = 1;
        //         $asal_kelas = 1;
        //     }
        //     DB::table('praktikan')->insert([
        //         'sesi_id' => $sesi,
        //         'asal_kelas_id' => $asal_kelas,
        //         'mahasiswa_id' => $i,
        //         'tgl_registrasi' => $created_at,
        //         'status' => 0,
        //     ]);
        // }

        // for($i=0; $i<100; $i++){
        //     App\Praktikan::create([
        //         'periode_praktikum_id' => rand(1,6),
        //         'sesi_id' => rand
        //     ]);
        // }

        for($i=1;$i<=120;$i++){
            \App\Praktikan::create([
                'periode_praktikum_id' => rand(1,6),
                'sesi_id' => rand(1,6),
                'asal_kelas_id' => rand(1,6),
                'mahasiswa_id' => $i,
                'status' => 0,
            ]);
        }
    }
}
