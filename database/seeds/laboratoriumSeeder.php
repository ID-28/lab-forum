<?php

use Illuminate\Database\Seeder;
use App\Laboratorium;

class laboratoriumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Laboratorium::create([
            'id' => 1,
            'nama' => 'Rekayasa Perangkat Lunak',
            'ruangan' => 'H1-405',
        ]);
        
        Laboratorium::create([
            'id' => 2,
            'nama' => 'Jaringan Komputer',
            'ruangan' => 'H1-404',
        ]);
        
        Laboratorium::create([
            'id' => 3,
            'nama' => 'Bahasa Pemrograman',
            'ruangan' => 'H1-104',
        ]);

    }
}
