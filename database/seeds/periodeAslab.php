<?php

use Illuminate\Database\Seeder;

class periodeAslab extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 100; $i++) {
            App\Periode_praktikum_aslab::create([
                'periode_praktikum_id' => rand(1,6),
                'aslab_id' => $i,
                'jenis_kelas_id' => rand(1,3),
                'kuota' => rand(15,30),
            ]);
        }
    }
}
