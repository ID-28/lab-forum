<?php

use Illuminate\Database\Seeder;
use App\Praktikum;


class praktikumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Praktikum::create([
            'id' => 1,
            'laboratorium_id' => 1,
            'nama' => 'Basis Data',
        ]);
        
        Praktikum::create([
            'id' => 2,
            'laboratorium_id' => 2,
            'nama' => 'Jaringan Komputer',
        ]);
        
        Praktikum::create([
            'id' => 3,
            'laboratorium_id' => 3,
            'nama' => 'Struktur Data',
        ]);
        
        Praktikum::create([
            'id' => 4,
            'laboratorium_id' => 1,
            'nama' => 'Pemrograman Berorientasi Objek',
        ]);
        
        Praktikum::create([
            'id' => 5,
            'laboratorium_id' => 2,
            'nama' => 'Sistem Operasi',
        ]);
        
        Praktikum::create([
            'id' => 6,
            'laboratorium_id' => 3,
            'nama' => 'Pemrograman Terstruktur',
        ]);
    }
}
