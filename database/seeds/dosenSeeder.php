<?php

use Illuminate\Database\Seeder;
use App\Dosen;
use Illuminate\Support\Facades\Hash;

class dosenSeeder extends Seeder
{
    public function run()
    {
        Dosen::create([
            'nip' => '153045',
            'nama' => 'Muhammad Kurniawan, S.Kom., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '083862118631',
        ]);
        
        Dosen::create([
            'nip' => '122093',
            'nama' => 'Nanang Fakhrur Rozi, S.ST., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085103886639',
        ]);
        Dosen::create([
            'nip' => '112062',
            'nama' => 'Farida, S.Kom., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085646013899',
        ]);
        Dosen::create([
            'nip' => '153101',
            'nama' => 'Weny Mistarika Rahmawati, S.Kom., M.Kom., M.Sc.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '081335250560',
        ]);
        Dosen::create([
            'nip' => '153076',
            'nama' => 'Dian Puspita Hapsari, S.Kom., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '08564553446',
        ]);
        Dosen::create([
            'nip' => '153070',
            'nama' => 'Danang Haryo Sulaksono, S.ST., M.T.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085646361267',
        ]);
        Dosen::create([
            'nip' => '153047',
            'nama' => 'Hendro Nugroho, S.T., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '081515482010',
        ]);
        Dosen::create([
            'nip' => '173132',
            'nama' => 'Septiyawan Rosetya Wardhana, S.Kom., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085851337314',
        ]);
        Dosen::create([
            'nip' => '122094',
            'nama' => 'Rachman Arief, S.Kom., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '0000000',
        ]);
        Dosen::create([
            'nip' => '173133',
            'nama' => 'Rahmi Rizkiana Putri, S.ST., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085231261252',
        ]);

        Dosen::create([
            'nip' => '001117',
            'nama' => 'Tutuk Indriyani, S.T., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085231971485',
        ]);
        Dosen::create([
            'nip' => '133011',
            'nama' => 'Rinci Kembang Hapsari, S.Si., M.Kom.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085231261252',
        ]);
        Dosen::create([
            'nip' => '153052',
            'nama' => 'Rani Rotul Muhima, S.Si., M.T.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085733336701',
        ]);
        Dosen::create([
            'nip' => '153073',
            'nama' => 'Tukadi, S.T., M.T.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '0000000',
        ]);
        Dosen::create([
            'nip' => '153082',
            'nama' => 'Maftahatul Hakimah, S.Si., M.Si.',
            'password' => Hash::make('praktikum2019'),
            'no_tlpn' => '085257536020',
        ]);
    }
}

