<?php

use Illuminate\Database\Seeder;
use App\User_izin;


class userIzinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User_izin::create([
            'user_id' => 2,
            'izin_id' => 5,
        ]);
        User_izin::create([
            'user_id' => 3,
            'izin_id' => 6,
        ]);
        User_izin::create([
            'user_id' => 4,
            'izin_id' => 7,
        ]);
    }
}

