<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class aslabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $id_lab = 1;
        for($i = 0; $i < 100; $i++) {

            App\Aslab::create([
                'laboratorium_id' => $id_lab,
                'nama' => $faker->name,
                'username' => '06.2017.1.' . $i,
                'password' => Hash::make("password"),
                'jenis_kelamin' => array('L','K')[rand(0,1)],
                'no_tlpn' => "6285732660403",
            ]);

            if($id_lab == 3){
                $id_lab = 0;
            }

            $id_lab++;
        }
    }
}
