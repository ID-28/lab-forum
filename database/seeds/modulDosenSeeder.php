<?php

use Illuminate\Database\Seeder;
use App\Modul_dosen;

class modulDosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // BASDAT
        Modul_dosen::create([
            'periode_praktikum_id' => 1,
            'nama' => 'Modul 1',
        ]);
        Modul_dosen::create([
            'periode_praktikum_id' => 1,
            'nama' => 'Modul 2',
        ]);
        Modul_dosen::create([
            'periode_praktikum_id' => 1,
            'nama' => 'Modul 3',
        ]);

        // JARKOM
        Modul_dosen::create([
            'periode_praktikum_id' => 2,
            'nama' => 'Modul 1',
        ]);
        Modul_dosen::create([
            'periode_praktikum_id' => 2,
            'nama' => 'Modul 2',
        ]);
        Modul_dosen::create([
            'periode_praktikum_id' => 2,
            'nama' => 'Modul 3',
        ]);

        // STRUKDAT
        Modul_dosen::create([
            'periode_praktikum_id' => 3,
            'nama' => 'Modul 1',
        ]);
        Modul_dosen::create([
            'periode_praktikum_id' => 3,
            'nama' => 'Modul 2',
        ]);
        Modul_dosen::create([
            'periode_praktikum_id' => 3,
            'nama' => 'Modul 3',
        ]);
     
        
    }
}
