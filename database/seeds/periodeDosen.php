<?php

use Illuminate\Database\Seeder;

class periodeDosen extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($praktikum = 1; $praktikum <= 6; $praktikum++) {
            for($i = 1; $i <= 10; $i++) {
                App\Periode_praktikum_dosen::create([
                    'periode_praktikum_id' => $praktikum,
                    'dosen_laboratorium_id' => $i,
                    'jenis_kelas_id' => rand(1,3),
                    'kuota' => rand(15,30),
                ]);
            }
        }
    }
}
