<?php

use Illuminate\Database\Seeder;
use App\Mahasiswa;
use Faker\Factory as Faker;


class mahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mahasiswa::create([
        //     'npm' => '06.2017.1.06852',
        //     'nama' => 'Khisby Al Ghofari',
        //     'no_tlpn' => '085742660403',
        //     'foto' => '06-2017-1-06852.png'
        // ]);

        // Mahasiswa::create([
        //     'jenis_kelas_id' => 1,
        //     'npm' => '06634',
        //     'nama' => 'Farid Usnadi',
        //     'no_tlpn' => '0891234546',
        //     'foto' => 'fulan.jpg'
        // ]);
        // Mahasiswa::create([
        //     'jenis_kelas_id' => 2,
        //     'npm' => '06681',
        //     'nama' => 'Anandi Ferdiansyah',
        //     'no_tlpn' => '083819182',
        //     'foto' => 'someone.jpg'
        // ]);
        // Mahasiswa::create([
        //     'jenis_kelas_id' => 1,
        //     'npm' => '06609',
        //     'nama' => 'Futaikhi',
        //     'no_tlpn' => '083819182',
        //     'foto' => 'lol.jpg'
        // ]);
        // Mahasiswa::create([
        //     'jenis_kelas_id' => 2,
        //     'npm' => '06625',
        //     'nama' => 'ari',
        //     'no_tlpn' => '0838191212',
        //     'foto' => 'img.jpg'
        // ]);
        // Mahasiswa::create([
        //     'jenis_kelas_id' => 1,
        //     'npm' => '06698',
        //     'nama' => 'fajri',
        //     'no_tlpn' => '083811234',
        //     'foto' => 'foto.jpg'
        // ]);

        $faker = Faker::create();

        for($i = 1; $i <= 120; $i++){ 
            DB::table('mahasiswa')->insert([                
                'npm' => $faker->unique()->numberBetween(6600,9900),
                'nama' => $faker->name,
                'no_tlpn' => '08'.$faker->unique()->numberBetween(6600,9900).'65'.$faker->unique()->numberBetween(6600,9900),
                'foto' => $faker->text($maxNbChars = 5).'jpg',
            ]);
        }
            
       

    }
}
