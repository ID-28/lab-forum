<?php

use Illuminate\Database\Seeder;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Mahasiswa;
use App\Praktikan;
use App\User_peran;
use Faker\Factory as Faker;

class fakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=1; $i <=8 ; $i++) { 
            $mahasiswa = Mahasiswa::create([                
                'npm' => '06.2017.1.'.$i.$i.$i.$i.$i,
                'nama' => $faker->name,
                'no_tlpn' => '08'.$faker->unique()->numberBetween(6600,9900).'65'.$faker->unique()->numberBetween(6600,9900),
                'foto' => $faker->text($maxNbChars = 5).'jpg',
            ]);

            $user = User::create([
                'jenis_user_id' => 5,
                'username' => '06.2017.1.'.$i.$i.$i.$i.$i,
                'password' => Hash::make('praktikan'),
            ]);

            $peran = User_peran::create([
                'user_id' => $user->id,
                'peran_id' =>  5
            ]);

            $praktikan = Praktikan::create([
                'periode_praktikum_id' => 1,
                'sesi_id' => 1,
                'mahasiswa_id' => $mahasiswa->id,
                'asal_kelas_id' => 1,
            ]);
                
        }
    }
}
