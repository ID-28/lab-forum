<?php

use Illuminate\Database\Seeder;
use App\Jenis_kelas;


class jenisKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jenis_kelas::create([
            'id' => 1,
            'nama' => 'pagi'
        ]);

        Jenis_kelas::create([
            'id' => 2,
            'nama' => 'malam'
        ]);

        Jenis_kelas::create([
            'id' => 3,
            'nama' => 'bebas'
        ]);
    }
}
