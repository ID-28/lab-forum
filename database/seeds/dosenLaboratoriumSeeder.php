<?php

use Illuminate\Database\Seeder;
use App\Dosen_laboratorium;
use Illuminate\Support\Facades\Hash;

class dosenLaboratoriumSeeder extends Seeder
{
    public function run()
    {
        // RPL
        for ($i=1; $i <=15; $i++) { 
            Dosen_laboratorium::create([
                'dosen_id' => $i,
                'laboratorium_id' => '1',
            ]);
        }
        // JARKOM
        for ($i=1; $i <=15; $i++) { 
            Dosen_laboratorium::create([
                'dosen_id' => $i,
                'laboratorium_id' => '2',
            ]);
        }
        // BASPROG
        for ($i=1; $i <=15; $i++) { 
            Dosen_laboratorium::create([
                'dosen_id' => $i,
                'laboratorium_id' => '3',
            ]);
        }
    
    }
}

