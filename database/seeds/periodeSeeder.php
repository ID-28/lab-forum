<?php

use Illuminate\Database\Seeder;
use App\Periode_praktikum;

class periodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Periode_praktikum::create([
            'praktikum_id'    => '1',
            'tahun'    => '2019',
            'status' => 1,
        ]);
        Periode_praktikum::create([
            'praktikum_id'    => '2',
            'tahun'    => '2019',
            'status' => 1,
        ]);
        Periode_praktikum::create([
            'praktikum_id'    => '3',
            'tahun'    => '2019',
            'status' => 1,
        ]);
        Periode_praktikum::create([
            'praktikum_id'    => '4',
            'tahun'    => '2019',
            'status' => 0,
        ]);
        Periode_praktikum::create([
            'praktikum_id'    => '5',
            'tahun'    => '2019',
            'status' => 0,
        ]);
        Periode_praktikum::create([
            'praktikum_id'    => '6',
            'tahun'    => '2019',
            'status' => 0,
        ]);
    }
}
