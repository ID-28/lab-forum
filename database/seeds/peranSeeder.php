<?php

use Illuminate\Database\Seeder;
use App\Peran;

class peranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Peran::create([
            'nama' => 'super admin',
        ]);
        Peran::create([
            'nama' => 'admin',
        ]);
        Peran::create([
            'nama' => 'dosen',
        ]);
        Peran::create([
            'nama' => 'aslab',
        ]);
        Peran::create([
            'nama' => 'praktikan',
        ]);
    }
}
