<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Mahasiswa;
use App\User_peran;
use Faker\Factory as Faker;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        User::create([
            'jenis_user_id' => 1,
            'username' => 'superman',
            'password' => Hash::make('superman'),
        ]);
        User::create([
            'jenis_user_id' => 2,
            'username' => 'adminrpl',
            'password' => Hash::make('adminrpl'),
        ]);
        User::create([
            'jenis_user_id' => 2,
            'username' => 'adminjarkom',
            'password' => Hash::make('adminjarkom'),
        ]);
        User::create([
            'jenis_user_id' => 2,
            'username' => 'adminbasprog',
            'password' => Hash::make('adminbasprog'),
        ]);

        // Dosen
         User::create([
             'jenis_user_id' => 3,
             'username' => '153045',
             'password' => Hash::make('praktikum2019'),
         ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '122093',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '112062',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153101',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153076',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153070',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153047',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '173132',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '122094',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '173133',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '001117',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '133011',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153052',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153073',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '153082',
        //     'password' => Hash::make('praktikum2019'),
        // ]);
        // User::create([
        //     'jenis_user_id' => 3,
        //     'username' => '193165',
        //     'password' => Hash::make('praktikum2019'),
        // ]);

        // praktikan
        // User::create([
        //     'jenis_user_id' => 5,
        //     'username' => '06.2017.1.06852',
        //     'password'  => Hash::make('khisby'),
        //     ]);


    }
}
