<?php

use Illuminate\Database\Seeder;
use App\Sesi;
class sesiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($praktikum=1;$praktikum<=6;$praktikum++) {
            $sesinya = array("Jum'at, 08.00 - 09.00","Jum'at, 09.00 - 10.00","Jum'at, 13.00 - 14.00","Jum'at, 14.00 - 15.00","Jum'at, 18.00 - 17.00","Selasa, 20.00 - 21.00");

            $i = 1;
            foreach($sesinya as $s){
                $sesi = new \App\Sesi();
                $sesi->nama = 'Sesi '.$i;
                $sesi->periode_praktikum_id = $praktikum;
                $sesi->waktu = $s;
                $sesi->kuota = rand(15, 35);
                $sesi->save();
                $i++;
            }

            // for ($i=1;$i<=6;$i++) {
            //     // $sesinya = array("Jum'at, 08.00 - 09.00","Jum'at, 09.00 - 10.00","Jum'at, 13.00 - 14.00","Jum'at, 14.00 - 15.00","Jum'at, 18.00 - 17.00","Selasa, 20.00 - 21.00");
            //     $randomIndex = rand(0, count($sesinya)-1);
                
            //     $sesi = new \App\Sesi();
            //     $sesi->nama = 'Sesi '.$i;
            //     $sesi->periode_praktikum_id = $praktikum;
            //     $sesi->waktu = $sesinya[$randomIndex];
            //     $sesi->kuota = rand(15, 35);
            //     $sesi->save();

            //     unset($sesinya[$randomIndex]);
            // }
        }
    }
}
