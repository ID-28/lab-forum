<?php

use Illuminate\Database\Seeder;
use App\Peran_izin;

class peranIzinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Peran_izin::create([
            'peran_id' => 1,
            'izin_id' => 1,
        ]);
        Peran_izin::create([
            'peran_id' => 1,
            'izin_id' => 2,
        ]);
        Peran_izin::create([
            'peran_id' => 1,
            'izin_id' => 3,
        ]);
        Peran_izin::create([
            'peran_id' => 1,
            'izin_id' => 4,
        ]);
        Peran_izin::create([
            'peran_id' => 2,
            'izin_id' => 1,
        ]);
        Peran_izin::create([
            'peran_id' => 2,
            'izin_id' => 2,
        ]);
        Peran_izin::create([
            'peran_id' => 2,
            'izin_id' => 3,
        ]);
        Peran_izin::create([
            'peran_id' => 2,
            'izin_id' => 4,
        ]);
    }
}
