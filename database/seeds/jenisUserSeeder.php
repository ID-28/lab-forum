<?php

use Illuminate\Database\Seeder;
use App\Jenis_user;

class jenisUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jenis_user::create([
            'nama' => 'super admin'
        ]);
        Jenis_user::create([
            'nama' => 'admin'
        ]);
        Jenis_user::create([
            'nama' => 'dosen'
        ]);
        Jenis_user::create([
            'nama' => 'aslab'
        ]);
        Jenis_user::create([
            'nama' => 'praktikan'
        ]);
    }
}
