<?php

use Illuminate\Database\Seeder;
use App\Materi;

class materiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Materi::create([
            'id' => 1,
            'praktikum_id' => 1,
            'nama' => 'Generate CDM ke PDM secara manual',
        ]);
        
        Materi::create([
            'id' => 2,
            'praktikum_id' => 1,
            'nama' => 'Pembuatan table dan perubahasn struktur table',
        ]);
        
        Materi::create([
            'id' => 3,
            'praktikum_id' => 1,
            'nama' => 'Insert, update dan delete data',
        ]);
        
        Materi::create([
            'id' => 4,
            'praktikum_id' => 1,
            'nama' => 'klausa, concate, alias, group by dan order by',
        ]);
        
        Materi::create([
            'id' => 5,
            'praktikum_id' => 1,
            'nama' => 'implementasi  fungsi aggregate ',
        ]);
        
        Materi::create([
            'id' => 6,
            'praktikum_id' => 1,
            'nama' => 'implementasi subquery ',
        ]);
        
        Materi::create([
            'id' => 7,
            'praktikum_id' => 1,
            'nama' => 'implementasi joint table ',
        ]);
        
        Materi::create([
            'id' => 8,
            'praktikum_id' => 1,
            'nama' => 'implementasi view ',
        ]);
        
        Materi::create([
            'id' => 9,
            'praktikum_id' => 1,
            'nama' => 'konsep PL /SQL pada function dan procedure ',
        ]);
        
        Materi::create([
            'id' => 10,
            'praktikum_id' => 1,
            'nama' => 'implementasi trigger ',
        ]);
        
        Materi::create([
            'id' => 11,
            'praktikum_id' => 1,
            'nama' => 'implementasi konsep pembuatan user serta hak ',
        ]);
        
        Materi::create([
            'id' => 12,
            'praktikum_id' => 2,
            'nama' => 'seting ip statis dan dhcp ',
        ]);
        
        Materi::create([
            'id' => 13,
            'praktikum_id' => 2,
            'nama' => 'bisa mengkonfigurasi webserver nginx, dan apa ',
        ]);
        
        Materi::create([
            'id' => 14,
            'praktikum_id' => 2,
            'nama' => 'seting cisco packet tracer ',
        ]);
        
        Materi::create([
            'id' => 15,
            'praktikum_id' => 3,
            'nama' => 'memahami stak ',
        ]);
        
        Materi::create([
            'id' => 16,
            'praktikum_id' => 3,
            'nama' => 'memahami queue ',
        ]);
        
        Materi::create([
            'id' => 17,
            'praktikum_id' => 3,
            'nama' => 'memahami linked list ',
        ]);
    }
}
