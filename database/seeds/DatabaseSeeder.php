<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(jenisKelasSeeder::class);
        $this->call(asalKelasSeeder::class);
        $this->call(laboratoriumSeeder::class);
         $this->call(dosenSeeder::class);
         $this->call(dosenLaboratoriumSeeder::class);
        $this->call(praktikumSeeder::class);
        $this->call(periodeSeeder::class);
         $this->call(modulDosenSeeder::class);
         $this->call(materiSeeder::class);
         $this->call(sesiSeeder::class);
//         $this->call(mahasiswaSeeder::class);
//         $this->call(praktikanSeeder::class);
        $this->call(jenisUserSeeder::class);
        $this->call(userSeeder::class);
        $this->call(peranSeeder::class);
        $this->call(izinSeeder::class);
        $this->call(userPeranSeeder::class);
        $this->call(userIzinSeeder::class);
        $this->call(peranIzinSeeder::class);
         $this->call(aslabSeeder::class);
         $this->call(periodeAslab::class);
         $this->call(periodeDosen::class);
         $this->call(fakeUserSeeder::class);
    }
}
