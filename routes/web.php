<?php

Route::post('/login-admin','LoginController@loginAdmin')->name('login_admin');
Route::post('/login-dosen','LoginController@loginDosen')->name('login_dosen');
Route::post('/login-praktikan','LoginController@loginPraktikan')->name('login_praktikan');
Route::post('/login-aslab','LoginController@loginAslab')->name('login_aslab');
Route::post('/register-praktikan','LoginController@registerPraktikan')->name('register_praktikan');
Route::get('/logout','LoginController@logout')->name('logout');

Route::get('/', 'PraktikanController@beranda')->name('home');
Route::get('/informasi','PraktikanController@berita')->name('berita');
Route::get('/informasi/detail/{berita}','PraktikanController@beritaDetail')->name('beritaDetail');

Route::group([
    'middleware' => [
        'guest',
    ],
], function() {

    Route::get('/ghost', function () {
        return view('pages.login_admin');
    })->name('login.admin.view');
    Route::get('/login-dosen', function(){
        return view('pages.login_dosen');
    })->name('login.dosen.view');
    Route::get('/login-aslab', function(){
    return view('pages.login_aslab');
    })->name('login.aslab.view');
    Route::get('/login-praktikan', function(){
        return view('pages.login_praktikan');
    })->name('login.praktikan.view');
    Route::get('/register-praktikan', function(){
        return view('pages.register_praktikan');
    })->name('register.praktikan.view');

});


// Superman
Route::group([
    'prefix'     => 'super',
    'middleware' => [
        'auth',
        'peran:super admin',
    ],
], function() {
    Route::resource('/','SuperController');
});


// Admin
Route::group([
    'prefix' => 'admin',
    'middleware' => [
        'auth',
        'peran:admin',
    ],
], function(){
    Route::get('/' ,'AdminController@index')->name('admin');

    // Dosen
    Route::get('management-dosens' ,'AdminController@management_dosens')->name('management-dosens');
    Route::post('management-dosens/create/store' ,'AdminController@dosen_store')->name('dosen_store');
    Route::put('management-dosens/update/{id}' ,'AdminController@dosen_update')->name('dosen_update');
    Route::delete('management-dosens/delete/{id}' ,'AdminController@dosen_delete')->name('dosen_delete');

    // Modul Dosen
    Route::get('management-modul-dosen' ,'AdminController@management_modul_dosen')->name('management_modul_dosen');
    Route::post('management-modul-dosen/create/store' ,'AdminController@modul_store_dosen')->name('modul_store_dosen');
    Route::put('management-modul-dosen/update/{id}' ,'AdminController@modul_update_dosen')->name('modul_update_dosen');
    Route::delete('management-modul-dosen/delete/{id}' ,'AdminController@modul_delete_dosen')->name('modul_delete_dosen');

    // Modul Aslab
    Route::get('management-modul-aslab' ,'AdminController@management_modul_aslab')->name('management_modul_aslab');
    Route::post('management-modul-aslab/create/store' ,'AdminController@modul_store_aslab')->name('modul_store_aslab');
    Route::put('management-modul-aslab/update/{id}' ,'AdminController@modul_update_aslab')->name('modul_update_aslab');
    Route::delete('management-modul-aslab/delete/{id}' ,'AdminController@modul_delete_aslab')->name('modul_delete_aslab');

    // Materi
    Route::get('management-materis' ,'AdminController@management_materis')->name('management-materis');
    Route::post('management-materis/create/store' ,'AdminController@materi_store')->name('materi_store');
    Route::put('management-materis/update/{id}' ,'AdminController@materi_update')->name('materi_update');
    Route::delete('management-materis/delete/{id}' ,'AdminController@materi_delete')->name('materi_delete');

    // Penilaian Dosen
    Route::get('management-penilaian-dosen' ,'AdminController@management_penilaian_dosen')->name('management-penilaian-dosen');
    Route::post('management-penilaian-dosen/create/store' ,'AdminController@penilaian_dosen_store')->name('penilaian_dosen_store');
    Route::delete('management-penilaian-dosen/delete/{id}' ,'AdminController@penilaian_dosen_delete')->name('penilaian_dosen_delete');

    // Penilaian Aslab
    Route::get('management-penilaian-aslab' ,'AdminController@management_penilaian_aslab')->name('management-penilaian-aslab');
    Route::post('management-penilaian-aslab/create/store' ,'AdminController@penilaian_aslab_store')->name('penilaian_aslab_store');
    Route::delete('management-penilaian-aslab/delete/{id}' ,'AdminController@penilaian_aslab_delete')->name('penilaian_aslab_delete');

    // Verifikasi praktikan
    Route::get('verifikasiPraktikan' ,'AdminController@verifikasiPraktikan')->name('verifikasiPraktikan');
    Route::get('verifikasiPraktikanDetail/{id}' ,'AdminController@verifikasiPraktikanDetail')->name('verifikasiPraktikanDetail');
    Route::get('verifikasiPraktikanDetailProses/{praktikan}' ,'AdminController@verifikasiPraktikanDetailProses')->name('verifikasiPraktikanDetailProses');
    Route::get('deleteRegistrasi/{praktikan}' ,'AdminController@deleteRegistrasiPraktikan')->name('deleteRegistrasiPraktikan');

    //Aktifasi Dosen
    Route::get('periodeDosen', 'AdminController@periodeDosenView')->name('periodeDosenView');
    Route::post('periodeDosen', 'AdminController@periodeDosenProses')->name('periodeDosenProses');
    Route::delete('periodeDosen/delete/{id}', 'AdminController@periodeDosenDelete')->name('periodeDosenDelete');
    Route::put('periodeDosen/update/{id}', 'AdminController@periodeDosenUpdate')->name('periodeDosenUpdate');

    // Kartu Praktikum
    Route::get('cetakKartuPraktikum' ,'AdminController@cetakKartuPraktikum')->name('cetakKartuPraktikum');
    Route::get('cetakKartuPraktikumBelakang' ,'AdminController@cetakKartuPraktikumBelakang')->name('cetakKartuPraktikumBelakang');

    //absensi
    Route::get('cetakAbsensi' ,'AdminController@cetakAbsensi')->name('cetakAbsensi');

    //Praktikan
    Route::get('praktikan' ,'AdminController@praktikanView')->name('praktikanview');
    Route::get('praktikanDetail/{id}' ,'AdminController@praktikanViewDetail')->name('praktikanViewDetail');
    Route::get('praktikanResetPassword/{user}' ,'AdminController@praktikanResetPassword')->name('praktikanResetPassword');
    Route::get('praktikanDelete/{id}' ,'AdminController@praktikanDelete')->name('praktikanDelete');


    // Change Password
    Route::put('change-password','AdminController@change_password')->name('change_password');

    //Aslab
    Route::get('manajemenAslab', 'AdminController@manajemenAslabView')->name('manajemenAslab');
    Route::post('manajemenAslab', 'AdminController@manajemenAslabProses')->name('manajemenAslabProses');
    Route::delete('manajemenAslab', 'AdminController@manajemenAslabDelete')->name('manajemenAslabDelete');
    Route::put('manajemenAslab', 'AdminController@manajemenAslabUpdate')->name('manajemenAslabUpdate');

    //Manajemen Periode Praktikum
    Route::get('periodePraktikum', 'AdminController@periodePraktikumView')->name('periodePraktikum');
    Route::post('periodePraktikum', 'AdminController@periodePraktikumProses')->name('periodePraktikumProses');
    Route::get('aktifkanPeriodePraktikum/{periode}', 'AdminController@aktifkanPeriodePraktikum')->name('aktifkanPeriodePraktikum');

    //Manajemen sesi praktikum
    Route::get('sesiPraktikum', 'AdminController@sesiPraktikumView')->name('sesiPraktikumView');
    Route::post('sesiPraktikum', 'AdminController@sesiPraktikumProses')->name('sesiPraktikumProses');
    Route::delete('sesiPraktikum', 'AdminController@sesiPraktikumProsesDelete')->name('sesiPraktikumDelete');
    Route::put('sesiPraktikum', 'AdminController@sesiPraktikumProsesUpdate')->name('sesiPraktikumUpdate');

    //Aktifasi Aslab
    Route::get('periodeAslab', 'AdminController@periodeAslabView')->name('periodeAslabView');
    Route::post('periodeAslab', 'AdminController@periodeAslabProses')->name('periodeAslabProses');
    Route::delete('periodeAslab', 'AdminController@periodeAslabDelete')->name('periodeAslabDelete');
    Route::put('periodeAslab', 'AdminController@periodeAslabUpdate')->name('periodeAslabUpdate');

    //Berita
    Route::get('Berita', 'AdminController@manajemenBerita')->name('manajemenBerita');
    Route::get('BeritaView/{berita}', 'AdminController@manajemenBeritaView')->name('manajemenBeritaView');
    Route::get('BeritaViewEdit/{berita}', 'AdminController@manajemenBeritaViewEdit')->name('manajemenBeritaViewEdit');
    Route::post('BeritaViewEdit', 'AdminController@manajemenBeritaViewEditProses')->name('manajemenBeritaViewEditProses');
    Route::get('BeritaViewDelete/{id}', 'AdminController@manajemenBeritaViewDelete')->name('manajemenBeritaViewDelete');
    Route::post('BeritaTambahProses', 'AdminController@manajemenBeritaTambahProses')->name('manajemenBeritaTambahProses');

    // Lihat Nilai Praktikan
    Route::get('lihatNilai' ,'AdminController@lihatNilai')->name('lihatNilai');
    Route::get('lihatNilai/export/{nip}/{idPraktikum}' ,'AdminController@excel')->name('export_excel');
    Route::put('lihatNilai/reject/{id}' ,'AdminController@reject')->name('reject');

    Route::get('/thread/list/all', 'AdminController@listThread')->name('thread_list');
    Route::get('/thread/detail/{thread}', 'AdminController@detailThread')->name('thread_detail');

});


// dosen
Route::group([
    'prefix'     => 'dosen',
    'middleware' => [
        'auth',
        'peran:dosen',
    ],
], function() {
    Route::get('/' ,'DosenController@index')->name('dosen');
    Route::get('entry-nilai' ,'DosenController@entry_nilai')->name('entry_nilai');
    Route::get('entry-nilai/create' ,'DosenController@entry_nilai_create')->name('entry_nilai_create');
    Route::post('entry-nilai/store' ,'DosenController@entry_nilai_store')->name('entry_nilai_store');
    Route::get('list-praktikan' ,'DosenController@list_praktikan')->name('list_praktikan');
    Route::get('list-praktikan/view' ,'DosenController@list_praktikan_view')->name('list_praktikan_view');
    Route::put('change-password','DosenController@change_password')->name('change_password');

    Route::put('sendNilai/{idLab}' ,'DosenController@kirimNilai')->name('kirimNilai');
    // PDF
    // Route::get('get-pdf/{idPraktikum}' ,'DosenController@get_pdf')->name('get_pdf');

    // //Excel
    // Route::get('export/{idPraktikum}','DosenController@sendEmailExcel')->name('export_excel');
    Route::get('/thread', 'DosenController@daftarPraktikum')->name('thread_awal');
    Route::get('/thread/{praktikum}', 'DosenController@listModul')->name('thread_kedua');
    Route::get('/thread/modul/{modul}', 'DosenController@listMateri')->name('thread_ketiga');
});


// // aslab
Route::group([
    'prefix'     => 'aslab',
    'middleware' => [
        // 'auth',
        // 'peran:aslab',
    ],
], function() {
    Route::get('/' ,'AslabController@index')->name('aslab');
    Route::get('entry-nilai' ,'AslabController@entryNilai')->name('entryNilaiAslab');
    Route::post('entry-nilai/praktikan' ,'AslabController@entryNilaiPraktikan')->name('entryNilaiPraktikanAslab');
    Route::get('list-praktikan' ,'AslabController@listPraktikan')->name('listPraktikanAslab');
    Route::get('list-praktikan-view/{id}' ,'AslabController@listPraktikanView')->name('listPraktikanAslabView');
    Route::put('change-password','AslabController@change_password')->name('change_password');
    Route::get('cetakListPraktikanAslab' ,'AslabController@cetakListPraktikanAslab')->name('cetakListPraktikanAslab');
    Route::get('/thread', 'AslabController@listPraktikum')->name('praktikum_thread');
    Route::get('/thread/{praktikum}', 'AslabController@listModul')->name('modul_thread');
    Route::get('/thread/modul/{modul}', 'AslabController@listMateri')->name('materi_thread');
    Route::get('/thread/materi/{materi}', 'AslabController@listThread')->name('index_thread');
    Route::get('/thread/detail/{thread}', 'AslabController@view')->name('thread_view');
    Route::post('comment/create/{thread}','AslabController@addThreadComment')->name('threadcomment.aslab');
});


// // Praktikan
Route::group([
    'prefix'     => 'praktikan',
    'middleware' => [
        'auth',
        'peran:praktikan',
    ],
], function() {
    Route::get('/' ,'PraktikanController@index')->name('praktikan');
    Route::get('/editProfile' ,'PraktikanController@edit')->name('praktikan.editProfile.view');
    Route::post('/editProfile' ,'PraktikanController@update')->name('praktikan.editProfile.proses');
    Route::get('/sesiPraktikum' ,'PraktikanController@sesiPraktikum')->name('praktikan.sesiPraktikum');
    Route::get('/registrasiPraktikum' ,'PraktikanController@registrasiPraktikum')->name('praktikan.registrasiPraktikum.view');
    Route::post('/registrasiPraktikum' ,'PraktikanController@registrasiPraktikumProses')->name('praktikan.registrasiPraktikum.proses');
    Route::get('/cetakFormulir/{id}' ,'PraktikanController@cetakFormulir')->name('praktikan.cetakFormulir');
    Route::get('/thread', 'PraktikanController@listPraktikum')->name('thread_praktikum');
    Route::get('/thread/{praktikum}', 'PraktikanController@listModul')->name('thread_modul');
    Route::get('/thread/modul/{modul}', 'PraktikanController@listMateri')->name('thread_materi');
});

Route::get('anu', function(){
    return Hash::make('khisby');
});

Route::get('/thread/materi/{materi}', 'ThreadController@index')->name('thread_index');

Route::get('/thread/create/{materi}', 'ThreadController@create')->name('thread_create');
Route::post('/thread/store', 'ThreadController@store')->name('thread_store');

Route::post('comment/create/{thread}','ThreadController@addThreadComment')->name('threadcomment.store');
//Route::post('reply/create/{comment}','ThreadController@addReplyComment')->name('replycomment.store');

Route::post('thread/mark-as-solution}','ThreadController@threadSolution')->name('markAsSolution');

Route::get('/thread/detail/{thread}', 'ThreadController@view')->name('view_thread');

Route::put ('comment/edit/{comment}','ThreadController@updateComment')->name('threadcomment.update');

