<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login-admin','API\ApiLoginController@loginAdmin');
Route::post('/login-dosen','API\ApiLoginController@loginDosen');
Route::post('/login-praktikan','API\ApiLoginController@loginPraktikan');
Route::post('/login-aslab','API\ApiLoginController@loginAslab');
Route::post('/register-praktikan','API\ApiLoginController@registerPraktikan');
Route::get('/logout','API\ApiLoginController@logout');

Route::post('/dosen', 'API\ApiDosenController@index');
Route::post('/dosen/thread', 'API\ApiDosenController@daftarPraktikum');
Route::get('/dosen/thread/{praktikum}', 'API\ApiDosenController@listModul');
Route::get('/dosen/thread/modul/{modul}', 'API\ApiDosenController@listMateri');
Route::get('/dosen/thread/materi/{materi}', 'API\ApiDosenController@threadList');
Route::post('/dosen/comment/create/{thread}','API\ApiDosenController@addThreadComment');
Route::get('/dosen/thread/detail/{thread}', 'API\ApiDosenController@view');
Route::get('/dosen/thread/comment/praktikan/{thread}', 'API\ApiDosenController@commentPrakikan');
Route::get('/dosen/thread/comment/dosen/{thread}', 'API\ApiDosenController@commentDosen');
Route::get('/dosen/thread/comment/aslab/{thread}', 'API\ApiDosenController@commentAslab');

Route::post('/praktikan', 'API\ApiPraktikanController@index');
Route::post('/praktikan/thread', 'API\ApiPraktikanController@listPraktikum');
Route::get('/praktikan/thread/{praktikum}', 'API\ApiPraktikanController@listModul');
Route::get('/praktikan/thread/modul/{modul}', 'API\ApiPraktikanController@listMateri');
Route::get('/praktikan/thread/materi/{materi}', 'API\ApiPraktikanController@threadList');
Route::post('/praktikan/thread/store', 'API\ApiPraktikanController@store');
Route::get('/praktikan/thread/detail/{thread}', 'API\ApiPraktikanController@view');
Route::post('/praktikan/comment/create/{thread}','API\ApiPraktikanController@addThreadComment');
Route::post('/praktikan/thread/mark-as-solution/{thread}','API\ApiPraktikanController@threadSolution');
Route::get('/praktikan/thread/comment/praktikan/{thread}', 'API\ApiPraktikanController@commentPrakikan');
Route::get('/praktikan/thread/comment/dosen/{thread}', 'API\ApiPraktikanController@commentDosen');
Route::get('/praktikan/thread/comment/aslab/{thread}', 'API\ApiPraktikanController@commentAslab');

Route::post('/aslab', 'API\ApiAslabController@index');
Route::post('/aslab/thread', 'API\ApiAslabController@listPraktikum');
Route::get('/aslab/thread/{praktikum}', 'API\ApiAslabController@listModul');
Route::get('/aslab/thread/modul/{modul}', 'API\ApiAslabController@listMateri');
Route::get('/aslab/thread/materi/{materi}', 'API\ApiAslabController@threadList');
Route::post('/aslab/comment/create/{thread}','API\ApiAslabController@addThreadComment');
Route::get('/aslab/thread/detail/{thread}', 'API\ApiAslabController@view');
Route::get('/aslab/thread/comment/praktikan/{thread}', 'API\ApiAslabController@commentPrakikan');
Route::get('/aslab/thread/comment/dosen/{thread}', 'API\ApiAslabController@commentDosen');
Route::get('/aslab/thread/comment/aslab/{thread}', 'API\ApiAslabController@commentAslab');

//Route::get('/thread/list/all', 'API\ApiThreadController@listThread');

//Route::get('/thread/create/{materi}', 'API\ApiThreadController@create');
//Route::post('/thread/store', 'API\ApiThreadController@store');



//Route::get('/thread/detail/{thread}', 'API\ApiThreadController@view');


//Route::post('register', 'API\UserController@register');
//Route::post('login', 'API\UserController@login');
//
//Route::get('hello', 'API\ApiThreadController@ThreadAuth')->middleware('jwt.verify');
//Route::get('user', 'API\UserController@getAuthenticatedUser')->middleware('jwt.verify');
