{{-- modal edit password dosen --}}
<div class="modal fade" id="editDosen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <div class="section-body">
          <div class="card card-primary">
            <div class="card-body">
              <form method="POST" class="change-password-modal">
                {{ method_field('put') }}
                {{csrf_field()}}
                <div class="row">
                  <div class="form-group col-12">
                    <label for="materi1">Old Password</label>
                    <input type="password" class="form-control" name="old" autofocus>
                    <label for="materi1">New Password</label>
                    <input type="password" class="form-control" name="new1" autofocus>
                    <label for="materi1">Confirm Password</label>
                    <input type="password" class="form-control" name="new2" autofocus>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="agree" class="custom-control-input" id="agree" required>
                    <label class="custom-control-label" for="agree" >I agree with the terms and conditions</label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="change" name="submit" >Change</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  // show modal change password
  $(document).on('click','.editDosen',function(){
    $('#editDosen').modal('show'); 
    $(".change-password-modal").attr('action','{{ url('dosen/change-password') }}');
    // let dataid=$(this).attr('data-id');
    // let temp=$(this).data('materi');
    // $('.modal-title').text('Nilai Modul '+dataid);
  });
</script>