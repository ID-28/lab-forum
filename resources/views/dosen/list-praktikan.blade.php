@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editDosen">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Dosen Page
@endsection

@section('sidebar')
	@include('dosen.sidebar-dosen')
@endsection

@section('content')
<div class="section-body">
  <div class="card card-primary">
    <div class="card-body">
      <div class="card">
        <div class="card-header"><h4>Pilih Praktikum</h4></div>
        <div class="card-body">
          <form method="get" class="pilih_praktikum" action="{{ route('list_praktikan_view') }}">
            {{ method_field('GET') }}
            {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-12">
                <label>Praktikum</label>
                <select class="form-control" id="nama_praktikum" name="nama_praktikum">
                @foreach (App\Periode_praktikum::with('praktikum')->where('periode_praktikum.status',1)->get() as $e)
                    <option value="{{$e->praktikum->id}}">{{$e->praktikum->nama}} {{$e->tahun}}</option>
                @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block entry-nilai" name="submit">Pilih&nbsp;&nbsp;<i class="ion ion-edit"></i></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


  {{-- Modal Change Password --}}
  @include('dosen.form-change-password')    

@endsection
