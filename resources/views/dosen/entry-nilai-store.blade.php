@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editDosen">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Dosen Page
@endsection

@section('sidebar')
	@include('dosen.sidebar-dosen')
@endsection

@section('content')
  {{-- Entry Nilai praktikan --}}
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">
        <div class="card">
          <div class="card-header">
            @if (count($praktikan) == 0)
              <h3>Anda Tidak Mempunyai Praktikan Pada Praktikum Ini</h3>
            @else
              @php
                $praktikum = '';
              @endphp
              <h4 class="float-left">Entry Nilai Praktikan
                @foreach (App\Periode_praktikum::with('praktikum')->where('periode_praktikum.praktikum_id',$idPraktikum)->get() as $e)
                  {{$e->praktikum->nama}} {{$e->tahun}}
                  @php
                    $praktikum = $e->praktikum->nama;
                  @endphp
                @endforeach
              </h4>
              @if ($idPeriodePraktikumDosen->status == 1)
                <p class="float-right">Note: Anda Telah Mengirim Nilai Pada Aslab {{$praktikum}}</p>
              @endif
              @endif
          </div>
          <div class="card-body">
            <div class="table-responsive">
              @if (count($praktikan) != 0)
                <table id="tabelEntryNilai" class="display table table-stripped" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NPM</th>
                      <th>Nama</th>
                      <th>Sesi</th>
                      @for ($i=1; $i<=$banyakModul ; $i++)
                        <th>Nilai {{$i}}</th>
                      @endfor
                    </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($praktikan as $e)
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$e->npm}}</td>
                        <td>{{$e->namaMahasiswa}}</td>
                        <td>{{$e->namaSesi}} {{$e->waktu}}</td>
                        @for ($i=1; $i<=$banyakModul; $i++)
                          @php
                            // $val = $det[$i-1];
                          @endphp
                          <td>
                            @php echo number_format($nilai[$no-1][$i-1],2); @endphp
                            <a href="#"
                              class="icon-right entryNilaiPraktikan"
                              data-modulmateri="{{$modulMateri}}"
                              data-idmodul="{{$idModul[$i-1]->idModul}}" 
                              data-pertemuan="{{$i}}" 
                              data-idpraktikum="{{$idPraktikum}}"
                              data-npm="{{$e->npm}}"
                              data-idpenilaian="{{$e->idPenilaian}}"
                              ><i class="ion-plus-circled"></i>
                            </a>
                          </td> 
                        @endfor
                      </tr>
                      @php
                        $no++;
                      @endphp               
                    @endforeach
                  </tbody>
                </table><br>
              @endif
              <a href="{{ route('entry_nilai') }}" class="btn btn-success">Kembali&nbsp;&nbsp;<i class="fa fa-arrow-circle-left"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- Modal Entry Nilai --}}
  <div class="modal fade" id="entryNilaiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model" action="{{ route('entry_nilai_store') }}">
                    {{ method_field('POST') }}
                    {{csrf_field()}}
                    <div class="row">
                      <div class="form-group col-12 materi">
                        {{-- isi --}}
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Entry Nilai</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <script>
    $(document).ready(function() {
      $('#tabelEntryNilai').DataTable();
    });

    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))

          return false;
      return true;
    }

    $(document).ready(function() {
      $('.entryNilaiPraktikan').click(function(){
        
        let idModul=$(this).data('idmodul');
        let modulMateri=$(this).data('modulmateri');
        let pertemuan=$(this).data('pertemuan');
        let idPraktikum=$(this).data('idpraktikum');
        let npm=$(this).data('npm');
        let idPenilaian=$(this).data('idpenilaian');

        // show modal
        $('#entryNilaiModal').modal('show');
        $('.modal-title').text('Nilai Pertemuan '+pertemuan);

        $(".materi").empty();

        $(".materi").append(
          "<p><b>Topik Materi : </b></p>"
        );

        for (var i = 0; i<modulMateri.length; i++) {
          if(modulMateri[i].idDosen == idModul){
            $(".materi").append(
              "<label><b>"+modulMateri[i].nama+" : </b></label>" +
              "<input type='text' onkeypress='return hanyaAngka(event)' maxlength='2' class='form-control' name='materi[]' id='materi' <?php if($idPeriodePraktikumDosen->status == 1){echo 'disabled';}?> placeholder='Masukkan nilai' autofocus required>" +
              "<input type='hidden' value="+idPraktikum+" class='form-control' name='idPraktikum' autofocus >" +
              "<input type='hidden' value="+npm+" class='form-control' name='npm' autofocus >" +
              "<input type='hidden' value="+idModul+" class='form-control' name='idModul' autofocus >" +
              "<input type='hidden' value="+modulMateri[i].idMateri+" class='form-control' name='materiId[]' autofocus >" +
              "<input type='hidden' value="+idPenilaian+" class='form-control' name='idpenilaian' autofocus > <br>" 
            );
          }
        }
      });
    });
  </script>

@include('dosen.form-change-password')
@endsection

