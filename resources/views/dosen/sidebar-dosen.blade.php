@component('layouts.sidebar' )
    @slot('menu')
        <li class="menu-header">Dashboard</li>
    <li class="{{ Request::is('dosen') ? 'active' : ''}}">
        <a href="{{ route('dosen') }}"><i class="ion ion-ios-home"></i><span>Dashboard</span></a>
    </li>
    <li class="{{ Request::is('dosen/entry-nilai') || Request::is('dosen/entry-nilai/*') ? 'active' : ''}}">
        <a href="{{ route('entry_nilai') }}"><i class="ion ion-clipboard"></i><span>Entry Nilai</span></a>
    </li>
    <li class="{{ Request::is('dosen/list-praktikan') || Request::is('dosen/list-praktikan/*') ? 'active' : ''}}">
        <a href="{{ route('list_praktikan') }}"><i class="ion ion-android-contact"></i><span>List Praktikan</span></a>
    </li>
    <li class="{{ Request::is('dosen/thread') || Request::is('dosen/thread/*') ? 'active' : ''}}">
        <a href="{{ route('thread_awal') }}"><i class="ion ion-android-contact"></i><span>Thread</span></a>
    </li>

    {{-- <li>
        <a href="#" class="has-dropdown"><i class="ion ion-levels"></i><span>Entry Nilai</span></a>
        <ul class="menu-dropdown">
            <form method="get" action="{{ route('entry_nilai_create') }}">
                {{ method_field('GET') }}
                {{ csrf_field() }}
                @foreach (App\Periode_praktikum::with('praktikum')->where('periode_praktikum.status',1)->get() as $e)
                    <li class="#">
                        <a href="{{ route('entry_nilai_create') }}"><i class="ion ion-ios-circle-outline"></i>{{$e->praktikum->nama}} {{$e->tahun}}</a>
                    </li>
                    <input type="hidden" name="nama_praktikum" value="{{$e->praktikum->id}}">
                @endforeach
            </form>

        </ul>
    </li> --}}
    @endslot
@endcomponent
