@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editDosen">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Dosen Page
@endsection

@section('sidebar')
	@include('dosen.sidebar-dosen')
@endsection

@section('content')

  {{-- Entry Nilai praktikan --}}
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">
        <div class="card">
          <div class="card-header">
            @if (count($praktikan) == 0)
              <h3>Anda Tidak Mempunyai Praktikan Pada Praktikum Ini</h3>
            @else
              <h4 class="float-left">List Praktikan
                @php
                    $praktikum = '';
                @endphp
                @foreach (App\Periode_praktikum::with('praktikum')->where('periode_praktikum.praktikum_id',$idPraktikum)->get() as $e)
                  {{$e->praktikum->nama}} {{$e->tahun}}
                  @php
                    $praktikum = $e->praktikum->nama;
                  @endphp 
                @endforeach
              </h4>
              <p style="color: red" class="float-right" ><b>Penting:</b> Jika Sudah Mengirim Nilai Anda Tidak Bisa Merubah Nilai Pada Menu Entry Nilai</p>
              {{-- <a class="float-right btn btn-action btn-primary" href="{{ url('dosen/export/'.$idPraktikum) }}">Email Ke Aslab</a>          
              <a class="float-right btn btn-action btn-primary" href="{{ url('dosen/get-pdf/'.$idPraktikum) }}">Unduh PDF</a> --}}
            @endif
          </div>
          <div class="card-body">
            <div class="table-responsive">
              @if (count($praktikan) != 0)
                <table id="tabelListPraktikan" class="display table table-stripped" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NPM</th>
                      <th>Nama</th>
                      <th>Sesi</th>
                      @for ($i=1; $i<=$banyakModul ; $i++)
                        <th>Nilai {{$i}}</th>
                      @endfor
                      <th>Nilai Akhir</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($praktikan as $e)
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$e->npm}}</td>
                        <td>{{$e->namaMahasiswa}}</td>
                        <td>{{$e->namaSesi}} {{$e->waktu}}</td>
                        @for ($i=1; $i<=$banyakModul; $i++)
                          <td>
                            {{number_format($avg[$no-1][$i-1],2) }}
                          </td> 
                        @endfor
                        <td>
                          {{number_format($e->nilaiAkhir,2) }}
                        </td>
                      </tr>
                      @php
                        $no++;
                      @endphp               
                    @endforeach
                  </tbody>
                </table><br>
              @endif
            </div>
            <a href="{{ route('list_praktikan') }}" class="btn btn-success float-left">Kembali&nbsp;&nbsp;<i class="fa fa-arrow-circle-left"></i></a>
            @if ($idDosenLab->status == 0)
              <form action="{{ route('kirimNilai', ['id'=>$idDosenLab->idLab]) }}" method="post">
                @method('PUT')
                @csrf
                  <button type="submit" class="btn btn-success float-right ">Kirim Nilai Ke Aslab {{$praktikum}}</button>
              </form>
              <br><br><br>
              <p class="float-right">Note: Jika Praktikum Sudah Selesai Kirim Nilai Dengan Klik Tombol Diatas</p>
            @else
              <p class="float-right">Note: Anda Telah Mengirim Nilai Praktikan</p>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
     $(document).ready(function() {
      $('#tabelListPraktikan').DataTable();
    });
  </script>

    @include('dosen.form-change-password')
@endsection

