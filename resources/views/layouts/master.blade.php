      {{-- @include('layouts.header')       --}}

      @component('layouts.header')
        @slot('nama_user')
          @yield('nama_user')
        @endslot
        @slot('btnProfile')
          @yield('btnProfile')
        @endslot
      @endcomponent
      <div id="preloader">
        <div class="loader" id="loader-1"></div>
      </div>

      {{-- sidebar --}}
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          {{-- title sidebar --}}
          <div class="sidebar-brand">
            <a href="{{ url('/') }}">@yield('title_page')</a>
          </div>
          @yield('sidebar')
        </aside>
      </div>

      {{-- Main Content --}}
      <div class="main-content">
        <section class="section">
            <div>@yield('nav_main')</div>
                {{-- <br>
                  @if ($errors->any())
                      <div class="alert alert-danger" id="danger-alert">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>
                                    <a  class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ $error }} </strong>
                                  </li>
                              @endforeach
                          </ul>
                      </div>
                      @elseif ($flash = session('massage'))
                      <div class="alert alert-success" id="success-alert">
                              {{$flash}}
                      </div>
                  @endif --}}
        </section>

        @yield('content')
      </div>
      @include('layouts.footer')
    @include('layouts.message')
{{-- <script>
// Danger alert
 $("#danger-alert").fadeTo(3000, 700).slideUp(700, function(){
    $("#danger-alert").slideUp(700);
}); --}}
</script>
