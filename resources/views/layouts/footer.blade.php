      {{-- Footer --}}
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2019 <div class="bullet"></div> Design By Lab RPL
        </div>
        <div class="footer-right"></div>
      </footer>

    </div>
  </div>

  <script src="{{ asset ('dist/modules/popper.js')}}"></script>
  <script src="{{ asset ('dist/modules/datatables/jquery.datatables.min.js')}}"></script>
  <script src="{{ asset ('dist/modules/tooltip.js')}}"></script>
  <script src="{{ asset ('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset ('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
  <script src="{{ asset ('dist/js/sa-functions.js')}}"></script>
  <script src="{{ asset ('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset ('dist/modules/chart.min.js')}}"></script>
  <script src="{{ asset ('dist/modules/summernote/summernote-lite.js')}}"></script>


  <script src="{{ asset ('dist/js/scripts.js') }} "></script>
  <script src="{{ asset ('dist/js/custom.js') }} "></script>
  <!-- <script src="{{ asset ('dist/js/demo.js') }} "></script> -->
  @include('assets.loader.js')
  <!-- <script>
     $(document).ready(function(){
        $(".loader").fadeOut("slow");
    });
  </script> -->
  @yield('scriptBottom')

</body>
</html>