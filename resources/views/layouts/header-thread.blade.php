<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">

  {{-- @can('managejarkom') --}}
  <link rel="shortcut icon" href="{{ url('/') . '/asset/favicon.ico' }}" type="image/x-icon">
  <link rel="icon" href="{{ url('/') . '/asset/favicon.ico' }}" type="image/x-icon">
  <title>Lab Rekayasa Perangkat Lunak</title>
  {{-- @endcan --}}


  <link rel="stylesheet" href="{{ asset ('dist/modules/bootstrap/css/bootstrap.min.css ') }}">
  <link rel="stylesheet" href="{{ asset ('dist/modules/ionicons/css/ionicons.min.css ') }}">
  <link rel="stylesheet" href="{{ asset ('dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css ') }}">

  <link rel="stylesheet" href="{{ asset ('dist/modules/summernote/summernote-lite.css ') }}">
  <link rel="stylesheet" href="{{ asset ('dist/modules/flag-icon-css/css/flag-icon.min.css ') }}">
  <link rel="stylesheet" href="{{ asset ('dist/modules/datatables/jquery.datatables.min.css ') }}">
  <!-- <link rel="stylesheet" href="{{ asset ('dist/css/demo.css ') }}"> -->
  <link rel="stylesheet" href="{{ asset ('dist/css/style.css ') }}">
  <link rel="stylesheet" href="{{ asset ('css/toastr.min.css') }}">
  <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <script src="{{ asset ('js/toastr.min.js') }} "></script>
  <script src="{{ asset ('dist/modules/jquery.min.js')}}"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  @include('assets.loader.css')

</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <!-- navbar -->
      <nav class="navbar navbar-expand-lg main-navbar" style="left: -245px">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="ion ion-search"></i></a></li>
          </ul>

          <!-- Search -->
          {{-- <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            <button class="btn" type="submit"><i class="ion ion-search"></i></button>
          </div> --}}
        </form>


        <ul class="navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
            <i class="ion ion-android-person d-lg-none"></i>
            <div class="d-sm-none d-lg-inline-block">
              @php
                  if(!isset($_SESSION['aslab'])){
                    $namaDosen = App\Dosen::select('nama')->where('nip',Auth::user()->username)->get();
                    $namaAdmin = App\User::select('username')->where('username',Auth::user()->username)->get();
                  }
              @endphp
              @if(Auth::check())
                @if (Auth::user()->hasperan('admin'))
                  {{$namaAdmin[0]->username}}
                @elseif (Auth::user()->hasperan('dosen'))
                  {{$namaDosen[0]->nama}}
                @elseif (Auth::user()->hasperan('super admin'))
                  {{$namaAdmin[0]->username}}
                @elseif (Auth::user()->hasperan('praktikan'))
                  {{$namaAdmin[0]->username}}
                @endif
              @else
                  {{ $_SESSION['aslab']->nama }}
              @endif


            </div></a>
            <div class="dropdown-menu dropdown-menu-right">
              {{$btnProfile}}
              {{-- <a href="profile.html" class="dropdown-item has-icon">
                <i class="ion ion-android-person"></i> Profile
              </a> --}}
              <a href="{{ route('logout') }}" class="dropdown-item has-icon">
                <i class="ion ion-log-out"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  @yield('js')
</body>
