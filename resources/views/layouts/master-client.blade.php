<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="Laboratorium Teknik Informatika Institut Teknologi Adhi Tama Surabaya. Bagian dari kami Laboratorium Bahasa Pemrograman (Basprog), Laboratorium Jaringan Komputer (Jarkom), Laboratorium Rekayasa Perangkat Lunak (RPL).">
    <meta name="author" content="khisoft">
    <meta name="url" content="http://www.khisoft.id">
    <meta name="copyright" content="Khisoft">
    <meta name="robots" content="index,follow">
    
    <link rel="shortcut icon" href="{{ url('/') . '/asset/favicon.ico' }}" type="image/x-icon">
    <link rel="icon" href="{{ url('/') . '/asset/favicon.ico' }}" type="image/x-icon"> 
    <link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="{{ url('/') . '/asset/favicon.ico' }}">
    <title>Laboratorium Teknik Informatika</title>

    <link rel="stylesheet" href="{{ url('/') . '/vendor/fusion/css/bootstrap.min.css' }}" >
    <link rel="stylesheet" href="{{ url('/') . '/vendor/fusion/fonts/line-icons.css' }}">
    <link rel="stylesheet" href="{{ url('/') . '/vendor/fusion/css/animate.css' }}">
    <link rel="stylesheet" href="{{ url('/') . '/vendor/fusion/css/main.css' }}">
    <link rel="stylesheet" href="{{ url('/') . '/vendor/fusion/css/responsive.css' }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <style>
        .container-berita .card > * {
            font-family: "Poppins", sans-serif !important;
        }
    </style>

  </head>
  <body>

    <header id="header-wrap">
      <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container" style="margin-top: 10px;">
          <a href="{{ url('/') }}" class="navbar-brand"><img src="{{ url('/') . '/asset/informatika.png' }}" alt="" style="width: 60%; height: auto;"></a>       
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item {{ Route::current()->getName() == 'home' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">
                  Beranda
                </a>
              </li>
              <li class="nav-item {{ (Route::current()->getName() == 'berita' || Route::current()->getName() == 'beritaDetail') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('berita') }}">
                  Informasi
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login.praktikan.view') }}">
                  Login Praktikan
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login.dosen.view') }}">
                  Login Dosen
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('register.praktikan.view') }}">
                  Register Praktikan
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    @yield('content')

<!-- 
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="copyright-content">
                <p>Copyright © 2019 LAB RPL</p>
              </div>
            </div>
          </div>
        </div>
      </div>   
    </footer>  -->
    <!-- Footer Section End -->
    
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <script src="{{ url('/') . '/vendor/fusion/js/jquery-min.js' }}"></script>
    <!-- <script src="{{ url('/') . '/vendor/fusion/js/popper.min.js' }}"></script> -->
    <script src="{{ url('/') . '/vendor/fusion/js/bootstrap.min.js' }}"></script>
    <script src="{{ url('/') . '/vendor/fusion/js/main.js' }}"></script>  
    @include('assets.loader.js')
  </body>
</html>
