      {{-- @include('layouts.header')       --}}

      @component('layouts.header-thread')
        @slot('nama_user')
          @yield('nama_user')
        @endslot
        @slot('btnProfile')
          @yield('btnProfile')
        @endslot
      @endcomponent

      {{-- Main Content --}}
      <div class="main-content" style="margin-left: -18.5%; width: inherit">
        <section class="section">
            <div>@yield('nav_main')</div>

        </section>

        @yield('content')
      </div>
      @include('layouts.footer')
    @include('layouts.message')
{{-- <script>
// Danger alert
 $("#danger-alert").fadeTo(3000, 700).slideUp(700, function(){
    $("#danger-alert").slideUp(700);
}); --}}
</script>
