@component('layouts.sidebar' )
    @slot('menu')
        <li class="menu-header">Dashboard</li>
        <li class="{{ Request::is('super') ? 'active' : ''}}">
            <a href="{{ route('index') }}"><i class="ion ion-speedometer"></i><span>Dashboard</span></a>
        </li>
        <li class="{{ Request::is('xx') ? 'active' : ''}}">
            <a href=""><i class="ion ion-speedometer"></i><span>Manage Account</span></a>
        </li>
        <li class="{{ Request::is('xx') ? 'active' : ''}}">
            <a href=""><i class="ion ion-speedometer"></i><span>Manage Privilage</span></a>
        </li>
    @endslot
@endcomponent
