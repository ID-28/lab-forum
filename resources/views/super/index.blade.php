@extends('layouts/master')

@section('title_page')
  SUPERMAN
@endsection

@section('sidebar')
  @include('super\sidebar-super')
@endsection

@section('content')
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">

        <h2 class="section-title">Welcome Superman</h2><br>
        <div class="row">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-primary">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  24
                </div>
                <div class="card-header pb-0">
                  <h4>Total Praktikan</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('listPraktikanAslab') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-danger">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  12
                </div>
                <div class="card-header pb-0">
                  <h4>Total Aslab</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-warning">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  1,201
                </div>
                <div class="card-header pb-0">
                  <h4>Other</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="#">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-success">
                <i class="ion ion-record"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  47
                </div>
                <div class="card-header pb-0">
                  <h4>Other</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="#">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection