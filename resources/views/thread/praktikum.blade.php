@extends('layouts.master-thread')

@section('content')
    @if($id_user->jenis_user == 3)
        @foreach($praktikum as $p)
            <a href="{{route('thread_kedua',$p->idPraktikum)}}" class="list-group-item">
                <h4 class="list-group-item-heading">{{$p->namaPraktikum}}</h4>
            </a>
            <br>
        @endforeach
    @elseif($id_user->jenis_user == 5)
        @foreach($praktikum as $p)
            <a href="{{route('thread_modul',$p->idPraktikum)}}" class="list-group-item">
                <h4 class="list-group-item-heading">{{$p->namaPraktikum}}</h4>
            </a>
            <br>
        @endforeach
    @endif

@endsection
