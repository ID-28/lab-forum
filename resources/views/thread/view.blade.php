@extends('layouts.master')

@section('sidebar')
    @if($jenisUser->jenis_user == 5)
        @include('layouts.sidebar-praktikan')
    @elseif($jenisUser->jenis_user == 3)
        @include('dosen.sidebar-dosen')
    @elseif($jenisUser->jenis_user == 4)
        @include('aslab.sidebar-aslab')
    @endif
@endsection

@section('content')
    @if($jenisUser->jenis_user == 5)
        <div class="blockquote">
            <a href="{{route('praktikan')}}" style="color: #00aced"><i class="ion ion-home"></i></a> /
            <a href="{{route('thread_modul',$materi->idPraktikum)}}" style="color: #00aced">{{$materi->namaPraktikum}}</a> /
            <a href="{{route('thread_materi',$materi->idModul)}}" style="color: #00aced">{{$materi->namaModul}}</a> /
            <a href="{{route('thread_index',$materi->idMateri)}}" style="color: #00aced">{{$materi->namaMateri}}</a> /
            <a style="color: #00aced">{{$materi->judulThread}}</a>
        </div>
    @elseif($jenisUser->jenis_user == 3)
        <div class="blockquote">
            <a href="{{route('dosen')}}" style="color: #00aced"><i class="ion ion-home"></i></a> /
            <a href="{{route('thread_awal')}}" style="color: #00aced">{{$materi->namaPraktikum}}</a> /
            <a href="{{route('thread_kedua',$materi->idPraktikum)}}"
               style="color: #00aced">{{$materi->namaModulDosen}}</a> /
            <a href="{{route('thread_ketiga',$materi->idModulDosen)}}"
               style="color: #00aced">{{$materi->namaMateri}}</a>
        </div>
    @endif
    <div class="section-body">
        <div class="card card-primary">
            <div class="card-body">
                <div class="col-md-pull-3 fa-pull-right">
                    @if($jenisUser->jenis_user == 5)
                        <a class="btn btn-danger" href="{{route('thread_index',$materi->idMateri)}}"><i class="ion ion-android-arrow-back"></i></a>
                    @else
                        <a class="btn btn-danger" href="{{url()->previous()}}">GO BACK</a>
                    @endif
                </div>
                <h1>{{$materi->judulThread}}</h1>
                <hr>

                <p> Name : {{$materi->namaMahasiswa}} // NPM : {{$materi->username}}
                    at {{\Carbon\Carbon::parse($materi->posted)->diffForHumans()}} asked :</p>

                <div class="thread-details">
                    <h4>{!! \Michelf\Markdown::defaultTransform($materi->ket) !!}</h4>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <br><br>


    {{--    Answer/comment--}}
    <div class="section-body">
        <div class="card card-primary">
            <div class="card-body">
                <h4 class="section-title">Comment Praktikan</h4>
                <hr>
                @forelse($MComment as $c)
                    <div class="comment-list ">
                        @include('thread.partials.comment-list')
                    </div>
                    <hr>
                @empty
                    <h3>Belum ada yang komentar</h3>
                @endforelse
                @if($jenisUser->jenis_user == 5)
                    @include('thread.partials.comment-form')
                @endif
            </div>
        </div>
    </div>  <br>

    <div class="section-body">
        <div class="card card-primary">
            <div class="card-body">
                <h4 class="section-title">Comment Dosen</h4>
                <hr>
                @forelse($DComment as $c)
                    <div class="comment-list well well-lg">
                        @include('thread.partials.comment-list')
                    </div>
                    <hr>
                @empty
                    <h3>Belum ada Dosen yang komentar</h3>
                @endforelse
                @if($jenisUser->jenis_user == 3)
                    @include('thread.partials.comment-form')
                @endif
            </div>
        </div>
    </div> <br>

    <div class="section-body">
        <div class="card card-primary">
            <div class="card-body">
                <h4 class="section-title">Comment Asisten</h4>
                <hr>
                @forelse($AComment as $c)
                    <div class="comment-list ">
                        @include('thread.partials.comment-list')
                    </div>
                    <hr>
                @empty
                    <h3>Belum ada yang komentar</h3>
                @endforelse
                @if($jenisUser->jenis_user == 4)
                    @include('thread.partials.comment-form')
                @endif
            </div>
        </div>
    </div>  <br>
@endsection
