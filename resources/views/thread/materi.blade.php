@extends('layouts.master-thread')

@section('content')
    @foreach($materi as $m)
        <a href="{{route('thread_index',$m->idmat)}}" class="list-group-item">
            <h4 class="list-group-item-heading">{{$m->namaMateri}}</h4>
        </a>
        <br>
    @endforeach
@endsection
