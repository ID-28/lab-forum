@extends('layouts.front')

@section('heading',"Edit Thread")

@section('content')
    <div class="row">
        <div class="well">
                <form class="form-vertical" action="{{route('thread.update',$thread->id)}}" method="post" role="form"
                      id="create-thread-form">
                    {{csrf_field()}}
                    {{method_field('put')}}
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" class="form-control" name="subject" id="" placeholder="Input..."
                               value="{{$thread->subject}}">
                    </div>

{{--                    <div class="form-group">--}}
{{--                        <label for="tag">Tags</label>--}}
{{--                        <select name="tags[]" multiple id="tag">--}}
{{--                            --}}{{-- todo add from db--}}
{{--                            @foreach($tags as $tag)--}}
{{--                                <option value="{{$tag->id}}">{{$tag->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}

                    <div class="form-group">
                        <label for="type">Subject</label>
                        <input type="text" class="form-control" name="type" id="" placeholder="Input..."
                               value="{{$thread->type}}">
                    </div>

                    <div class="form-group">
                        <label for="thread">Thread</label>
                        <textarea class="form-control" name="thread" id="" placeholder="Input..."
                        > {{$thread->thread}}</textarea>
                    </div>

                    {{--  <div class="form-group">
                       {!! app('captcha')->display() !!}
                    </div>  --}}

                    <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection