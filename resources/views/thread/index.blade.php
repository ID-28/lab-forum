@extends('layouts.master')

@section('heading')

@endsection

@section('sidebar')
    @if($jenisUser->jenis_user == 5)
        @include('layouts.sidebar-praktikan')
    @elseif($jenisUser->jenis_user == 3)
        @include('dosen.sidebar-dosen')
    @elseif($jenisUser->jenis_user == 4)
        @include('aslab.sidebar-aslab')
    @endif
@endsection

@section('content')

    @if($jenisUser->jenis_user != 4)
        @if(!empty($praktikum) || !empty($praktikum_dosen) )
            @include('thread.partials.thread-list')
        @else
            <h1>Hello</h1>
        @endif
    @else
        @include('thread.partials.thread-list')
    @endif
@endsection
