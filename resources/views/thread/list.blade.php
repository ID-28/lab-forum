@extends('layouts/master')

@section('heading',"List Thread All")

@section('content')
    <div class="list-group">

        @forelse($thread as $t)
            <a href="{{route('thread_detail',$t->idThread)}}" class="list-group-item">
                <h4 class="list-group-item-heading">{{$t->judul}}</h4>
                <p class="list-group-item-text">{{str_limit($t->keterangan,100) }}</p>
                <hr>
                <p class="list-group-item-text">{{$t->namaMahasiswa}}</p>

            </a>
            <br>
        @empty
            <h5>No threads</h5>

        @endforelse
    </div>
@endsection
