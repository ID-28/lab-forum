@if(!empty($c->state))
    @if($c->state == $c->idComment)
        <button class="btn btn-success float-right" style="pointer-events: none">Solution</button>
     @endif
@else
    @if(auth()->check())
        @if(auth()->user()->id == $c->user_id)
            <form action="{{route('markAsSolution')}}" method="post" >
                {{csrf_field()}}
                <input type="hidden" name="threadId" value="{{$c->idThread}}">
                <input type="hidden" name="statusId" value="{{$c->idComment}}">
                <input type="submit" class="btn btn-success float-right" id="{{$c->idComment}}" value="Mark As Solution">
            </form>
        @endif
    @endif
@endif

<lead>{{$c->nama}} || {{$c->username}} at {{\Carbon\Carbon::parse($c->created_at)->format('d/m/Y H:i')}} said: </lead>

<h4>{{$c->comment}}</h4>

{{--@if($idUserLogin->idUser == $c->comment_id && $jenisUser->jenis_user == $c->comment_by)--}}
{{--    <div class="actions">--}}
{{--        <a class="btn btn-primary btn-xs" data-toggle="modal" href="#{{$c->idComment}}">Edit</a>--}}
{{--        <div class="modal fade" id="{{$c->idComment}}">--}}
{{--            <div class="modal-dialog">--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;--}}
{{--                        </button>--}}
{{--                        <h4 class="modal-title">Edit Comment</h4>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}
{{--                        <div class="comment-form">--}}
{{--                            <form action="{{route('threadcomment.update',$c->idComment)}}" method="post"--}}
{{--                                  role="form">--}}
{{--                                {{csrf_field()}}--}}
{{--                                {{method_field('put')}}--}}

{{--                                <div class="form-group">--}}
{{--                                    <input type="text" class="form-control" name="body" id=""--}}
{{--                                           placeholder="Input..." value="{{$c->comment}}">--}}
{{--                                </div>--}}

{{--                                <button type="button" class="btn btn-default" data-dismiss="modal">Close--}}
{{--                                </button>--}}
{{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div><!-- /.modal-content -->--}}
{{--            </div><!-- /.modal-dialog -->--}}
{{--        </div><!-- /.modal -->--}}
{{--    </div>--}}

{{--@endif--}}

@if($jenisUser->jenis_user == 2)
    <form action="#" method="POST" class="inline-it">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <input class="btn btn-xs btn-danger" type="submit" value="Delete">
    </form>
@endif
