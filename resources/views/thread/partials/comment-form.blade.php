<div class="section-body">
    <div class="card card-primary">
        <div class="card-body">
            <div class="comment-form">
                @if ($jenisUser->jenis_user != 4)
                    <form action="{{route('threadcomment.store',$materi->idThread)}}" method="post" role="form" class="form-inline">
                        {{csrf_field()}}
                        <legend>Add Comment</legend>

                        <div class="form-group" style="margin-right: 2%; ">
                            <input type="text" class="form-control col-lg-12" name="body" id="" placeholder="comment" size="100">
                        </div>
                        <button type = "submit" class = "btn btn-default">Submit</button>
                    </form>
                @else
                    <form action="{{route('threadcomment.aslab',$materi->idThread)}}" method="post" role="form">
                        {{csrf_field()}}
                        <legend>Create comment</legend>

                        <div class="form-group">
                            <input type="text" class="form-control" name="body" id="" placeholder="Input...">
                        </div>

                        <button type="submit" class="btn btn-primary">Comment</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
