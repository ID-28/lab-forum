@if($jenisUser->jenis_user == 5)
    <div class="col-md-3">
        <a class="btn btn-success form-control"  href="{{route('thread_create',$datamateri->idMateri)}}">Create Thread</a>
    </div>

    <div class="blockquote">
        <a href="{{route('praktikan')}}">Home</a> /
        <a href="{{route('thread_modul',$datamateri->idPraktikum)}}">{{$datamateri->namaPraktikum}}</a> /
        <a href="{{route('thread_materi',$datamateri->idModul)}}">{{$datamateri->namaModul}}</a> /
        <a href="{{route('thread_index',$datamateri->idMateri)}}">{{$datamateri->namaMateri}}</a>
    </div>

    <h1>
        {{$datamateri->namaMateri}}
    </h1>
@elseif($jenisUser->jenis_user == 3)
    <div class="blockquote">
        <a href="{{route('dosen')}}" style="color: #00aced">Home</a> /
        <a href="{{route('thread_awal')}}" style="color: #00aced">{{$datamateri->namaPraktikum}}</a> /
        <a href="{{route('thread_kedua',$datamateri->idPraktikum)}}" style="color: #00aced">{{$datamateri->modulDosen}}</a> /
        <a href="{{route('thread_index',$datamateri->idMateri)}}" style="color: #00aced">{{$datamateri->namaMateri}}</a>
    </div>

    <h1>
        {{$datamateri->namaMateri}}
    </h1>
@else
    <div class="blockquote">
        <a href="{{route('aslab')}}" style="color: #00aced">Home</a> /
        <a href="{{route('praktikum_thread')}}" style="color: #00aced">{{$datamateri->namaPraktikum}}</a> /
        <a href="{{route('modul_thread',$datamateri->idPraktikum)}}" style="color: #00aced">{{$datamateri->namaModul}}</a> /
        <a href="{{route('materi_thread',$datamateri->idModul)}}" style="color: #00aced">{{$datamateri->namaMateri}}</a>
    </div>

    <h1>
        {{$datamateri->namaMateri}}
    </h1>
@endif

<div class="list-group">

    @forelse($thread as $t)
        @if ($jenisUser->jenis_user != 4)
            <a href="{{route('view_thread',$t->idThread)}}" class="list-group-item">
        @else
            <a href="{{route('thread_view',$t->idThread)}}" class="list-group-item">
        @endif

        <h4 class="list-group-item-heading">{{$t->judul}}</h4>
        <p class="list-group-item-text">{{str_limit($t->keterangan,50) }}
                <hr>
        <p class="list-group-item-text">{{$t->namaMahasiswa}}</p>
                {{--        Posted by <a href="{{route('user_profile',$thread->user->name)}}">{{$thread->user->name}}</a>--}}
        </p>
        </a>
        <br>
    @empty
        <h5>No threads</h5>

    @endforelse
</div>
