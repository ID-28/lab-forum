@extends('layouts.master-thread')

@section('heading',"Create Thread")

@section('content')

    <div class="row">
        <div class="well col-md-7">
                <form class="form-vertical" action="{{route('thread_store')}}" method="post" role="form"
                      id="create-thread-form">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="judul">Judul Thread</label>
                        <input type="text" class="form-control" name="judul" id="" placeholder="Judul Thread Anda"
                               value="{{old('judul')}}">
                    </div>

                    <div class="form-group">
                        <label for="materi">Materi</label>
                        <select name="materi" id="materi" class="form-control">
                            <option value="{{$mate->idmat}}"> {{$mate->namaMate}} </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="" placeholder="Detail Thread Anda"
                        > {{old('keterangan')}}</textarea>
                    </div>

                    {{--  <div class="form-group">
                       {!! app('captcha')->display() !!}
                    </div>  --}}

                    <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection
