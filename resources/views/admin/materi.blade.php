@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')
    @slot('nav1')
      Preview Kumpulan Materi
    @endslot
    @slot('nav2')
      Register Kumpulan Materi
    @endslot
  @endcomponent
@endsection

@section('content')


<div class="tab-content" id="myTabContent">
    {{-- previeww materi --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Materi Praktikum {{$praktikum[0]->nama}} Dan {{$praktikum[1]->nama}}</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabelMateri" class="display table table-stripped" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Praktikum</th>
                        <th>Nama Materi</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                      @endphp
                        @foreach ($materi as $e)
                          <tr>
                            <td>{{$i}}</td>
                            <td>{{$e->namaPraktikum}}</td>
                            <td>{{$e->nama}}</td>
                            <td>
                            <button class="btn btn-primary btn-action mr-1 updateRecord" data-user='["{{$e->id}}","{{$e->nama}}","{{$e->idPraktikum}}"]' ><i class="ion ion-edit"></i></button>
                                <button class="btn btn-danger btn-action mr-1 deleteRecord"  data-userid="{{$e->id}}"><i class="ion ion-trash-b"></i></button>
                            </td>
                            @php
                              $i++;
                            @endphp
                          </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Register Materi --}}
    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Register Materi Praktikum {{$praktikum[0]->nama}} Dan {{$praktikum[1]->nama}}</h4>
              </div>
              <div class="card-body">
                <form method="POST" action="{{ route('materi_store') }}">
                  {{csrf_field()}}
                  <div class="row">
                    <div class="form-group col-12">
                      <label>Praktikum <span style="color: red">*</span></label>
                      <select class="form-control" name="praktikum">
                        @foreach ($daftarPraktikum as $e)
                            <option value='{{$e->id}}' >{{$e->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-12">
                      <label for="frist_name">Nama Materi <span style="color: red">*</span></label>
                      <input id="frist_name" type="text" class="form-control" name="nama" placeholder="Nama Materi" autofocus required>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" name="submit">
                      Register
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    {{-- modal Delete --}}
    <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="remove-record-model">
            {{ method_field('delete') }}
            {{ csrf_field() }}

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger"">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Materi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}

                    <div class="row">
                      <div class="form-group col-12">
                        <label>Praktikum <span style="color: red">*</span></label>
                        <select class="form-control upraktikum " name="upraktikum" >
                          @foreach ($daftarPraktikum as $e)
                            <option value='{{$e->id}}' >{{$e->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="nama_dosen">Nama Materi <span style="color: red">*</span></label>
                        <input  type="text" class="form-control" name="unama" autofocus required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <script>
    $(document).ready(function() {
      $('#tabelMateri').DataTable();
    });

    // Delete Modal Record
    $(document).on('click','.deleteRecord',function(){
        $('#deleteModalRecord').modal('show');
        let userID=$(this).attr('data-userid');
        $(".remove-record-model").attr('action', '{{ url('admin/management-materis/delete') }}'+'/'+userID);
    });

    // Update Modal Record
    $(document).on('click','.updateRecord',function(){
        $('#updateModalRecord').modal('show');
        let user=$(this).data('user');
        $('.upraktikum').val(user[2]);
        $('input[name=unama]').val(user[1]);
        $(".update-record-model").attr('action', '{{ url('admin/management-materis/update') }}'+'/'+user[0]);
    });
  </script>
  @include('admin.form-change-password')    
@endsection