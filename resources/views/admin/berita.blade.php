@extends('layouts/master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Berita
    @endslot
    @slot('nav2')
      Register Berita
    @endslot     
  @endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- previeww asleb --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Berita</h4>
              </div>
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table table-striped" id="datatables">
                    <thead>
                      <tr>
                        <th>Judul</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($berita as $a)
                      <tr>
                          <td>{{ $a->judul }}</td>
                          <td>
                            <a href="{{ route('manajemenBeritaView',$a->id) }}" class="btn btn-action btn-primary">View</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    {{-- Tambah Berita --}}
    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <form method="POST" action="{{ route('manajemenBeritaTambahProses') }}" enctype='multipart/form-data'>
              @csrf
              <div class="row">
                <div class="form-group col-12">
                  <label for="judul">Judul</label>
                  <input id="judul" type="text" class="form-control" name="judul" autofocus required>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label for="gambar">Gambar</label>
                  <input type="file" id="gambar" class="form-control" name="gambar">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label for="content">Content</label>
                  <textarea name="content" id="content" cols="30" rows="10" class="form-control" required></textarea>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" name="submit">
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- @include('admin.form-change-password')         -->

@endsection

@section('scriptBottom')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
            $('#content').summernote({
              height: 300,
              popover: {
                image: [],
                link: [],
                air: []
              }
            });
        });
    </script>
@endsection