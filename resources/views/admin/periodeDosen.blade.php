@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Dosen Aktif
    @endslot
    @slot('nav2')
      Atur Dosen Aktif
    @endslot     
  @endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Dosen Aktif Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="datatables">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIP</th>
                        <th>Nama </th>
                        <th>Praktikum</th>
                        <th>Periode</th>
                        <th>Pagi/Malam</th>
                        <th>Kuota</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                      @endphp
                      @foreach($periodeDosen as $p)
                        <tr>
                          <td>{{$i}}</td>
                          <td>{{ $p->nip }}</td>
                          <td>{{ $p->nama }}</td>
                          <td>{{ $p->namaPraktikum }}</td>
                          <td>{{ $p->tahun }}</td>
                          <td>{{ $p->pagiMalam }}</td>
                          <td>{{ $p->kuota }}</td>
                          <td>
                              <a href="#" class="btn btn-action btn-primary updateRecord" data-user='["{{$p->id}}" , "{{$p->nama}}","{{$p->idPagiMalam}}" ,"{{$p->kuota}}"]'><i class="ion ion-edit"></i></a>
                              <a href="#" class="btn btn-action btn-danger deleteRecord" data-userid="{{$p->id}}"><i class="ion ion-trash-b"></i></a>
                          </td>
                          @php
                            $i++;
                          @endphp
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
            <div class="card-header">
              <h4>Register Dosen Aktif {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
            </div> 
            <div class="card-body"> 
              <form method="POST" action="{{ route('periodeDosenProses') }}">
                @csrf
                <div class="row">
                  <div class="form-group col-5">
                    <label for="tahun">Praktikum <span style="color: red"> *</span></label>
                    <input type="hidden" name="periodePraktikumId" value="{{ $praktikum->id }}">
                    <input type="text" value="{{ $praktikum->nama }} {{ $praktikum->tahun }}" class="form-control" disabled>
                  </div>
                  <div class="form-group col-7">
                    <label>Dosen <span style="color: red"> *</span></label>
                    @if (count($cekDosen) == 0)
                      <input type="text" value="Dosen Tidak Tersedia" class="form-control" disabled>
                    @elseif (count($dosen) == 0)
                      <input type="text" value="Dosen Sudah Terpakai Semua" class="form-control" disabled>
                    @else
                      <select name="dosen" class="form-control">
                        @foreach($dosen as $a)
                            <option value="{{ $a->id }}">{{ $a->nama }} ({{ $a->nip }})</option>
                        @endforeach
                      </select>
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-5">
                    <label>Kuota <span style="color: red"> *</span></label>
                    <input type="text" name="kuota" class="form-control" placeholder="Kuota" onkeypress="return hanyaAngka(event)" required>
                  </div>  
                  <div class="form-group col-7">
                    <label>Kelas Pagi/Malam <span style="color: red"> *</span></label>
                    <select name="pagiMalam" class="form-control">
                      @foreach (App\Jenis_kelas::orderBy('created_at','desc')->get() as $e)
                        <option value="{{ $e->id }}">{{ $e->nama }}</option>
                      @endforeach 
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  @if (count($cekDosen) == 0)
                    <a href="{{ route('management-dosens') }}" class="btn btn-primary btn-block">Register Dosen Baru</a>
                  @elseif (count($dosen) == 0)
                    <a href="{{ route('periodeDosenView') }}" class="btn btn-primary btn-block">Kembali</a>
                  @else                  
                    <button type="submit" class="btn btn-primary btn-block" name="submit">Register</button>
                  @endif                  
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- modal Delete --}}
  <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="removePeriodeDosen">
            {{ method_field('delete') }}
            {{ csrf_field() }}

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger"">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Dosen Aktif</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="updatePeriodeDosen">
                    {{ method_field('put') }}
                    @csrf
                    <div class="row">
                      <div class="form-group col-12">
                        <label>Nama <span style="color: red"> *</span></label>
                        <input type="text" value="" name="uNama" class="form-control" disabled>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-6">
                        <label>Kelas Pagi/Malam <span style="color: red"> *</span></label>
                        <select class="form-control uJenisKelas" name="uPagiMalam">
                          @foreach (App\Jenis_kelas::orderBy('created_at','desc')->get() as $e)
                            <option value="{{$e->id}}">{{$e->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group col-6">
                        <label>Kuota <span style="color: red"> *</span></label>
                        <input type="text" name="uKuota" class="form-control" onkeypress="return hanyaAngka(event)" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update</button>
                      </div>
                    </div>                   
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  @include('admin.form-change-password')  

@endsection

@section('scriptBottom')
    <script>
      $(document).ready( function () {
          $('#datatables').DataTable();
      });

      $(document).on('click','.deleteRecord',function(){
        $('#deleteModalRecord').modal('show');
        let userID=$(this).attr('data-userid');
        $(".removePeriodeDosen").attr('action', '{{ url('admin/periodeDosen/delete') }}'+'/'+userID);
      });

      $(document).on('click','.updateRecord',function(){
        $('#updateModalRecord').modal('show');

        function pasValue(count,attr,val){
          for (let i=0; i<=count; i++) {
            $('input[name='+attr[i]+']').val(val[i]);
          }
        }

        let user=$(this).data('user');

        let attr = ['uNama','uKuota'];
        let val  = [user[1],user[3]];

        pasValue(1,attr,val);
   
        $('.uJenisKelas').val(user[2]);

        // atur url tujuan
        $(".updatePeriodeDosen").attr('action', '{{ url('admin/periodeDosen/update') }}'+'/'+user[0]);
  
      });

      function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
      }


    </script>
@endsection