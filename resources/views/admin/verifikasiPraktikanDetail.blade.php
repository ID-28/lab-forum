@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection


@section('content')
    <div class="tab-content" id="myTabContent">
    {{-- Praktikan Verifikasi Detail --}}
        <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
            <div class="section-body">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Preview Detail Verifikasi Praktikan</h4>
                    </div>
                    <div class="card-body">
                        Mohon diteliti data dari praktikan : 
                        <ul>
                            <li>Formulir Pendaftaran Praktikum (1x)</li>
                            <li>Kwitansi Pembayaran Praktikum (1x)</li>
                            <li>Kwitansi Pembayaran Praktikum Fotokopi (1x)</li>
                            <li>Kartu Rencana Studi {{ $praktikan->namaPraktikum }} (1x)</li>
                            <li><b>Teliti data dibawah ini</b></li>
                        </ul>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td class="text-left">ID Registrasi</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ FormulirHelp::regId($praktikan->namaPraktikum,$praktikan->tahun,$praktikan->id) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Nama</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ $praktikan->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">NPM</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ $praktikan->npm }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Praktikum</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left"> {{ $praktikan->namaPraktikum }} {{ $praktikan->tahun }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Jadwal</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left"> {{ $praktikan->waktu }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kelas</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left"> {{ $praktikan->namaKelas }} ({{ ucfirst($praktikan->pagiMalam) }}) </td>
                                    </tr> 
                                    <tr>
                                        <td class="text-left">Foto</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">
                                            <img src="{{ url('upload/foto\\') . $praktikan->foto }}" alt="Foto Profile" width="400px" height="auto">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"></td>
                                        <td class="text-center"></td>
                                        <td class="text-left">
                                            @if($praktikan->status > 0)
                                                <span class="text-primary h3 font-weight-bold">Sudah diverifikasi</span>
                                            @else
                                                <a href="{{ route('verifikasiPraktikanDetailProses', $praktikan->id) }}" class="btn btn-action btn-primary">Verifikasi</a>
                                                <a href="{{ route('deleteRegistrasiPraktikan', $praktikan->id) }}" class="btn btn-action btn-danger">Hapus Registrasi</a>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('admin.form-change-password')    

@endsection