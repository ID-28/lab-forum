@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Aslab
    @endslot
    @slot('nav2')
      Register Aslab
    @endslot     
  @endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- previeww asleb --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Aslab</h4>
              </div>
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table table-striped" id="datatables">
                    <thead>
                      <tr>
                        <th>NPM</th>
                        <th>Nama Aslab </th>
                        <th>No Telepon</th>
                        <th>Jenis Kelamin</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($aslab as $a)
                      <tr>
                          <td>{{ $a->username }}</td>
                          <td>{{ $a->nama }}</td>
                          <td>{{ $a->no_tlpn }}</td>
                          <td>{{ $a->jenis_kelamin }}</td>
                          <td>
                            <a href="#" class="btn btn-action btn-primary mr-1 updateRecord" data-url="{{ route('manajemenAslabUpdate') }}" data-data='{"id":"{{$a->id}}" , "nama": "{{$a->nama}}", "npm": "{{$a->username}}", "no_tlpn":"{{$a->no_tlpn}}", "jenisKelamin": "{{$a->jenis_kelamin}}" }'>Update</a>
                            <a href="#" class="btn btn-action btn-danger mr-1 deleteRecord"  data-url="{{ route('manajemenAslabDelete') }}" data-id="{{$a->id}}">Delete</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- modal Delete --}}
    <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="remove-record-model">
            {{ method_field('delete') }}
            {{ csrf_field() }}
            <input type="hidden" name="id">

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger"">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Aslab</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="frist_name">Nama Lengkap</label>
                        <input id="frist_name" type="text" class="form-control" name="nama" autofocus>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="npm">NPM</label>
                        <input id="npm" type="text" class="form-control" name="npm">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-6" style="margin-bottom: 0">
                        <label for="password" class="d-block">Password</label>
                        <input id="password" type="password" class="form-control" name="password">
                      </div>
                      <div class="form-group col-6" style="margin-bottom: 0">
                        <label for="password2" class="d-block">Password Confirmation</label>
                        <input id="password2" type="password" class="form-control" name="password-confirm">
                      </div>
                      <div class="form-group col-12">
                        <span classs="col-6" style="font-weight: 300;font-size: 80%; color: red;">* kosongi kolom passsword jika tidak diubah</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label>No Telepon</label>
                        <input type="text" class="form-control" name="no_tlpn">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label>Jenis Kelamin</label>
                        <select name="jenisKelamin" class="form-control">
                          <option value="L">Laki - Laki</option>
                          <option value="P">Perempuan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update Record</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    {{-- asleb register --}}
    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <form method="POST" action="{{ route('manajemenAslabProses') }}">
              @csrf
              <div class="row">
                <div class="form-group col-12">
                  <label for="frist_name">Nama Lengkap</label>
                  <input id="frist_name" type="text" class="form-control" name="nama" autofocus>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label for="npm">NPM</label>
                  <input id="npm" type="text" class="form-control" name="npm">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-6">
                  <label for="password" class="d-block">Password</label>
                  <input id="password" type="password" class="form-control" name="password">
                </div>
                <div class="form-group col-6">
                  <label for="password2" class="d-block">Password Confirmation</label>
                  <input id="password2" type="password" class="form-control" name="password-confirm">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label>No Telepon</label>
                  <input type="text" class="form-control" name="no_tlpn">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label>Jenis Kelamin</label>
                  <select name="jenisKelamin" class="form-control">
                    <option value="L">Laki - Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" name="submit">
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.form-change-password')

@endsection

@section('scriptBottom')
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
        });
        {{-- Delete Modal Record --}}
          $(document).on('click','.deleteRecord',function(){
              $('#deleteModalRecord').modal('show');
              var userID=$(this).attr('data-id');
              var dataUrl=$(this).attr('data-url');
              $(".remove-record-model").attr('action', dataUrl['url']);
              $('.remove-record-model [name="id"]').attr('value', userID);
          });

          {{-- Update Modal Record --}}
          $(document).on('click','.updateRecord',function(){
              $('#updateModalRecord').modal('show');

              var ambilData=$(this).attr('data-data');
              var data = JSON.parse("["+ambilData.replace(/^\n+|\n+$/g, "").replace(/\n+/g, ",")+"]")[0];
              for(x in data){
                $('.update-record-model [name='+x+']').val(data[x]);
              }

              var dataUrl=$(this).attr('data-url');
              $(".update-record-model").attr('action', dataUrl['url']);
          });
    </script>
@endsection