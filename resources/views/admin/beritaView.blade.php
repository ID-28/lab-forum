@extends('layouts/master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection


@section('content')
    <div class="tab-content" id="myTabContent">
    {{-- Berita View Detail --}}
        <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
            <div class="section-body">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Preview Berita View</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td class="text-left">Judul</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ $berita->judul }}</td>
                                    </tr>
                                    @if($berita->gambar != null)
                                        <tr>
                                            <td class="text-left">Gambar</td>
                                            <td class="text-center">:</td>
                                            <td class="text-left">
                                                <img src="{{ url('upload/berita\\') . $berita->gambar }}" alt="Gambar berita" width="400px" height="auto">
                                            </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td class="text-left">Content</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{!! $berita->deskripsi !!}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"> </td>
                                        <td class="text-center"></td>
                                        <td class="text-left">
                                            <a href="{{ route('manajemenBeritaViewEdit',$berita->id) }}" class="btn btn-action btn-primary">Edit</a>
                                            <a href="{{ route('manajemenBeritaViewDelete',$berita->id) }}" class="btn btn-action btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('admin.form-change-password')    

@endsection