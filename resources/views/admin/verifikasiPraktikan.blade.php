@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection



@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- verifikasi praktikan --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4 class="float-left">Verifikasi Praktikan</h4>
                <a class="float-right btn btn-action btn-primary" href="{{ route('cetakAbsensi') }}">Unduh Form Absensi</a>
                <a class="float-right btn btn-action btn-primary" href="{{ route('cetakKartuPraktikumBelakang') }}">Unduh Kartu Belakang Praktikum</a>
                <a class="float-right btn btn-action btn-primary" href="{{ route('cetakKartuPraktikum') }}">Cetak Kartu Praktikum</a>
              </div>
              <div class="card-body">
                <table class="table table-striped" id="datatables">
                    <thead>
                        <tr>
                            <th>No. Reg</th>
                            <th>NPM</th>
                            <th>Nama</th>
                            <th>Praktikum</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($praktikan as $u)
                            <tr>
                                <td class="text-left">{{ FormulirHelp::regId($u->namaPraktikum,$u->tahun,$u->id) }}</td>
                                <td class="text-left">{{ $u->npm }}</td>
                                <td class="text-left">{{ $u->nama }}</td>
                                <td class="text-left">{{ $u->namaPraktikum }} {{ $u->tahun }}</td>
                                <td>
                                    <a href="{{ route('verifikasiPraktikanDetail', $u->id) }}" class="btn btn-action btn-{{ $u->status == 0?'primary':'secondary' }}">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.form-change-password')    

@endsection


@section('scriptBottom')
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
        });
    </script>
@endsection