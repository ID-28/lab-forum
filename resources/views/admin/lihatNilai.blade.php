@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- previeww dosen --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Cetak Nilai Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                    <table id="tabelListPraktikan" class="display table table-stripped" style="width:100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Dosen</th>
                          <th>Praktikum</th>
                          <th>Total Praktikan</th>
                          <th>Cetak</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                          $no=1;
                        @endphp
                        @foreach ($asdos as $e)
                          <tr>
                            <td>{{$no}}</td>
                            <td>{{$e->namaDosen}}</td>
                            <td>{{$e->namaPraktikum}}</td>
                            <td>{{$e->total}}</td>
                            <td>
                                <form action="{{ route('export_excel', ['nip' => $e->nip,'id'=>$e->idPraktikum]) }}" method="GET">
                                    @method('get')
                                    @csrf
                                    <button type="submit" class="btn btn-action btn-success" <?php if ($e->status == 0) { echo "disabled";}?>>Cetak</button>
                                </form>
                                <form action="{{ route('reject', ['id' => $e->idPeriodePraktikumDosen]) }}" method="POST">
                                    @method('put')
                                    @csrf
                                    <button type="submit" class="btn btn-action btn-danger" <?php if ($e->status == 0) { echo "disabled";}?>>Reject</button>
                                </form>
                            </td>
                          </tr>
                          @php
                            $no++;
                          @endphp               
                        @endforeach
                      </tbody>
                    </table><br>
                </div>
        
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>

  <script>
    $(document).ready(function() {
      $('#tabelDosen').DataTable();
    });
  </script>
  
  @include('admin.form-change-password')    

@endsection