<html style="padding: 0 !important; margin: 0 !important;">
    <head>
        <title>Kartu Praktikum</title>
        <style>
            *{
                font-family: 'Helvetica';
            }
        </style>
    </head>
    <body style="width: 100%; height: 100%; margin: 0 !important; padding: 0 !important;">
        @foreach($praktikan as $p)
            <table style="width: 100%;padding-top:3%;">
                <tr>
                    <td style="width: 25%; text-align:center;">
                        <img src="{{ public_path('') . '\asset\logoitats.png'}}" alt="logo ITATS" style="width: 50px;height:auto;">
                    </td>
                    <td colspan="2" style="width: 50%; text-align:center; line-height: 1;">
                        <span style="font-weight: bold; font-size: 12pt">Kartu Praktikum <br> {{ $praktikum->nama }}</span> <br>
                        <span style="font-size: 11pt">Tahun {{ $praktikum->tahun }}</span>
                    </td>
                    <td style="width: 25%;text-align:center;">
                        <img src="{{ public_path('') . '\asset\\'}}{{ $idLab }}.png" alt="logo Lab" style="width: 50px;height:auto;">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr size="3" color="#808080">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 30%; text-align:center;">
                                    <img src="{{ public_path(''). '\upload\foto\\' . $p->foto }}" alt="Foto Profile" style="width: 120px !important;height: 160px !important;">
                                </td>
                                <td style="width: 70%;font-size: 80%;">
                                    <table sytle="width:100%">
                                        <tr>
                                            <td><b>Nama</b></td>
                                            <td>:</td>
                                            <td>{{ $p->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>NPM</b></td>
                                            <td>:</td>
                                            <td>{{ $p-> npm }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Sesi</b></td>
                                            <td>:</td>
                                            <td>{{ $p->waktu }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Asisten Pembimbing :</b></td>
                                        </tr>
                                        <tr width="100%">
                                            <td colspan="3">{{ $p->namaAslab }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Dosen Pembimbing :</b></td>
                                        </tr>
                                        <tr sytle="width:100%">
                                            <td colspan="3" sytle="width:100%">{{ $p->namaDosen }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-left:4%">
                        <table border="1" cellpadding="0" cellspacing="0"  style="width: 92%; font-size: 60%;">
                            <tr style="width: 100%;text-align:center;font-weight: bold;">
                                <td>Modul 1</td>
                                <td>Modul 2</td>
                                <td>Modul 3</td>
                                <td>Modul 4</td>
                                <td>Modul 5</td>
                                <td>Modul 6</td>
                            </tr>
                            <tr>
                                <td> <br><br><br><br> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="font-size: 10pt;padding-left:20px; font-size:80%; color:red;" valign="bottom">
                        *Kartu praktikum harap dibawa saat mengikuti praktikum dan bimbingan. <br>
                        *Kartu praktikum harap disimpan.
                    </td>
                </tr>
            </table>
        @endforeach
    </body>
</html>