@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection



@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- manajemen praktikan --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Manajemen Praktikan</h4>
              </div>
              <div class="card-body">
                <table class="table table-striped" id="datatables">
                    <thead>
                        <tr>
                            <th>NPM</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $u)
                            <tr>
                                <td>{{ $u->npm }}</td>
                                <td>{{ $u->nama }}</td>
                                <td>
                                    <a href="{{ route('praktikanViewDetail', $u->userId) }}" class="btn btn-action btn-primary">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.form-change-password')    

@endsection


@section('scriptBottom')
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
        });
    </script>
@endsection