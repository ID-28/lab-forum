@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Periode Praktikum
    @endslot
    @slot('nav2')
      Atur Periode Praktikum
    @endslot     
  @endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Periode Praktikum</h4>
              </div>
              <div class="card-body ">
                <div class="table-responsive">
                  <table class="table table-striped" id="datatables">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Praktikum</th>
                        <th>Periode </th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                      @endphp
                      @foreach($periodePraktikum as $p)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $p->nama }}</td>
                            <td>{{ $p->tahun }}</td>
                            <td>
                                @if($p->status == 0)
                                    <span class="text-secondary">Tidak Aktif</span>
                                @else
                                    <span class="text-primary">Aktif</span>
                                @endif
                            </td>
                            <td>
                                @if($p->status == 0)
                                  <a href="{{ route('aktifkanPeriodePraktikum',$p->id) }}" class="btn btn-action btn-primary">Aktifkan</a>
                                @else
                                  <a href="#" class="btn btn-action btn-primary disabled">Aktifkan</a>
                                @endif
                            </td>
                            @php
                              $i++;
                            @endphp
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Atur Periode Praktikum</h4>
              </div> 
              <div class="card-body"> 
                <form method="POST" action="{{ route('periodePraktikumProses') }}">
                  @csrf
                  <div class="row">
                    <div class="form-group col-6">
                      <label>Praktikum <span style="color: red"> *</span></label>
                      <select name="praktikum" class="form-control">
                        @foreach($praktikum as $p)
                            <option value="{{ $p->id }}">{{ $p->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-6">
                      <label for="tahun">Tahun <span style="color: red"> *</span></label>
                      <select name="tahun" class="form-control">
                        @for($i = 2019; $i<=2030; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" name="submit">
                      Register
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.form-change-password')         

@endsection

@section('scriptBottom')
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
        });
    </script>
@endsection