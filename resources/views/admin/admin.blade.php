@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('content')
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">

        {{-- {{dd(App\Materi::with(['praktikum.periode_praktikum' => function($e){
          $e->where('periode_praktikum.status',1);
        }])->get())}} --}}
        {{-- {{App\Materi::find(1)->praktikum->find(1)->periode_praktikum}} --}}
        
        <h4 class="section-title">Informasi Praktikum {{$praktikum->nama}} {{$praktikum->tahun}} </h4><br>
        <div class="row">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-primary">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  {{count($periodeDosen)}}
                </div>
                <div class="card-header pb-0">
                  <h4>Dosen Aktif</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('periodeDosenView') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-danger">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                    {{count($periodeAslab)}}
                </div>
                <div class="card-header pb-0">
                  <h4>Aslab Aktif</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('periodeAslabView') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-warning">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  0
                </div>
                <div class="card-header pb-0">
                  <h4>Total Praktikan</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="#">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-primary">
                <i class="ion ion-clock"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  0
                </div>
                <div class="card-header pb-0">
                  <h4>Total Sesi Praktikum</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="#">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-success">
                <i class="ion ion-android-folder"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  {{count($modulDosen)}}
                </div>
                <div class="card-header pb-0">
                  <h4>Total Modul Dosen</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('management_modul_dosen') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-warning">
                <i class="ion ion-android-folder"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  {{count($modulAslab)}}
                </div>
                <div class="card-header pb-0">
                  <h4>Total Modul Aslab</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('management_modul_aslab') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-primary">
                <i class="ion ion-clipboard"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  {{count($materi)}}
                </div>
                <div class="card-header pb-0">
                  <h4>Total Kumpulan Materi</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('management-materis') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  @include('admin.form-change-password')    
@endsection



