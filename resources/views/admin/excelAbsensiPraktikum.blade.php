<table>
    <tbody>
        <tr>
            <td colspan="{{ $jumlahModul+4 }}" style="text-align:center; font-size:16px"><h1><b>Absensi Praktikum {{ $namaPraktikum }} - {{ $tahunPraktikum }}</b></h1></td>
        </tr>
        <tr>
            <td colspan="{{ $jumlahModul+4 }}"> <br> </td>
        </tr>
        <tr>
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">No</td>
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">NPM</td>
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">Nama</td>
            @for($i=1;$i<=$jumlahModul;$i++)
                <td style="text-align:center;font-weight:bold;border:1px solid $000;">Modul {{ $i }}</td>
            @endfor
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">TA</td>
        </tr>
        @php $no = 1 @endphp
        @foreach($praktikan as $p)
            <tr>
                <td style="text-align:center;vertical-align:middle;border:1px solid $000;">{{ $no }}</td>
                <td style="border:1px solid $000;">{{ $p->npm }}</td>
                <td style="border:1px solid $000;">{{ $p->nama }}</td>
                @for($i=1;$i<=$jumlahModul+1;$i++)
                    <td style="border:1px solid $000;"></td>
                @endfor
            </tr>
            @php $no++ @endphp
        @endforeach
    </tbody>
</table>