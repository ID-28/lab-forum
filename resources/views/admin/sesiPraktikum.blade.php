@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Sesi Praktikum
    @endslot
    @slot('nav2')
      Atur Sesi Praktikum
    @endslot     
  @endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Sesi Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="datatables">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Praktikum</th>
                        <th>Periode </th>
                        <th>Nama Sesi</th>
                        <th>Waktu</th>
                        <th>Kuota</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                      @endphp
                      @foreach($sesi as $s)
                        <tr>
                          <td>{{ $i }}</td>
                          <td>{{ $s->namaPraktikum }}</td>
                          <td>{{ $s->tahun }}</td>
                          <td>{{ $s->nama }}</td>
                          <td>{{ $s->waktu }}</td>
                          <td>{{ $s->kuota }}</td>
                          <td>
                            <a href="#" class="btn btn-action btn-primary mr-1 updateRecord" data-url="{{ route('sesiPraktikumUpdate') }}" data-data='{"id":"{{$s->id}}" , "namaSesi": "{{$s->nama}}", "waktu": "{{$s->waktu}}", "kuota":"{{$s->kuota}}"}'>Update</a>
                            <a href="#" class="btn btn-action btn-danger mr-1 deleteRecord"  data-url="{{ route('sesiPraktikumDelete') }}" data-id="{{$s->id}}">Delete</a>
                          </td>
                          @php
                            $i++;
                          @endphp
                        </tr>
                      @endforeach
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- modal Delete --}}
    <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="remove-record-model">
            {{ method_field('delete') }}
            {{ csrf_field() }}
            <input type="hidden" name="id">

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Sesi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="namaSesi">Nama Sesi <span style="color: red"> *</span></label>
                        <input type="text" name="namaSesi" class="form-control" placeholder="Nama Sesi">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-5">
                        <label for="waktu">Waktu <span style="color: red"> *</span></label>
                        <input type="text" name="waktu" class="form-control" placeholder="Waktu">
                      </div>
                      <div class="form-group col-7">
                        <label for="kuota">Kuota <span style="color: red"> *</span></label>
                        <input type="text" name="kuota" class="form-control" placeholder="Kuota" onkeypress="return hanyaAngka(event)">
                      </div>  
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update Record</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Atur Sesi Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>
              <div class="card-body">
                <form method="POST" action="{{ route('sesiPraktikumProses') }}">
                  @csrf
                  <div class="row">
                    <div class="form-group col-5">
                      <label>Praktikum <span style="color: red"> *</span></label>
                      <input type="hidden" name="idPraktikum" value="{{ $praktikum->id }}">
                      <input type="text" class="form-control" value="{{ $praktikum->nama }} {{ $praktikum->tahun }}" disabled>
                    </div>
                    <div class="form-group col-7">
                      <label for="namaSesi">Nama Sesi <span style="color: red"> *</span></label>
                      <input type="text" name="namaSesi" class="form-control" placeholder="Nama Sesi">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-5">
                      <label for="waktu">Waktu <span style="color: red"> *</span></label>
                      <input type="text" name="waktu" class="form-control" placeholder="Waktu">
                    </div>
                    <div class="form-group col-7">
                      <label for="kuota">Kuota <span style="color: red"> *</span></label>
                      <input type="text" name="kuota" class="form-control" placeholder="Kuota" onkeypress="return hanyaAngka(event)">
                    </div>  
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" name="submit">
                      Register
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
  @include('admin.form-change-password')       

@endsection

@section('scriptBottom')
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
        });

        {{-- Delete Modal Record --}}
          $(document).on('click','.deleteRecord',function(){
              $('#deleteModalRecord').modal('show');
              var userID=$(this).attr('data-id');
              var dataUrl=$(this).attr('data-url');
              $(".remove-record-model").attr('action', dataUrl['url']);
              $('.remove-record-model [name="id"]').attr('value', userID);
          });

          {{-- Update Modal Record --}}
          $(document).on('click','.updateRecord',function(){
              $('#updateModalRecord').modal('show');

              var ambilData=$(this).attr('data-data');
              var data = JSON.parse("["+ambilData.replace(/^\n+|\n+$/g, "").replace(/\n+/g, ",")+"]")[0];
              for(x in data){
                $('.update-record-model [name='+x+']').val(data[x]);
              }

              var dataUrl=$(this).attr('data-url');
              $(".update-record-model").attr('action', dataUrl['url']);
          });

        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))

              return false;
          return true;
        }
    </script>
@endsection