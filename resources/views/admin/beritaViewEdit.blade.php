@extends('layouts/master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection


@section('content')
    <div class="tab-content" id="myTabContent">
    {{-- Berita View Detail --}}
        <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
            <div class="section-body">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Preview Berita View</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <form method="POST" action="{{ route('manajemenBeritaViewEditProses') }}" enctype='multipart/form-data'>
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $berita->id }}">
                                        <tr>
                                            <td class="text-left">Judul</td>
                                            <td class="text-center">:</td>
                                            <td class="text-left">
                                                <input id="judul" type="text" class="form-control" name="judul" value="{{ $berita->judul }}" autofocus required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Gambar</td>
                                            <td class="text-center">:</td>
                                            <td class="text-left">
                                                @if($berita->gambar != null)
                                                    <img src="{{ url('upload/berita\\') . $berita->gambar }}" alt="Gambar berita" width="400px" height="auto" style="margin-bottom: 20px;">
                                                @endif
                                                <input type="file" id="gambar" class="form-control" name="gambar">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Content</td>
                                            <td class="text-center">:</td>
                                            <td class="text-left"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left" colspan="3">
                                                <textarea name="content" id="content" cols="30" rows="10" class="form-control" required>{!! $berita->deskripsi !!}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"></td>
                                            <td class="text-center"></td>
                                            <td class="text-left"> 
                                                <button type="submit" class="btn btn-primary btn-block" name="submit">Submit</button>
                                            </td>
                                        </tr>
                                    </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('admin.form-change-password')    

@endsection

@section('scriptBottom')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script>
        $(document).ready( function () {
            $('#content').summernote({
              height: 300,
              width: 750,
              popover: {
                image: [],
                link: [],
                air: []
              }
            });
        });
    </script>
@endsection