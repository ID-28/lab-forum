@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')
    @slot('nav1')
      Preview Modul Dosen
    @endslot
    @slot('nav2')
      Register Modul Dosen
    @endslot
	@endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- previeww Modul --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Modul Dosen Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>    
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabelModul" class="display table table-stripped" style="width:100%">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Praktikum</th>
                          <th>Nama Modul</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                      @endphp
                        @foreach ($modul_dosen as $e)
                          <tr>
                            <td>{{$i}}</td>
                            <td>{{$e->namaPraktikum}} ({{$e->tahun}})</td>
                            <td>{{$e->namaModul}}</td>
                            <td>
                                <button class="btn btn-primary btn-action mr-1 updateRecord" data-user='["{{$e->id}}","{{$e->namaModul}}"]' ><i class="ion ion-edit"></i></button>
                                <button class="btn btn-danger btn-action mr-1 deleteRecord"  data-userid="{{$e->id}}"><i class="ion ion-trash-b"></i></button>
                            </td>
                            @php
                              $i++;
                            @endphp
                          </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- modal Delete --}}
    <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="remove-record-model">
            {{ method_field('delete') }}
            {{ csrf_field() }}

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger"">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Modul Dosen</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}
                    <div class="row">
                      <div class="form-group col-12">
                          <label>Praktikum <span style="color: red">*</span></label>
                          <input type="hidden" name="upraktikum" value="{{ $praktikum->id }}">
                          <input type="text" value="{{ $praktikum->nama }} {{ $praktikum->tahun }}" class="form-control" disabled>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="nama_dosen">Nama Modul <span style="color: red">*</span></label>
                        <input  type="text" class="form-control" name="unama" autofocus required>
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" name="agree" class="custom-control-input" id="agree" required>
                          <label class="custom-control-label" for="agree">I agree with the terms and conditions</label>
                        </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Register Modul --}}
    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">    
              <div class="card-header">
                <h4>Register Modul Dosen Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>    
              <div class="card-body">
                <form method="POST" action="{{ route('modul_store_dosen') }}">
                  {{csrf_field()}}
                  <div class="row">
                    <div class="form-group col-6">
                      <label>Praktikum <span style="color: red">*</span></label>
                      <input type="hidden" name="praktikum" value="{{ $praktikum->id }}">
                      <input type="text" value="{{ $praktikum->nama }} {{ $praktikum->tahun }}" class="form-control" disabled>
                    </div>
                    <div class="form-group col-6">
                      <label>Jumlah Modul <span style="color: red"> *</span></label>
                      <select class="form-control" name="jumlah" >
                        @for ($i = 1; $i <=6; $i++)
                          <option value='{{$i}}' >{{$i}} Modul</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="agre" class="custom-control-input" id="agre" required>
                      <label class="custom-control-label" for="agre">I agree with the terms and conditions</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" name="submit">
                      Register
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <script>
    $(document).ready(function() {
      $('#tabelModul').DataTable();
    });

    // Delete Modal Record
    $(document).on('click','.deleteRecord',function(){
        $('#deleteModalRecord').modal('show');
        let userID=$(this).attr('data-userid');
        $(".remove-record-model").attr('action', '{{ url('admin/management-modul-dosen/delete') }}'+'/'+userID);
    });

    // Update Modal Record
    $(document).on('click','.updateRecord',function(){
        $('#updateModalRecord').modal('show');
        let user=$(this).data('user');
        $('input[name=unama]').val(user[1]);
        $(".update-record-model").attr('action', '{{ url('admin/management-modul-dosen/update') }}'+'/'+user[0]);
    });
  </script>
  @include('admin.form-change-password')    

@endsection