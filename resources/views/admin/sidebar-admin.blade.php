@component('layouts.sidebar' )
    @slot('menu')
        <li class="menu-header">Dashboard</li>
        <li  class="{{ Request::is('admin') ? 'active' : ''}}">
            <a href="{{ route('admin') }}"><i class="ion ion-ios-home"></i><span>Dashboard</span></a>
        </li>
        <li>
            <a href="#" class="has-dropdown"><i class="ion ion-android-contact"></i><span>Management User</span></a>
            <ul class="menu-dropdown">
                <li class="{{ Request::is('admin/management-dosens') || Request::is('admin/management-dosens/*') ? 'active' : ''}}">
                    <a href="{{ route('management-dosens') }}"><i class="ion ion-ios-circle-outline"></i>Dosen</a>
                </li>
                <li class="{{ Request::is('admin/manajemenAslab') || Request::is('admin/manajemenAslab/*') ? 'active' : ''}}" >
                    <a href="{{ route('manajemenAslab') }}"><i class="ion ion-ios-circle-outline"></i>Aslab</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="has-dropdown"><i class="ion ion-levels"></i><span>Management Praktikum</span></a>
            <ul class="menu-dropdown">
                <li class="#">
                    <a href="{{ route('periodePraktikum') }}"><i class="ion ion-ios-circle-outline"></i>Praktikum</a>
                </li>
                <li class="#">
                    <a href="{{ route('sesiPraktikumView') }}"><i class="ion ion-ios-circle-outline"></i>Sesi Praktikum</a>
                </li>
                <li class="#">
                    <a href="{{ route('periodeAslabView') }}"><i class="ion ion-ios-circle-outline"></i>Aslab Aktif</a>
                </li>
                <li class="#">
                    <a href="{{ route('periodeDosenView') }}"><i class="ion ion-ios-circle-outline"></i>Dosen Aktif</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="has-dropdown"><i class="ion ion-ios-folder"></i><span>Management Modul</span></a>
            <ul class="menu-dropdown">
                <li class="{{ Request::is('admin/management-modul-dosen') || Request::is('admin/management-modul-dosen/*') ? 'active' : ''}}">
                    <a href="{{ route('management_modul_dosen') }}"><i class="ion ion-ios-circle-outline"></i>Modul Dosen</a>
                </li>
                <li class="{{ Request::is('admin/management-modul-aslab') || Request::is('admin/management-modul-aslab/*') ? 'active' : ''}}">
                    <a href="{{ route('management_modul_aslab') }}"><i class="ion ion-ios-circle-outline"></i>Modul Aslab</a>
                </li>
            </ul>
        </li>
        <li class="{{ Request::is('admin/management-materis') || Request::is('admin/management-materis/*') ? 'active' : ''}}" >
            <a href="{{ route('management-materis') }}"><i class="ion ion-ios-list"></i><span>Management Materi</span></a>
        </li>
        <li>
            <a href="#" class="has-dropdown"><i class="ion ion-ios-calculator"></i><span>Management Penilaian</span></a>
            <ul class="menu-dropdown">
                <li class="{{ Request::is('admin/management-penilaian-dosen') || Request::is('admin/management-penilaian-dosen/*') ? 'active' : ''}}">
                    <a href="{{ route('management-penilaian-dosen') }}"><i class="ion ion-ios-circle-outline"></i>Dosen</a>
                </li>
                <li class="{{ Request::is('admin/management-penilaian-aslab') || Request::is('admin/management-penilaian-aslab/*') ? 'active' : ''}}">
                    <a href="{{ route('management-penilaian-aslab') }}"><i class="ion ion-ios-circle-outline"></i>Aslab</a>
                </li>
            </ul>
        </li>
        
        <li class="{{ Request::is('admin/verifikasi-praktikan') || Request::is('admin/verifikasi-praktikan/*') ? 'active' : ''}}" >
            <a href="{{ route('verifikasiPraktikan') }}"><i class="ion-ios-toggle"></i><span>Verifikasi Praktikan</span></a>
        </li>
        <li class="{{ Request::is('admin/praktikan') || Request::is('admin/praktikan/*') ? 'active' : ''}}" >
            <a href="{{ route('praktikanview') }}"><i class="ion-android-person"></i><span>Praktikan</span></a>
        </li>
        <li class="{{ Request::is('admin/berita') || Request::is('admin/berita*') ? 'active' : ''}}" >
            <a href="{{ route('manajemenBerita') }}"><i class="ion ion-clipboard"></i><span>Berita</span></a>
        </li>
        <li class="{{ Request::is('admin/lihatNilai') || Request::is('admin/lihatNilai/*') ? 'active' : ''}}" >
            <a href="{{ route('lihatNilai') }}"><i class="ion-document-text"></i><span>Lihat Nilai</span></a>
        </li>
    @endslot
@endcomponent