<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kartu Praktikum</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
        .tabelinfo *{
            font-size: 12pt;
        }
        .content{
            margin-left: 15px;
        }
        .headerinfo *{
            font-size: 12pt;
        }
    </style>

</head>
<body>
    @foreach($praktikan as $p)
        <div class="information">
            <table width="100%" style="border-bottom:2px solid #808080;">
                <tr>
                    <td align="left" style="width: 25%;">
                        <img src="{{ public_path('') . '\asset\logoitats.png'}}" alt="logo ITATS" style="width: 50px;height:auto;">
                    </td>
                    <td align="center" class="headerinfo">
                            <span style="font-weight: bold;">Kartu Praktikum</span> <br>
                            {{ $praktikum->nama }} - {{ $praktikum->tahun }} <br>
                            Laboratorium {{ $lab->nama }}
                    </td>
                    <td align="right" style="width: 25%;">
                        <img src="{{ public_path('') . '\asset\\'}}{{ $idLab }}.png" alt="logo Lab" style="width: 50px;height:auto;">
                    </td>
                </tr>
            </table>
        </div>

        <br/>

        <div class="content">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="width: 25%">
                            <img src="{{ public_path(''). '\upload\foto\\' . $p->foto }}" alt="Foto Profile" style="width: 120px; height: 160px;">
                        </td>
                        <td style="width: 75%">
                            <table class="tabelinfo" sytle="width:100%">
                                <tr>
                                    <td><b>Nama</b></td>
                                    <td>:</td>
                                    <td>{{ $p->nama }}</td>
                                </tr>
                                <tr>
                                    <td><b>NPM</b></td>
                                    <td>:</td>
                                    <td>{{ $p-> npm }}</td>
                                </tr>
                                <tr>
                                    <td><b>Sesi</b></td>
                                    <td>:</td>
                                    <td>{{ $p->waktu }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><b>Asisten Pembimbing :</b></td>
                                </tr>
                                <tr width="100%">
                                    <td colspan="3">{{ $p->namaAslab }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><b>Dosen Pembimbing :</b></td>
                                </tr>
                                <tr sytle="width:100%">
                                    <td colspan="3" sytle="width:100%">{{ $p->namaDosen }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="content">
            <table border="1" cellpadding="0" cellspacing="0" style="width: 97%">
                <tr style="width: 100%;text-align:center;font-weight: bold;">
                    @for($i=1;$i<=count($materiAslab);$i++)
                        <td>Modul {{ $i }}</td>
                    @endfor
                </tr>
                <tr>
                    @for($i=1;$i<=count($materiAslab);$i++)
                        <td> <br><br><br><br> </td>
                    @endfor                
                </tr>
            </table>
        </div>

        <div class="information" style="position: absolute; bottom: 0;">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 10%;">
                    </td>
                    <td align="right" style="width: 90%; font-size: 10px;">
                        *Jika kartu ini hilang, Mohon menyerahkan ke Laboratorium {{ $lab->nama }}, {{ $lab->ruangan }}
                    </td>
                </tr>

            </table>
        </div>
    @endforeach
</body>
</html>