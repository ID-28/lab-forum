<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kartu Praktikum Belakang</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .content{
            margin: 15px;
        }
        .headerinfo *{
            font-size: 12pt;
        }
        .header *{
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
        }
        .header > th {
            padding: 10px auto;
        }

        table tr td {
            line-height: 1.034;
        }
    </style>

</head>
<body>
    <div class="content">
        <table border="1" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr class="header">
                <th rowspan="2" >Tanggal</th>
                <th rowspan="2">Modul</th>
                <th colspan="2" >Asisten</th>
                <th rowspan="2">Dosen</th>
            </tr>
            <tr class="header">
                <th>Pendahuluan</th>
                <th>Praktikum</th>
            </tr>
            @for($i=1;$i<=$jumlahTerbanyak;$i++)
                <tr>
                    <td> <br> <br> <br> </td>
                    <td align="center" style="font-size: 13px;font-weight: bold">{{ $i }}</td>
                    <td></td>
                    <td></td>
                    @if($setengah==true)
                        @if($i % 2 !=0)
                            <td rowspan="2"></td>
                        @endif
                    @else
                        <td></td>
                    @endif
                </tr>
            @endfor
            <tr>
                <td> <br> <br> <br> </td>
                <td align="center" style="font-size: 13px;font-weight: bold">TA</td>
                <td colspan="3"></td>
            </tr>
        </table>
    </div>
</body>
</html>