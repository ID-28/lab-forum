<div class="card">
  <div class="card-body">
    <ul class="nav nav-pills" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="menu" data-toggle="tab" href="#menu1" role="tab" aria-controls="menu" aria-selected="true">{{$nav1}}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="menu" data-toggle="tab" href="#menu2" role="tab" aria-controls="menu" aria-selected="false">{{$nav2}}</a>
      </li>
    </ul>
  </div>
</div>  