<table>
  <tbody>
      <tr>
          <td colspan="16" style="text-align:center; font-size:16px"><h1><b>Rekap Nilai Praktikum {{$praktikum->namaPraktikum}} Periode {{$praktikum->tahun}}  </b></h1></td>
      </tr>      
      <tr>
        <td colspan="16" style="text-align:center; font-size:16px"><h1><b>{{$praktikan[0]->namaDosen}}</b></h1></td>
      </tr>      
      <tr>
        <td colspan="1" style="text-align:center; font-size:16px"><b>No</b></td>
        <td colspan="5" style="text-align:center; font-size:16px"><b>Nama Praktikan</b></td>
        <td colspan="2" style="text-align:center; font-size:16px"><b>Nama Sesi</b></td>
        <td colspan="3" style="text-align:center; font-size:16px"><b>Nama Waktu</b></td>
        @for ($i=1; $i<=$banyakModul[0]->hitung ; $i++)
          <td colspan="1" style="text-align:center; font-size:16px"><b>Nilai {{$i}}</b></td>
        @endfor
        <td colspan="2" style="text-align:center; font-size:16px"><b>Nilai Akhir</b></td>
      </tr>
      @php $no = 1 @endphp
      @foreach($praktikan as $p)
        <tr>
            <td style="text-align:center;vertical-align:middle;">{{ $no }}</td>
            <td colspan="5" style="text-align:center;">{{ $p->namaMahasiswa }}</td>
            <td colspan="2" style="text-align:center;">{{ $p->namaSesi }}</td>
            <td colspan="3" style="text-align:center;"> {{ $p->waktu }}</td>
            @for ($i=1; $i<=$banyakModul[0]->hitung; $i++)
              <td style="text-align:center; font-size:16px">
                {{number_format($avg[$no-1][$i-1],2) }}
              </td> 
            @endfor
            <td colspan="2" style="text-align:center; font-size:16px">
              {{number_format($p->nilaiAkhir,2) }}
            </td>
        </tr>
        @php $no++ @endphp
      @endforeach --}}

  
      {{-- <tr>
          <td colspan="5" style="text-align:center; font-size:16px"><h1><b>Absensi Praktikum {{ $sesi->nama }} - {{ $sesi->tahun }}</b></h1></td>
      </tr>
      <tr>
          <td colspan="5" style="text-align:center; font-size:13px">{{ $sesi->namaSesi }} - {{ $sesi->waktu }}</td>
      </tr>
      <tr>
          <td colspan="5"> <br> </td>
      </tr>
      <tr>
          <td style="text-align:center;font-weight:bold">No</td>
          <td style="text-align:center;font-weight:bold">NPM</td>
          <td style="text-align:center;font-weight:bold">Nama</td>
          <td style="text-align:center;font-weight:bold" colspan="2">TTD</td>
      </tr>
      @php $no = 1 @endphp
      @foreach($praktikan as $p)
          <tr>
              <td style="text-align:center;vertical-align:middle;">{{ $no }}</td>
              <td style="">{{ $p->npm }}</td>
              <td style="">{{ $p->nama }}</td>
              @if($no % 2 != 0)    
                  <td style="">{{ $no }}............</td>
                  <td style=""></td>
              @else
                  <td style=""></td>
                  <td style="">{{ $no }}............</td>
              @endif
          </tr>
          @php $no++ @endphp
      @endforeach --}}
  </tbody>
</table>