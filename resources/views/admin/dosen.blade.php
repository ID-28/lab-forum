@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')
    @slot('nav1')
      Preview Dosen
    @endslot
    @slot('nav2')
      Register Dosen
    @endslot
	@endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">
    {{-- previeww dosen --}}
    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Dosen</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabelDosen" class="display table table-stripped" style="width:100%">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>NIP</th>
                          <th>Nama Dosen </th>
                          <th>No Telepon</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                          $i = 1;
                      @endphp
                        @foreach (App\Dosen::orderBy('created_at','desc')->get() as $e)
                          <tr>
                            <td>{{$i}}</td>
                            <td>{{$e->nip}}</td>
                            <td>{{$e->nama}}</td>
                            <td>{{$e->no_tlpn}}</td>
                              <td>
                              <button class="btn btn-primary btn-action mr-1 updateRecord" data-user='["{{$e->id}}" , "{{$e->nama}}","{{$e->password}}" ,"{{$e->no_tlpn}}","{{$e->nip}}"]'><i class="ion ion-edit"></i></button>
                              <button class="btn btn-danger btn-action mr-1 deleteRecord"  data-userid="{{$e->id}}"><i class="ion ion-trash-b"></i></button>
                            </td>
                            @php
                                $i++;
                            @endphp
                          </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- modal Delete --}}
    <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="remove-record-model">
            {{ method_field('delete') }}
            {{ csrf_field() }}

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger"">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Dosen</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="nama_dosen">Nama Lengkap <span style="color: red">*</span></label>
                        <input  type="text" class="form-control" name="unama_dosen" autofocus required>
                      </div>
                    </div>  
                    <div class="row">
                      <div class="form-group col-6">
                        <label>NIP <span style="color: red">*</span></label>
                        <input type="text" class="form-control" name="unip"  onkeypress="return hanyaAngka(event)" autofocus required>
                      </div>
                      <div class="form-group col-6">
                        <label for="no_tlpn" class="d-block">No Telefon <span style="color: red">*</span></label>
                        <input type="text" class="form-control" name="uno_tlpn" id="no_tlpn" autofocus required>
                      </div>    
                    </div>
                    <div class="row">
                      <div class="form-group col-6" style="margin-bottom: 0">
                        <label for="password" class="d-block">Password <span style="color: red">*</span></label>
                        <input id="password" type="password" class="form-control" name="password">
                      </div>
                      <div class="form-group col-6" style="margin-bottom: 0">
                        <label for="password2" class="d-block">Password Confirmation <span style="color: red">*</span></label>
                        <input id="password2" type="password" class="form-control" name="password-confirm">
                      </div>
                      <div class="form-group col-12">
                        <span classs="col-6" style="font-weight: 300;font-size: 80%; color: red;">* kosongi Kolom Password Jika Tidak Diubah, Min 6 Characters</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="agree" class="custom-control-input" id="agree" required>
                        <label class="custom-control-label" for="agree" >I agree with the terms and conditions</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update Record</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- register dosen --}}
    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <form method="POST" action="{{ route('dosen_store') }}">
              {{csrf_field()}}
              <input type="hidden" name="jenis_user" value="3">
              <div class="row">
                <div class="form-group col-6">
                  <label for="frist_name">NIP <span style="color: red">*</span></label>
                  <input id="frist_name" type="text" class="form-control" name="nip"  onkeypress="return hanyaAngka(event)" autofocus="autofocus" tabindex="0" required>
                </div>
                <div class="form-group col-6">
                  <label for="frist_name">Nama Lengkap <span style="color: red">*</span></label>
                  <input id="frist_name" type="text" class="form-control" name="nama_dosen" autofocus required>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-6">
                  <label for="password" class="d-block">Password <span style="color: red">*</span></label>
                  <input id="password" type="password" class="form-control" name="password" required>
                  <label style="color: red"><span style="color: red">*</span>Password Minimum 6 Characters</label>
                </div>
                <div class="form-group col-6">
                  <label for="password2" class="d-block">Password Confirmation <span style="color: red">*</span></label>
                  <input id="password2" type="password" class="form-control" name="password_confirm" required>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-6">
                  <label>No Telepon <span style="color: red">*</span></label>
                  <input type="text" class="form-control" name="no_tlpn"  onkeypress="return hanyaAngka(event)" required>
                </div>
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="agreee" class="custom-control-input" id="agreee" required>
                  <label class="custom-control-label" for="agreee" >I agree with the terms and conditions</label>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" name="submit">
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  {{-- <script src="{{ asset('js/jquery.mask.min.js') }}"></script> --}}
  <script>
    $(document).ready(function() {
      $('#tabelDosen').DataTable();
    });

       function hanyaHuruf(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)

            return false;
           return true;
       }
       function hanyaAngka(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

               return false;
           return true;
       }
  // //Format Phone
  //         $('#no_tlpn').mask('Z', {
  //           translation: {
  //               'Z': {
  //                   pattern: /[a-zA-Z\s]/,
  //                   recursive: true
  //               }
  //           }
  //       });

  //       $('#no_tlpn').mask('0000 0000 0000#');

    {{-- Delete Modal Record --}}
    $(document).on('click','.deleteRecord',function(){
        $('#deleteModalRecord').modal('show');
        let userID=$(this).attr('data-userid');
        $(".remove-record-model").attr('action', '{{ url('admin/management-dosens/delete') }}'+'/'+userID);
    });

    {{-- Update Modal Record --}}
    $(document).on('click','.updateRecord',function(){
        $('#updateModalRecord').modal('show');

        function pasValue(count,attr,val){
          for (let i=0; i<=count; i++) {
            $('input[name='+attr[i]+']').val(val[i]);
          }
        }

        let user=$(this).data('user');

        let attr = ['unama_dosen' , 'uno_tlpn', 'unip'];
        let val  = [user[1],user[3],user[4]];

        pasValue(3,attr,val);

        // jenis Kelas update mantul
        // $('.ujeniskelas').val(user[5]);

        // laboratorium update mantul
        // $('.ulaboratorium').val(user[4]);

        // atur url tujuan
        $(".update-record-model").attr('action', '{{ url('admin/management-dosens/update') }}'+'/'+user[0]);
    });
  </script>
  
  @include('admin.form-change-password')    

@endsection