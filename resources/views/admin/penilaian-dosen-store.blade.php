@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Send Materi
    @endslot
    @slot('nav2')
    @endslot
  @endcomponent
@endsection

@section('content')

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
            <div class="section-body">
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="card-header">
                            <h4>Daftar Penilaian Dan Materi</h4>
                        </div>
                        <form method="POST" action="{{ url('admin/management-penilaian-dosen/create/store') }}">
                            {{ method_field('post')}}
                            {{csrf_field()}}
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Pilih Penilaian</label>
                                    <select class="form-control" name="modul">
                                        @foreach (App\Modul::where([['praktikum_id',$idPraktikum],['status',0]])->get() as $e)
                                            <option value='{{$e->id}}' >{{$e->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- materi --}}
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Pilih Materi</label>
                                </div>
                                @foreach (App\Materi::where([['praktikum_id',$idPraktikum],['status',0]])->get() as $e)
                                    <div class="form-group col-6">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="materi[]" value="{{$e->id}}" class="custom-control-input" id="materi{{$e->id}}" >
                                            <label class="custom-control-label" for="materi{{$e->id}}">{{$e->nama}}</label>
                                        </div>
                                    </div>
                                @endforeach      
                            </div>
                            <div class="form-group">
                                <div class="modal-footer">
                                    <a href="{{ route('management-penilaian-dosen') }}" class="btn btn-success">Back&nbsp;&nbsp;<i class="fa fa-arrow-circle-left"></i></a>
                                    <button type="submit" class="btn btn-primary" name="submit">Input&nbsp;&nbsp;<i class="fa fa-check-square"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.form-change-password')    

@endsection



