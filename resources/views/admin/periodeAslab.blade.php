@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Asisten Lab Aktif
    @endslot
    @slot('nav2')
      Atur Asisten Lab Aktif
    @endslot     
  @endcomponent
@endsection

@section('content')
  <div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Preview Asisten Lab Aktif Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>
              <div class="card-body ">
                <div class="table-responsive">
                  <table class="table table-striped" id="datatables">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NPM</th>
                        <th>Nama </th>
                        <th>Praktikum</th>
                        <th>Periode</th>
                        <th>Pagi/Malam</th>
                        <th>Kuota</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $i = 1;
                      @endphp
                      @foreach($periodeAslab as $p)
                        <tr>
                          <td>{{ $i }}</td>
                          <td>{{ $p->username }}</td>
                          <td>{{ $p->nama }}</td>
                          <td>{{ $p->namaPraktikum }}</td>
                          <td>{{ $p->tahun }}</td>
                          <td>{{ $p->pagiMalam }}</td>
                          <td>{{ $p->kuota }}</td>
                          <td>
                              <a href="#" class="btn btn-action btn-primary mr-1 updateRecord" data-url="{{ route('periodeAslabUpdate') }}" data-data='{"id":"{{$p->id}}" ,"aslab" : "{{ $p->nama }}", "praktikum": "{{$p->namaPraktikum}} {{$p->tahun}}", "pagiMalam": "{{$p->idPagiMalam}}", "kuota":"{{$p->kuota}}"}'>Update</a>
                              <a href="#" class="btn btn-action btn-danger mr-1 deleteRecord"  data-url="{{ route('periodeAslabDelete') }}" data-id="{{$p->id}}">Delete</a>
                          </td>
                          @php
                            $i++;
                          @endphp
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu">
      <div class="section-body">
        <div class="card card-primary">
          <div class="card-body">
            <div class="card">
              <div class="card-header">
                <h4>Register Asisten Lab Aktif Praktikum {{$praktikum->nama}} {{$praktikum->tahun}}</h4>
              </div>
              <div class="card-body">
                <form method="POST" action="{{ route('periodeAslabProses') }}">
                  @csrf
                  <div class="row">
                    <div class="form-group col-5">
                      <label for="tahun">Praktikum <span style="color: red"> *</span></label>
                      <input type="hidden" name="periodePraktikumId" value="{{ $praktikum->id }}">
                      <input type="text" value="{{ $praktikum->nama }} {{ $praktikum->tahun }}" class="form-control" disabled>
                    </div>
                    <div class="form-group col-7">
                      <label>Aslab <span style="color: red"> *</span></label>
                      @if (count($cekAslab) == 0)
                        <input type="text" value="Asisten Lab Tidak Tersedia" class="form-control" disabled>
                      @elseif (count($aslab) == 0)
                        <input type="text" value="Asisten Lab Sudah Terpakai Semua" class="form-control" disabled>
                      @else
                        <select name="aslab" class="form-control">
                          @foreach($aslab as $a)
                              <option value="{{ $a->id }}">{{ $a->nama }} ({{ $a->username }})</option>
                          @endforeach
                        </select>
                      @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-5">
                      <label>Kuota <span style="color: red"> *</span></label>
                      <input type="text" name="kuota" placeholder="Kuota" class="form-control" onkeypress="return hanyaAngka(event)" required>
                    </div>
                    <div class="form-group col-7">
                      <label>Kelas Pagi/Malam <span style="color: red"> *</span></label>
                      <select name="pagiMalam" class="form-control">
                        @foreach($jenisKelas as $j)
                          <option value="{{ $j->id }}">{{ $j->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    @if (count($cekAslab) == 0)
                      <a href="{{ route('manajemenAslab') }}" class="btn btn-primary btn-block">Register Aslab Baru</a>
                    @elseif (count($aslab) == 0)
                      <a href="{{ route('periodeAslabView') }}" class="btn btn-primary btn-block">Kembali</a>
                    @else                  
                      <button type="submit" class="btn btn-primary btn-block" name="submit">Register</button>
                    @endif                  
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  {{-- modal Delete --}}
    <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form method="POST" class="remove-record-model">
            {{ method_field('delete') }}
            {{ csrf_field() }}
            <input type="hidden" name="id">

            <div class="modal-header">
              <h4 class="modal-title">Realy?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <div class="section-body">
                <p>Do you want to continue?</p>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Yes</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Update --}}
    <div class="modal fade" id="updateModalRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Sesi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="row">
                      <div class="form-group col-5">
                        <label for="tahun">Praktikum <span style="color: red"> *</span></label>
                        <input type="text" value="" name="praktikum" class="form-control" disabled>
                      </div>
                      <div class="form-group col-7">
                        <label>Aslab <span style="color: red"> *</span></label>
                        <input type="text" name="aslab" value="" class="form-control" disabled>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-5">
                        <label>Kuota <span style="color: red"> *</span></label>
                        <input type="text" name="kuota" placeholder="Kuota" class="form-control" onkeypress="return hanyaAngka(event)" required>
                      </div>
                      <div class="form-group col-7">
                        <label>Kelas Pagi/Malam <span style="color: red"> *</span></label>
                        <select name="pagiMalam" class="form-control">
                          @foreach($jenisKelas as $j)
                            <option value="{{ $j->id }}">{{ $j->nama }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Update Record</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  
  @include('admin.form-change-password')         

@endsection

@section('scriptBottom')
    <script>
        $(document).ready( function () {
            $('#datatables').DataTable();
        });

        {{-- Delete Modal Record --}}
          $(document).on('click','.deleteRecord',function(){
              $('#deleteModalRecord').modal('show');
              var userID=$(this).attr('data-id');
              var dataUrl=$(this).attr('data-url');
              $(".remove-record-model").attr('action', dataUrl['url']);
              $('.remove-record-model [name="id"]').attr('value', userID);
          });

          {{-- Update Modal Record --}}
          $(document).on('click','.updateRecord',function(){
              $('#updateModalRecord').modal('show');

              var ambilData=$(this).attr('data-data');
              var data = JSON.parse("["+ambilData.replace(/^\n+|\n+$/g, "").replace(/\n+/g, ",")+"]")[0];
              for(x in data){
                $('.update-record-model [name='+x+']').val(data[x]);
              }

              var dataUrl=$(this).attr('data-url');
              $(".update-record-model").attr('action', dataUrl['url']);
          });

        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))

              return false;
          return true;
        }
    </script>
@endsection