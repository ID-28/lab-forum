@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection

@section('nav_main')
  @component('admin.nav-admin')     
    @slot('nav1')
      Preview Materi Dosen
    @endslot
    @slot('nav2')
      Atur Materi Dosen
    @endslot     
	@endcomponent
@endsection

@section('content')
<div class="tab-content" id="myTabContent">

  <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
    <div class="section-body" id="awal">
      <div class="card card-primary">
        <div class="card-body">
          <div class="card">
            <div class="card-header">
              <h4>Materi Dosen {{$praktikum->nama}} Periode {{$praktikum->tahun}}</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="tabelDetailModul" class="display table table-stripped" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Praktikum</th>
                      <th>Modul</th>
                      <th>Materi</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                      $i = 1;
                    @endphp
                      @foreach ($modulMateriDosen as $e)
                        <tr>
                          <td>{{$i}}</td>
                          <td>{{$e->namaPraktikum}} {{$e->tahun}}</td>
                          <td>{{$e->namaModul}}</td>
                          <td>{{$e->namaMateri}}</td>
                          <td>
                            <button class="btn btn-danger btn-action mr-1 deleteRecord" data-properti='{{$e->id}}'><i class="ion ion-trash-b"></i></button>
                          </td>
                          @php
                            $i++;
                          @endphp
                        </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="tab-pane" id="menu2" role="tabpanel" aria-labelledby="menu">
    <div class="section-body">
      <div class="card card-primary">
        <div class="card-body">
          <div class="card">
            <div class="card-header">
              <h4>Atur Modul Dan Materi Dosen {{$praktikum->nama}} Periode {{$praktikum->tahun}}</h4>
            </div>
            <div class="card-body">
              <form class="insert-materi" method="post" action="{{ route('penilaian_dosen_store') }}">
                {{csrf_field()}}
                <div class="row">
                  <div class="form-group col-6">
                    <label>Praktikum <span style="color: red"> *</span></label>
                    <input type="hidden" name="periodePraktikumId" value="{{ $praktikum->id }}">
                    <input type="text" value="{{ $praktikum->nama }} {{ $praktikum->tahun }}" class="form-control" disabled>
                  </div>
                  <div class="form-group col-6">
                    <label>Modul Dosen <span style="color: red"> *</span></label>
                    @if (count($cekModul) == 0)
                      <input type="text" value="Modul Dosen Periode Ini Tidak Tersedia" class="form-control" disabled>
                    @elseif (count($modul) == 0)
                      <input type="text" value="Modul Dosen Periode Ini Sudah Terpakai" class="form-control" disabled>
                    @else
                      <select name="modul" class="form-control">
                        @foreach($modul as $e)
                            <option value="{{ $e->id }}">{{ $e->nama }}</option>
                        @endforeach
                      </select>
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-12">
                    <label>Pilih Materi Dosen <span style="color: red"> *</span></label>
                    @if (count($cekMateri) == 0)
                      <input type="text" value="Materi Tidak Tersedia" class="form-control" disabled>
                    @elseif (count($materi) == 0)
                      <input type="text" value="Semua Materi Sudah Terpakai" class="form-control" disabled>
                    @endif
                  </div>
                  @if (count($materi) > 0)
                    @foreach ($materi as $e)
                      <div class="form-group col-6">
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" name="materi[]" value="{{$e->id}}" class="custom-control-input" id="materi{{$e->id}}" >
                              <label class="custom-control-label" for="materi{{$e->id}}">{{$e->nama}}</label>
                          </div>
                      </div>
                    @endforeach 
                  @endif                   
                </div>
                <div class="form-group">
                  <div class="modal-footer">
                      @if (count($cekModul) == 0)
                        <a href="{{ route('management_modul_dosen') }}" class="btn btn-primary">Registrasi Modul</a>
                      @elseif (count($cekMateri) == 0)
                        <a href="{{ route('management-materis') }}" class="btn btn-primary">Registrasi Materi</a>
                      @elseif (count($materi) == 0 || count($modul) == 0)
                        <a href="{{ route('management-penilaian-dosen') }}" class="btn btn-primary">Kembali</a>
                      @else
                        <button type="submit" class="btn btn-primary materiBasdat" name="submit">Simpan</button>
                      @endif
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- modal Delete --}}
  <div class="modal fade" id="deleteModalRecord" tabindex="-1" role="dialog" aria-labelledby="deleteModel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="POST" class="remove-record-model">
          {{ method_field('delete') }}
          {{ csrf_field() }}
        
          <div class="modal-header">
            <h4 class="modal-title">Realy?</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <p>Do you want to continue?</p>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-danger">Yes</button>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>

  <script>
    $(document).ready(function() {
      $('#tabelDetailModul').DataTable();
    });
    
    // Delete Modal Record
    $(document).on('click','.deleteRecord',function(){
        $('#deleteModalRecord').modal('show'); 
        let properti=$(this).data('properti');
        $(".remove-record-model").attr('action', '{{ url('admin/management-penilaian-dosen/delete') }}'+'/'+properti);
    });
  </script>
    @include('admin.form-change-password')    
    
@endsection



