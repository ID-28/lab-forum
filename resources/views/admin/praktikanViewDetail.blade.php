@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editAdmin">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Admin Page
@endsection

@section('sidebar')
  @include('admin.sidebar-admin')
@endsection


@section('content')
    <div class="tab-content" id="myTabContent">
    {{-- Praktikan View Detail --}}
        <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu">
            <div class="section-body">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Preview Detail Praktikan</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td class="text-left">Nama</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ $user->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">NPM</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ $user->npm }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">No telp</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">{{ $user->no_tlpn }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Foto</td>
                                        <td class="text-center">:</td>
                                        <td class="text-left">
                                            <img src="{{ url('upload/foto\\') . $user->foto }}" alt="Foto Profile" width="400px" height="auto">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"></td>
                                        <td class="text-center"></td>
                                        <td class="text-left">
                                            <a href="{{ route('praktikanResetPassword', $user->userId) }}" class="btn btn-action btn-primary">Reset Password</a>
                                            <a href="{{ route('praktikanDelete', $user->userId) }}" class="btn btn-action btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('admin.form-change-password')    

@endsection