<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
  <title>Login | Lab. RPL</title>

  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/demo.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
  @include('assets.loader.css')
</head>

<body>
  @include('assets.loader.body')
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              Login Admin
            </div>
            
            @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div><br />
              @endif
              @if(Session::has('pesan'))
                <p class="alert alert-{{ Session::get('jenis') }}">{{ Session::get('pesan') }}</p>
              @endif

            <div class="card card-primary">
              <div class="card-body">
                <form method="POST" action="{{ route('login_admin') }}" class="needs-validation" novalidate="">
                  @csrf
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input id="username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
                  </div>

                  <div class="form-group">
                    <label for="password" class="d-block">Password</label>
                    <div class="input-group" id="show_hide_password">
                      <input id="password" type="password" class="form-control form-password" name="password" tabindex="2" required>
                      <div class="input-group-addon">
                        <i id="showHiddenPassword" class="btn btn-outline-secondary fa fa-eye"></i>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                      <label class="custom-control-label" for="remember-me">Ingat saya!</label>
                    </div>
                  </div> -->

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                      Masuk
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright &copy; 2019 LAB RPL
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js') }}"></script>
  <script src="{{ asset('dist/js/sa-functions.js') }}"></script>
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>
  <script>
    $(document).ready(function(){
      var password = false;
      $("#showHiddenPassword").on('click',function(){
        if(password == false){
          $(this).removeClass('fa-eye');
          $(this).addClass('fa-eye-slash');
          $('.form-password').attr('type','text');
          password = true;
        }else{
          $(this).removeClass('fa-eye-slash');
          $(this).addClass('fa-eye');
          $('.form-password').attr('type','password');
          password = false;
        }
      });

    });
  </script>
  @include('assets.loader.js')
</body>
</html>