<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
  <title>Registrasi Praktikan | Lab. RPL</title>

  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/demo.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
  @include('assets.loader.css')
</head>

<body>
  @include('assets.loader.body')
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              Registrasi Praktikan
            </div>
             @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div><br />
              @endif
              @if(Session::has('pesan'))
                <p class="alert alert-{{ Session::get('jenis') }}">{{ Session::get('pesan') }}</p>
              @endif
            <div class="card card-primary">
              <div class="card-body">
                <form id="form" action="{{ route('register_praktikan') }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="form-group">
                    <label for="npm">NPM</label>
                    <input id="npm" pattern="[0-9]{2}.[0-9]{4}.[0-9]{1}.[0-9]{5}" type="text" class="form-control" name="npm" required>
                  </div>

                  <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input id="nama" type="text" class="form-control" name="nama" required>
                  </div>

                  <div class="form-group">
                    <label for="password" class="d-block">Kata Sandi</label>
                    <div class="input-group" id="show_hide_password">
                      <input id="password" type="password" class="form-control form-password" name="password" required>
                      <div class="input-group-addon">
                        <i id="showHiddenPassword" class="btn btn-outline-secondary fa fa-eye"></i>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                      <label for="password2" class="d-block">Konfirmasi Kata Sandi</label>
                      <input id="password2" type="password" class="form-control form-password" name="password_confirm" required>
                    </div>

                  <div class="form-group">
                    <label for="telp">No.Hp/Whatsapp</label>
                    <input id="telp" type="text" class="form-control" name="telp" onkeypress="return hanyaAngka(event)" required>
                  </div>

                  <div class="form-group">
                    <label for="foto">Foto</label>
                    <input id="foto" type="file" class="form-control" name="foto" accept="image/x-png,image/jpeg" required>
                    <p style="font-size80%;">
                      * Foto formal menggunakan Almamater dan berdasi <br>
                      * Background berwarna Merah <br>
                      * Foto harus rasio 3x4 <br>
                      * Maksimal file foto 2MB <br>
                    </p>
                  </div>

                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="agree" class="custom-control-input" id="agree" required>
                      <label class="custom-control-label" for="agree">Saya menyetujui segala syarat dan ketentuan yang berlaku.</label>
                    </div>
                  </div>

                  <div class="form-group">
                    <button id="submit" class="btn btn-primary btn-block">
                      Registrasi
                    </button>
                    <br>
                    <p class="text-center">
                      Sudah punya akun? <a href="{{ route('login.praktikan.view') }}"> <b>Login disini</b> </a>
                    </p>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright &copy; 2019 LAB RPL
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  
  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js') }}"></script>
  <script src="{{ asset('dist/js/sa-functions.js') }}"></script>
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>
  <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
  <script>
    
    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
    $(document).ready(function(){

        $('#foto').bind('change', function() {
            if(this.files[0].size > 2000000){
              alert('File foto harus kurang dari 2MB');
              $('#submit').prop('disabled',true);
            }else{
              $('#submit').prop('disabled',false);
            }
        });

        var password = false;
        $("#showHiddenPassword").on('click',function(){
          if(password == false){
            $(this).removeClass('fa-eye');
            $(this).addClass('fa-eye-slash');
            $('.form-password').attr('type','text');
            password = true;
          }else{
            $(this).removeClass('fa-eye-slash');
            $(this).addClass('fa-eye');
            $('.form-password').attr('type','password');
            password = false;
          }
        });
        $('#password2','#password').change(function(){
          validatePassword();
        });

        function validatePassword(){
          var password = document.getElementById("password");
          var confirm_password = document.getElementById("password2");
          if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Password tidak sama");
          } else {
            confirm_password.setCustomValidity('');
          }
        }
        $(":input").inputmask();

        $("#npm").inputmask({"mask": "06.2099.1.99999"});

    });
  </script>
  @include('assets.loader.js')
</body>
</html>
