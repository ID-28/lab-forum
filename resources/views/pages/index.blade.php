@extends('layouts.master-client')

@section('content')
   <div id="hero-area" class="hero-area-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
            <div class="contents">
              <span class="h2 font-weight-bold text-body ">Laboratorium <br> Teknik Informatika  </span>
              <p>Bagian dari kami Laboratorium Bahasa Pemrograman (Basprog), Laboratorium Jaringan Komputer (Jarkom), Laboratorium Rekayasa Perangkat Lunak (RPL).</p>
              <div class="header-button">
                <a href="{{ route('login.praktikan.view') }}" class="btn btn-common">Login Praktikan</a>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
            <div class="intro-img">
              <img class="img-fluid" src="{{ url('/') . '/vendor/fusion/img/intro-mobile.png'}}" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
