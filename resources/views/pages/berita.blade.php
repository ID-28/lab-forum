@extends('layouts.master-client')

@section('content')
  <div id="hero-area" class="hero-area-bg" style="background:none; padding: 92px 0 0 0;">
    <div class="container">
      <div class="row">
        <div class="col-md-8">

          @foreach($berita as $b)
            <div class="card col-md-12 p-5 mb-3">
                <div class="card-title">
                  <a href="{{ route('beritaDetail', $b->id) }}"><h2>{{ $b->judul }}</h2></a>
                </div>
                <div class="card-info">
                  <span>Di posting {{ \Carbon\Carbon::createFromTimeStamp(strtotime($b->created_at))->diffForHumans() }} / oleh @if($b->user_id == 2)
                                    <b>Admin RPL</b>
                                @elseif($b->user_id == 3)
                                    <b>Admin Jarkom</b>
                                @elseif($b->user_id == 4)
                                    <b>Admin Basprog</b>
                                @endif</span>
                </div>  
                <a href="{{ route('beritaDetail', $b->id) }}" class="button button-style button-anim fa fa-long-arrow-right"><span>Baca Selengkapnya...</span></a>
            </div>
          @endforeach

         

          @if ($berita->lastPage() == 1)
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item {{ ($berita->currentPage() == 1) ? ' disabled' : '' }}">
                    <a class="page-link" href="{{ $berita->url(1) }}@php
                        if(isset($_GET['lab'])){
                          echo '&lab=' . $_GET['lab'];
                        }
                      @endphp  
                    ">&larr; Sebelumnya</a>
                </li>
                <li class="page-item {{ ($berita->currentPage() == $berita->lastPage()) ? ' disabled' : '' }}">
                    <a class="page-link" href="{{ $berita->url($berita->currentPage()+1) }}@php
                        if(isset($_GET['lab'])){
                          echo '&lab=' . $_GET['lab'];
                        }
                      @endphp  
                      ">Setelahnya &rarr;</a>
                </li>
            </ul>
          @endif
          
        </div>

        <div class="col-md-4">
          <div class="card">
              <h5 class="card-header" style="background-color: #4769B1; color:white; font-weight: bold;">Kategori</h5>
              <div class="card-body">
                  <div class="row">
                      <div class="col-lg-12">
                          <ul class="list-unstyled mb-0">
                            <li>
                                <a href="{{ route('berita') }}?lab=basprog">Laboratorium Bahasa Pemrograman</a>
                            </li>
                            <li>
                                <a href="{{ route('berita') }}?lab=jarkom">Laboratorium Jaringan Komputer</a>
                            </li>
                            <li>
                                <a href="{{ route('berita') }}?lab=rpl">Laboratorium Rekayasa Perangkat Lunak</a>
                            </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
        </div>


      </div>
    </div>        
  </div>
  

@endsection
