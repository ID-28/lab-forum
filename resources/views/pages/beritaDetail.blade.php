@extends('layouts.master-client')

@section('content')
  <div id="hero-area" class="hero-area-bg" style="background:none; padding: 92px 0 0 0;">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="card col-md-12 p-5 mb-3">
            <div class="card-title m-0">
              <h2 class="text-uppercase">{{ $berita->judul }}</h2>
            </div>
            <div class="text-muted mb-3 display6">Di posting {{ \Carbon\Carbon::createFromTimeStamp(strtotime($berita->created_at))->diffForHumans() }} / oleh @if($berita->user_id == 2)
                                    <b>Admin RPL</b>
                                @elseif($berita->user_id == 3)
                                    <b>Admin Jarkom</b>
                                @elseif($berita->user_id == 4)
                                    <b>Admin Basprog</b>
                                @endif</div>
            <div class="card-info" style="font-size: 20px; font-weight: 300; color: #333">
                {!! $berita->deskripsi !!}
            </div>  
          </div>
        </div>
        
        <div class="col-md-4">
          <div class="card mb-4">
              <h5 class="card-header" style="background-color: #4769B1; color:white; font-weight: bold;">Artikel Lainnya</h5>
              <div class="card-body">
                  <div class="row">
                      <div class="col-lg-12">
                          <ul class="list-unstyled mb-0">
                            @foreach($lainnya as $l)
                              <li>
                                  <a href="{{ route('beritaDetail', $l->id) }}">{{ $l->judul }}</a>
                              </li>
                            @endforeach
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
          <a href="{{ route('berita') }}" class="btn btn-primary col-md-12"> &laquo; Kembali</a>
        </div>


      </div>
    </div>        
  </div>
  

@endsection
