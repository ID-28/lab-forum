<html>
    <head>
        <title>Formulir Pendaftaran Praktikum</title>
        <style>
            *{
                font-family: 'Helvetica';
            }
        </style>
    </head>
    <body>
        <table style="width:100%;">
            <tr>
                <td style="width: 20%;text-align:center;">
                    <img src="asset/logoitats.png" alt="logo ITATS" style="width: 70px;height:auto;">
                </td>
                <td style="width: 60%;text-align:center;">
                        <span>Laboratorium {{ ucfirst($praktikan->namaLab) }} Teknik Infomatika</span> <br>
                        <span>Institut Teknologi Adhi Tama Surabaya</span> <br>
                        <span>Jl. Arief Rahman Hakim No.100, Surabaya, Indonesia 60117</span>
                </td>
                <td style="width: 20%;text-align:center;">
                    <img src="asset/{{ $praktikan->labId }}.png" alt="logo Lab" style="width: 70px;height:auto;">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr size="3" color="#808080">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="font-size: 150%">
                    <u>Biodata Praktikan</u>
                </td>
            </tr>
            <tr>
                <td style="width:20%;font-size: 110%;font-weight:500%;line-height: 1.7;" valign="top">
                    ID Registrasi <br>
                    NPM <br>
                    Nama <br>
                    Praktikum <br>
                    Jadwal <br>
                </td>
                <td style="width:60%;font-size: 110%;line-height: 1.7;" valign="top">
                    : {{ FormulirHelp::regId($praktikan->namaPraktikum,$praktikan->tahun,$praktikan->id) }} <br>
                    : {{$praktikan->npm}} <br>
                    :  @php
                            {{
                                $nama = explode(" ",$praktikan->nama);
                                if(count($nama) > 4){
                                    for($i=0;$i<5;$i++){
                                        echo $nama[$i] . " ";
                                    }
                                }else{
                                    echo $praktikan->nama;
                                }
                            }}
                        @endphp <br>
                    : {{ $praktikan->namaPraktikum }} {{ $praktikan->tahun }} <br>
                    : {{ $praktikan->waktu }} <br>
                </td>
                <td style="width:20%;" valign="top">
                    <img src="upload/foto/{{ $praktikan->foto }}" alt="Foto Profil" style="width: 150px;height: 200px;">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="font-size: 150%">
                    <br>
                    <u>Persyaratan Berkas</u>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <ul>
                        <li>Form Pendaftaran Praktikum (1x)</li>
                        <li>Kwitansi Pembayaran Praktikum (1x)</li>
                        <li>Kwitansi Pembayaran Praktikum Fotokopi (1x)</li>
                        <li>Kartu Rencana Studi Fotokopi (1x)</li>
                    </ul>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                 <td colspan="3">
                    <table style="width: 100%">
                        <tr>
                            <td style="width:30%; text-align:center;"></td>
                            <td style="width:40%"></td>
                            <td style="width:30%; text-align:center;">Surabaya, {{ $waktu }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="font-size: 120%">
                    <table style="width: 100%">
                        <tr>
                            <td style="width:30%; text-align:center;">Asisten Laboratorium <br><br><br><br><br></td>
                            <td style="width:40%"></td>
                            <td style="width:30%; text-align:center;">Pendaftar Praktikum <br><br><br><br><br></td>
                        </tr>
                    </table>
                </td>
            </tr>
           <tr>
                <td colspan="3" style="font-size: 120%;">
                    <table style="width: 100%">
                        <tr>
                            <td style="width:30%; text-align:center;"><hr size="3" color="#808080"> <br></td>
                            <td style="width:40%"></td>
                            <td style="width:30%; text-align:center;">
                                <hr size="3" color="#808080" style="margin:0">
                                @php
                                    {{
                                        $nama = explode(" ",$praktikan->nama);
                                        if(count($nama) > 4){
                                            for($i=0;$i<5;$i++){
                                                echo $nama[$i] . " ";
                                            }
                                        }else{
                                            echo $praktikan->nama;
                                        }
                                    }}
                                @endphp 
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="font-size: 70%;">
                    <b>Petunjuk:</b>  
                    <ol>
                        <li>Cetak dan tanda tangani formulir ini pada kolom "Pendaftar Praktikum"</li>
                        <li>Serahkan semua persyaratan ke Laboratorium {{ ucfirst($praktikan->namaLab) }} Ruang {{ strtoupper($praktikan->ruangLab) }} untuk diverifikasi</li>
                        <li>Informasi Technical Meeting (TM) dapat dilihat di <span style="color:blue;">{{ url('/') }}</span></li>
                    </ol>
                    *Berkas persyaratan bisa dilihat di pengumuman <br>
                    *Jika ada kesalahan data silahkan hubungi Asisten Laboratorium {{ ucfirst($praktikan->namaLab) }}  <br>
                    *Informasi praktikum akan diumumkan pada halaman <span style="color:blue;">{{ url('/') }}/informasi</span>
                </td>
            </tr>
        </table>
    </body>
</html>