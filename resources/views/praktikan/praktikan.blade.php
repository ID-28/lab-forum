@extends('layouts.master')

@section('title_page')
<img src="{{ url('upload/foto',Auth::user()->mahasiswa->foto) }}" alt="Foto Profile" width="230px" style="margin-top:10px;"> <br>
<span class="text-primary">Hi, {{ strtok(Auth::user()->mahasiswa->nama,  ' ') }}</span>
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li class="active">
        <a href="{{ route('praktikan') }}"><i class="ion ion-home"></i><span>Dashboard</span></a>
      </li>
      <li>
        <a href=" {{ route('praktikan.editProfile.view') }}"><i class="ion ion-person"></i><span>Edit Profile</span></a>
      </li>
      <li>
        <a href="{{ route('praktikan.sesiPraktikum') }}"><i class="ion ion-calendar"></i><span>Sesi Praktikum</span></a>
      </li>
        <li>
        <a href="{{ route('praktikan.registrasiPraktikum.view') }}"><i class="ion ion-bookmark"></i><span>Registrasi Praktikum</span></a>
      </li>
      <li>
          <a href="{{ route('thread_praktikum') }}"><i class="ion ion-bookmark"></i><span>Thread</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">

        <h2 class="section-title">Dashboard</h2><br>
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div><br />
        @endif
        @if(Session::has('pesan'))
          <p class="alert alert-{{ Session::get('jenis') }}">{{ Session::get('pesan') }}</p>
        @endif
        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th> Praktikum </th>
                        <th> Jadwal </th>
                        <th> Verifikasi </th>
                    </tr>
                </thead>
                <tbody>
                  @if(count($praktikan) > 0)
                    @foreach($praktikan as $p)
                      <tr>
                          <td>
                            {{$p->namaPraktikum}} ({{$p->tahun}})
                          </td>
                          <td>
                            {{$p->waktu}}
                          </td>
                          @if($p->status == 0)
                              <td> <a href="{{ route('praktikan.cetakFormulir',$p->id) }}" class="btn btn-success">Cetak Formulir</a> </td>
                          @else
                              <td> <p class="text-success"> <b>Sudah diverifikasi</b> <br> <button class="btn btn-action btn-primary" data-toggle="modal" data-target="#modalAslabAsdos-{{$p->id}}">Lihat Pembimbing</button></p>
                              <div id="modalAslabAsdos-{{$p->id}}" class="modal fade bd-example-modal-lg" role="dialog">
                                <div class="modal-dialog modal-lg">">

                                  <div class="modal-content">
                                    <div class="modal-body">
                                     <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th></th>
                                            <th>Nama</th>
                                            <th>NIP/NPM</th>
                                            <th>No HP/Whatsapp</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td><b>Asisten Laboratorium</b></td>
                                            <td>{{ $aslabAsdos[0]->namaAslab }}</td>
                                            <td>{{ $aslabAsdos[0]->NPM }}</td>
                                            <td>{{ $aslabAsdos[0]->tlpnAslab }}</td>
                                          </tr>
                                          <tr>
                                            <td><b>Dosen Pembimbing</b></td>
                                            <td>{{ $aslabAsdos[1]->namaDosen }}</td>
                                            <td>{{ $aslabAsdos[1]->NIP }}</td>
                                            <td>{{ $aslabAsdos[1]->tlpnDosen }}</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>

                                </div>
                              </div>
                              </td>
                          @endif
                      </tr>
                    @endforeach
                  @else
                    <tr>
                        <td colspan="3" class="text-center">Anda belum mendaftar praktikum apapun</td>
                    </tr>
                  @endif
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
@endsection
