@extends('layouts.master')

@section('title_page')
<img src="{{ url('upload/foto',Auth::user()->mahasiswa->foto) }}" alt="Foto Profile" width="230px" style="margin-top:10px;"> <br>
<span class="text-primary">Hi, {{ strtok(Auth::user()->mahasiswa->nama,  ' ') }}</span>
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li>
        <a href="{{ route('praktikan') }}"><i class="ion ion-home"></i><span>Dashboard</span></a>
      </li>
      <li class="active">
        <a href=" {{ route('praktikan.editProfile.view') }}"><i class="ion ion-person"></i><span>Edit Profile</span></a>
      </li>
      <li>
        <a href="{{ route('praktikan.sesiPraktikum') }}"><i class="ion ion-calendar"></i><span>Sesi Praktikum</span></a>
      </li>
        <li>
        <a href="{{ route('praktikan.registrasiPraktikum.view') }}"><i class="ion ion-bookmark"></i><span>Registrasi Praktikum</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">

        <h2 class="section-title">Edit Profile</h2><br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        @if(Session::has('pesan'))
            <p class="alert alert-{{ Session::get('jenis') }}">{{ Session::get('pesan') }}</p>
        @endif

        <form id="form" method="POST" action="{{ route('praktikan.editProfile.proses') }}"enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group col-12">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ auth::user()->mahasiswa->nama }}" placeholder="Masukkan Nama Lengkap" required>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label>Kata Sandi Baru (<span class="text text-warning">*Kosongkan jika tidak diganti</span>)</label>
                   <div class="input-group" id="show_hide_password">
                        <input id="password" type="password" class="form-control form-password" name="password" placeholder="Masukkan kata sandi baru">
                        <div class="input-group-addon">
                            <i id="showHiddenPassword" class="btn btn-outline-secondary fa fa-eye"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label>Telp.</label>
                    <input type="text" class="form-control" name="telp" value="{{ auth::user()->mahasiswa->no_tlpn }}" placeholder="Masukkan nomor telp.">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label>Foto</label>
                    <input id="foto" type="file" class="form-control" name="foto" accept="image/x-png,image/jpeg">
                    <p style="font-size:80%" >
                      * Foto formal menggunakan Almamater dan berdasi <br>
                      * Background berwarna Merah <br>
                      * Foto harus rasio 3x4 <br>
                      * Maksimal file foto 2MB <br>
                    </p>
                </div>
            </div>

            <div class="form-group">
                <button id="submit" class="btn btn-primary btn-block">
                    Ubah
                </button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scriptBottom')
    <script>
        $(document).ready(function(){

            $('#foto').bind('change', function() {
                if(this.files[0].size > 2000000){
                    alert('File foto harus kurang dari 2MB');
                    $('#submit').prop('disabled',true);
                }else{
                    $('#submit').prop('disabled',false);
                }
            });

            var password = false;
            $("#showHiddenPassword").on('click',function(){
                if(password == false){
                    $(this).removeClass('fa-eye');
                    $(this).addClass('fa-eye-slash');
                    $('.form-password').attr('type','text');
                    password = true;
                }else{
                    $(this).removeClass('fa-eye-slash');
                    $(this).addClass('fa-eye');
                    $('.form-password').attr('type','password');
                    password = false;
                }
            });
        });
    </script>
@endsection()
