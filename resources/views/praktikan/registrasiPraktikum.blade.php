@extends('layouts.master')

@section('title_page')
<img src="{{ url('upload/foto',Auth::user()->mahasiswa->foto) }}" alt="Foto Profile" width="230px" style="margin-top:10px;"> <br>
<span class="text-primary">Hi, {{ strtok(Auth::user()->mahasiswa->nama,  ' ') }}</span>
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li>
        <a href="{{ route('praktikan') }}"><i class="ion ion-home"></i><span>Dashboard</span></a>
      </li>
      <li>
        <a href=" {{ route('praktikan.editProfile.view') }}"><i class="ion ion-person"></i><span>Edit Profile</span></a>
      </li>
      <li>
        <a href="{{ route('praktikan.sesiPraktikum') }}"><i class="ion ion-calendar"></i><span>Sesi Praktikum</span></a>
      </li>
        <li class="active">
        <a href="{{ route('praktikan.registrasiPraktikum.view') }}"><i class="ion ion-bookmark"></i><span>Registrasi Praktikum</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
    <div class="section-body">
        <div class="card card-primary">
        <div class="card-body">

            <h2 class="section-title">Registrasi Praktikum</h2><br>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            @if(Session::has('pesan'))
            <p class="alert alert-{{ Session::get('jenis') }}">{{ Session::get('pesan') }}</p>
            @endif
            
            <form id="formDaftar" method="POST" action="{{ route('praktikan.registrasiPraktikum.proses') }}">
                @csrf
                <div class="form-divider">
                *Pastikan form ini terisi dengan benar sebelum klik tombol registrasi
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-sm-12">
                        <label>Mata Kuliah</label>
                        <select id="matkul" name="matkul" class="form-control">
                            @php
                                $praktikum = [];
                                foreach($sesi as $s){
                                    if(!in_array($s->id_praktikum,$praktikum)){
                                        array_push($praktikum,$s->id_praktikum);
                                        echo "<option value='".$s->id_praktikum."'>". $s->nama_praktikum ." ($s->tahun) </option>";
                                    }
                                }
                            @endphp
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        <label>Kelas</label>
                        <select id="kelas" name="kelas" class="form-control">
                            @foreach($kelas as $k)
                                <option value="{{ $k->id }}">{{ $k->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-12">
                        <label>Sesi</label>
                        <select id="sesi" name="sesi" class="form-control">
                                
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#confirm">
                        Registrasi
                    </a>
                </div>
            </form>
        </div>
    </div>
  </div>
@endsection

@section('scriptBottom')
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Konfirmasi Pemilihan Sesi Praktikum
                </div>
                <div class="modal-body">
                    Apakah anda yakin dengan sesi yang sudah anda pilih? Perubahaan sesi hanya bisa dilakukan sebelum diverifikasi Asisten Laboratorium dan hanya dapat dirubah oleh Asisten Laboratorium
                </div>
                <div class="modal-footer">
                    <button id="btnBatal" type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" id="formSubmit" class="btn btn-primary btn-block">
                        Saya sudah yakin
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#btnBatal').click(function(){
                $('#confirm').modal('hide');
            });
            $('#formSubmit').click(function(){
               $('#formDaftar').submit();
            });

            var dataSesi = {!! json_encode($sesi) !!};

            pilihsesi(dataSesi);

            $('#matkul').on('change', function() {
                pilihsesi();
            });

            function pilihsesi(){
                var matkul = $('#matkul').val();
                $("#sesi").empty();
                for(var i=0; i<=dataSesi.length-1; i++){
                    if(dataSesi[i].id_praktikum == matkul){
                        var component = $('<option></option>').attr("value", dataSesi[i].id).text(dataSesi[i].nama+" - "+dataSesi[i].waktu);
                        $("#sesi").append(component);;
                    }
                }
            }
            
        });
    </script>
@endsection()