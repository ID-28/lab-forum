@extends('layouts.master')

@section('title_page')
<img src="{{ url('upload/foto',Auth::user()->mahasiswa->foto) }}" alt="Foto Profile" width="230px" style="margin-top:10px;"> <br>
<span class="text-primary">Hi, {{ strtok(Auth::user()->mahasiswa->nama,  ' ') }}</span>
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li>
        <a href="{{ route('praktikan') }}"><i class="ion ion-home"></i><span>Dashboard</span></a>
      </li>
      <li>
        <a href=" {{ route('praktikan.editProfile.view') }}"><i class="ion ion-person"></i><span>Edit Profile</span></a>
      </li>
      <li class="active">
        <a href="{{ route('praktikan.sesiPraktikum') }}"><i class="ion ion-calendar"></i><span>Sesi Praktikum</span></a>
      </li>
        <li>
        <a href="{{ route('praktikan.registrasiPraktikum.view') }}"><i class="ion ion-bookmark"></i><span>Registrasi Praktikum</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">

        <h2 class="section-title">Sesi Praktikum</h2><br>
    
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        @if(Session::has('pesan'))
            <p class="alert alert-{{ Session::get('jenis') }}">{{ Session::get('pesan') }}</p>
        @endif

        <div class="table-responsive">
            <table id="tabelDosen" class="display table table-stripped" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-left">Praktikum</th>
                        <th class="text-left">Sesi</th>
                        <th class="text-left">Waktu</th>
                        <th>Sisa Kuota</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($sesi) > 0)
                      @foreach($sesi as $s)
                        <tr>
                            <td class="text-left">{{ $s->nama_praktikum }} ({{ $s->tahun }})</td>
                            <td class="text-left">{{ $s->nama }}</td>
                            <td class="text-left">{{ $s->waktu }}</td>
                            <td>
                              @if($s->kuota > 0)
                                {{ $s->kuota }}
                              @else
                                <span class="text-danger font-weight-bold">Kuota Habis</span>
                              @endif
                            </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                          <td colspan="4" class="text-center">Sesi praktikum belum tersedia</td>
                      </tr>
                    @endif
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scriptBottom')
    
@endsection()