@extends('layouts/master')

@section('nama_user')
  Hi, Aslab
@endsection

@section('title_page')
  Aslab Page
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li class="active">
        <a href="{{ route('aslab') }}"><i class="ion ion-speedometer"></i><span>Dashboard</span></a>
      </li>
      <li>
        <a href="{{ route('entryNilaiAslab') }}"><i class="ion ion-clipboard"></i><span>Entry Nilai</span></a>
      </li>
      <li>
        <a href="{{ route('listPraktikanAslab') }}"><i class="ion ion-stats-bars"></i><span>Preview Praktikan</span></a>
      </li>
      <li>
        <a href="{{route('praktikum_thread')}}"><i class="ion ion-stats-bars"></i><span>Thread</span></a>
      </li>
      <li>
        <a href="chartjs.html"><i class="ion ion-stats-bars"></i><span>Other Menu</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">

        <h2 class="section-title">Welcome ....</h2><br>
        <div class="row">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-primary">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  24
                </div>
                <div class="card-header pb-0">
                  <h4>Total Praktikan</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="{{ route('listPraktikanAslab') }}">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-danger">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  12
                </div>
                <div class="card-header pb-0">
                  <h4>Total Aslab</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-warning">
                <i class="ion ion-person"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  1,201
                </div>
                <div class="card-header pb-0">
                  <h4>Other</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="#">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="card card-sm">
              <div class="card-icon text-success">
                <i class="ion ion-record"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                  47
                </div>
                <div class="card-header pb-0">
                  <h4>Other</h4>
                </div>
              </div>
              <div class="card-cta">
                <a href="#">More Info <i class="ion ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection