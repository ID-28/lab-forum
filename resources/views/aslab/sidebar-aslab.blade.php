@component('layouts.sidebar' )
    @slot('menu')
        <li class="menu-header">Dashboard</li>
    <li class="{{ Request::is('dosen') ? 'active' : ''}}">
        <a href="{{ route('aslab') }}"><i class="ion ion-ios-home"></i><span>Dashboard</span></a>
    </li>
    <li class="{{ Request::is('aslab/entry-nilai') || Request::is('aslab/entry-nilai/*') ? 'active' : ''}}">
        <a href="#"><i class="ion ion-clipboard"></i><span>Entry Nilai</span></a>
    </li>
    <li class="{{ Request::is('aslab/list-praktikan') || Request::is('aslab/list-praktikan/*') ? 'active' : ''}}">
        <a href="{{ route('listPraktikanAslab') }}"><i class="ion ion-android-contact"></i><span>List Praktikan</span></a>
    </li>
    <li>
          <a href="{{ route('thread_praktikum') }}"><i class="ion ion-bookmark"></i><span>Thread</span></a>
      </li>
    @endslot
@endcomponent