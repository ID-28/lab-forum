@extends('layouts/master')

@section('nama_user')
  Hi, Aslab
@endsection

@section('title_page')
  Dosen Page
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li>
        <a href="{{ route('aslab') }}"><i class="ion ion-speedometer"></i><span>Dashboard</span></a>
      </li>
      <li  class="active">
        <a href="{{ route('entryNilaiAslab') }}"><i class="ion ion-clipboard"></i><span>Entry Nilai</span></a>
      </li>
      <li>
        <a href="{{ route('listPraktikanAslab') }}"><i class="ion ion-stats-bars"></i><span>Preview Praktikan</span></a>
      </li>
      <li>
        <a href="chartjs.html"><i class="ion ion-stats-bars"></i><span>Other Menu</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
  <div class="card card-primary">
  <div class="card-header"><h4>Pilih Praktikum</h4></div>

  <div class="card-body">
    <form method="POST" action="{{ url('aslab/entry-nilai/praktikan') }}">
    {{ csrf_field() }}
    <div class="row">
      <div class="form-group col-12">
        <label>Praktikum</label>
        <select class="form-control">
          <option>Pemrograman Berorientasi Objek</option>
          <option>Basis Data </option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block">
        Pilih
      </button>
    </div>
    </form>
  </div>
</div>
@endsection