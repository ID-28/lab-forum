@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editDosen">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Aslab Page
@endsection

@section('sidebar')
	@include('aslab.sidebar-aslab') 
@endsection

@section('content')
    <div class="section-body">
        <div class="card card-primary">
            <div class="card-header">
                <h4>Preview Detail Praktikan</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td class="text-left">Nama</td>
                                <td class="text-center">:</td>
                                <td class="text-left">{{ $user->nama }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">NPM</td>
                                <td class="text-center">:</td>
                                <td class="text-left">{{ $user->npm }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">No telp</td>
                                <td class="text-center">:</td>
                                <td class="text-left">{{ $user->no_tlpn }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">Foto</td>
                                <td class="text-center">:</td>
                                <td class="text-left">
                                    <img src="{{ url('upload/foto\\') . $user->foto }}" alt="Foto Profile" width="400px" height="auto">
                                </td>
                            </tr>
                            <tr>
                                <td class="text-left"></td>
                                <td class="text-center"></td>
                                <td class="text-left">
                                    <a href="{{ route('listPraktikanAslab') }}" class="btn btn-primary">Kembali</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('aslab.form-change-password')
@endsection