@extends('layouts.master-thread')

@section('content')
    @foreach($praktikum as $p)
        <a href="{{route('modul_thread',$p->idPraktikum)}}" class="list-group-item">
            <h4 class="list-group-item-heading">{{$p->namaPraktikum}}</h4>
        </a>
        <br>
    @endforeach
@endsection
