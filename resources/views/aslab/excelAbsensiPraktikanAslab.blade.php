<table>
    <tbody>
        <tr>
            <td colspan="2" style="text-align:center; font-size:16px"><h1><b>Absensi Praktikan</b></h1></td>
        </tr>
        <tr>
            <td colspan="2"> <br> </td>
        </tr>
        <tr>
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">NPM</td>
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">Nama</td>
            <td style="text-align:center;font-weight:bold;border:1px solid $000;">Sesi</td>
        </tr>
        @foreach($praktikan as $p)
            <tr>
                <td style="border:1px solid $000;">{{ $p->npm }}</td>
                <td style="border:1px solid $000;">{{ $p->namaMahasiswa }}</td>
                <td style="border:1px solid $000;">{{ $p->namaSesi }} / {{ $p->waktu }}</td>
            </tr>
        @endforeach
    </tbody>
</table>