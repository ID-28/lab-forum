@extends('layouts/master')

@section('nama_user')
  Hi, Aslab
@endsection

@section('title_page')
  Dosen Page
@endsection

@section('sidebar')
  @component('layouts.sidebar' )
    @slot('menu')
      <li class="menu-header">Dashboard</li>
      <li>
        <a href="{{ route('aslab') }}"><i class="ion ion-speedometer"></i><span>Dashboard</span></a>
      </li>
      <li  class="active">
        <a href="{{ route('entryNilaiAslab') }}"><i class="ion ion-clipboard"></i><span>Entry Nilai</span></a>
      </li>
      <li>
        <a href="{{ route('listPraktikanAslab') }}"><i class="ion ion-stats-bars"></i><span>Preview Praktikan</span></a>
      </li>
      <li>
        <a href="chartjs.html"><i class="ion ion-stats-bars"></i><span>Other Menu</span></a>
      </li>
    @endslot
  @endcomponent
@endsection

@section('content')
  {{-- Entry Nilai praktikan --}}
  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">
        <div class="card">
          <div class="card-header">
            <h4>Entry Nilai Praktikan</h4>
          </div>
          <div class="card-body p-0">
            <div class="table-responsive">
              <table class="table table-striped">
                <tr>
                  <th>NPM</th>
                  <th>Nama Praktikan</th>
                  <th>Sesi</th>
                  <th>Entry Nilai</th>
                </tr>
                <tr>
                  <td>06.2016.1.06683</td>
                  <td>Rendi Prayoga</td>
                  <td>pagi</td>
                  <td><a class="btn btn-outline-primary entryNilaiPraktikan">Entry Nilai</a></td>
                </tr>
              </table>
            </div>
          </div>
          <br>

          <div class="card-footer text-right">
            <nav class="d-inline-block">
              <ul class="pagination mb-0">
                <li class="page-item disabled">
                  <a class="page-link" href="#" tabindex="-1"><i class="ion ion-chevron-left"></i></a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                <li class="page-item">
                  <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#"><i class="ion ion-chevron-right"></i></a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>


  {{-- Modal Entry Nilai --}}
  <div class="modal fade" id="entryNilaiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Entry Nilai Praktikan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="section-body">
              <div class="card card-primary">
                <div class="card-body">
                  <form method="POST" class="update-record-model">
                    {{ method_field('put') }}
                    {{csrf_field()}}
                    <div class="row">
                      <div class="form-group col-6">
                        <label for="nip">Nilai Modul 1</label>
                        <input type="text" class="form-control" name="unip"  autofocus>
                      </div>
                      <div class="form-group col-6">
                        <label for="nip">Nilai Modul 2</label>
                        <input type="text" class="form-control" name="unip" autofocus>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-6">
                        <label for="nip">Nilai Modul 3</label>
                        <input type="text" class="form-control" name="unip" autofocus>
                      </div>
                      <div class="form-group col-6">
                        <label for="nip">Nilai Modul 4</label>
                        <input type="text" class="form-control" name="unip" autofocus>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-6">
                        <label for="nip">Nilai Modul 5</label>
                        <input type="text" class="form-control" name="unip" autofocus>
                      </div>
                      <div class="form-group col-6">
                        <label for="nip">Nilai Modul 6</label>
                        <input type="text" class="form-control" name="unip" autofocus>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="custom-control ">
                        <input type="checkbox" name="agree" class="custom-control-input" required>
                        <label class="custom-control-label" for="agree">I agree with the terms and conditions</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="submit">Entry Nilai Record</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



   <script>
      function hanyaAngka(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

               return false;
           return true;
       }
    {{-- Delete Modal Record --}}
    $(document).on('click','.entryNilaiPraktikan',function(){
        $('#entryNilaiModal').modal('show');
    });

    {{-- Update Modal Record --}}
    // $(document).on('click','.updateRecord',function(){
    //     $('#updateModalRecord').modal('show');

    //     function pasValue(count,attr,val){
    //       for (let i=0; i<=count; i++) {
    //         $('input[name='+attr[i]+']').val(val[i]);
    //       }
    //     }

    //     let typeUser=$(this).attr('type-user');
    //     let user=$(this).data('user');

    //     let attr = ['unip', 'unama_dosen' , 'upassword' , 'upassword_confirm' , 'uno_tlpn'];
    //     let val  = [user[0] , user[1],user[2] , user[2], user[3]];

    //     pasValue(4,attr,val);
    //     $(".update-record-model").attr('action', '{{ url('admin/edit') }}/'+typeUser+'/'+user[0]);
    // });
  </script>
@endsection