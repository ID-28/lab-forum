@extends('layouts.master')

@section('btnProfile')
  <a href="#" class="dropdown-item has-icon editDosen">
    <i class="ion ion-log-out"></i> Change Password
  </a>
@endsection

@section('title_page')
  Aslab Page
@endsection

@section('sidebar')
	@include('aslab.sidebar-aslab') 
@endsection

@section('content')

  <div class="section-body">
    <div class="card card-primary">
      <div class="card-body">
        <div class="card">
          <div class="card-header">
            @if (count($praktikan) == 0)
              <h3>Anda Tidak Mempunyai Praktikan Pada Praktikum Ini</h3>
            @else
              <h3>Daftar Praktikan</h3>
              <a class="float-right btn btn-action btn-primary" href="{{ route('cetakListPraktikanAslab') }}">Unduh List Praktikan</a>
            @endif
          </div>
          <div class="card-body">
            <div class="table-responsive">
              @if (count($praktikan) != 0)
                <table id="tabelListPraktikan" class="display table table-stripped" style="width:100%">
                  <thead>
                    <tr>
                      <th>NPM</th>
                      <th>Nama</th>
                      <th>Sesi</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($praktikan as $p)
                      <tr>
                        <td>{{$p->npm}}</td>
                        <td>{{$p->namaMahasiswa}}</td>
                        <td>{{$p->namaSesi}} {{$p->waktu}}</td>
                        <td>
                          <a href="{{ route('listPraktikanAslabView', $p->idMahasiswa) }}" class="btn btn-primary">View</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table><br>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
     $(document).ready(function() {
      $('#tabelListPraktikan').DataTable();
    });
  </script>

    @include('aslab.form-change-password')
@endsection