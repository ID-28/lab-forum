@extends('layouts.master-thread')

@section('content')

    @if($jenis_user->jenis_user == 3)
        @foreach($modul as $m)
            <a href="{{route('thread_ketiga',$m->idModul)}}" class="list-group-item">
                <h4 class="list-group-item-heading">{{$m->namaModul}}</h4>
            </a>
            <br>
        @endforeach
    @elseif($jenis_user->jenis_user == 5)
        @foreach($modul as $m)
            <a href="{{route('thread_materi',$m->idModul)}}" class="list-group-item">
                <h4 class="list-group-item-heading">{{$m->namaModul}}</h4>
            </a>
            <br>
        @endforeach
    @elseif($jenis_user->jenis_user == 4)
        @foreach($modul as $m)
            <a href="{{route('materi_thread',$m->idModul)}}" class="list-group-item">
                <h4 class="list-group-item-heading">{{$m->namaModul}}</h4>
            </a>
            <br>
        @endforeach
    @endif
@endsection
